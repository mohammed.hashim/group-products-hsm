import { API_URL } from "@/config";
import { detectUserAgentInfo } from "./common-utils";

export interface ILEData {
  mobile?: string | number;
  country_code?: string;
  email?: string;
  name?: string;
  type?: string;
  product?: string;
  category?: string;
  kind?: string | string[];
  sub_kind?: string;
  ticket?: string | number;
  pageId?: string | number;
  tparty?: string;
  referrer?: string;
  url?: string;
  widget_name?: string;
  data?: {
    widget_name?: string;
    [key: string]: any;
  };
  kinds?: string[];
  tid?: string;
  cid?: string;
  userInfo?: any;
}

export async function logEvent(
  logEventEnabled?: boolean,
  inData?: ILEData,
  successCallback?: (...args: any) => void,
  failureCallback?: any
) {
  inData = inData || {};
  let gaObjMap;
  try {
    gaObjMap = (window as any)["ga"] && (window as any)["ga"].getAll()[0];
  } catch (e) {}
  let data: any = {
    type: inData.type || (window as any)["sharedConstants"]["logType"]["view"],
    // ticket: inData.ticket || (window as any)["LOG_TICKET"],
    page_id: inData.pageId || (window as any)["PAGE_ID"],
    widget_name: inData.widget_name,
    kind: (window as any)["sharedConstants"]["logKind"]["group_product"] || "",
    // product: inData.product || (window as any)["logKind"]["group_product"],
    sub_kind: inData.sub_kind || inData.category,
    // marketing_params: inData.tparty,
    referer: inData.referrer || document.referrer || undefined,
    url: inData.url || (window as any).location.href,
    data: inData.data,
    user_agent_info: (await detectUserAgentInfo()) || {},
    // tid: inData.tid || (gaObjMap && gaObjMap.get("trackingId")) || "",
    // cid: inData.cid || (gaObjMap && gaObjMap.get("clientId")) || "",
  };

  if (inData.kinds) {
    data.kinds = inData.kinds;
  }

  if (data.type === (window as any)["sharedConstants"]["logType"]["submit"]) {
    data.mobile = inData.mobile;
    data.country_code = inData.country_code;
    data.email = inData.email;
    data.name = inData.name;
  }

  let headers = {
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    // 'X-CSRF-Token': await (window as any)['__csrfPromise'],
  };
  if (data["mobile"]) {
    data["mobile"] = String(data["mobile"]);
  }
  if (data.data && data.data.widget_name) {
    data.data.widget_name = data.data.widget_name.replace(/-/g, "_");
  }
  if (logEventEnabled) {
    return fetch(`${API_URL}/log-event`, {
      method: "POST",
      headers,
      body: JSON.stringify(data),
    })
      .then((res: any) => successCallback && successCallback())
      .catch((err: any) => {
        const callback =
          failureCallback === true
            ? successCallback
            : failureCallback || successCallback;
        callback && callback();
      });
  }
}

// (window as any)['__csrfPromise'] = getCSRF()

(window as any)["sharedUtils"] = {
  logEvent,
};
