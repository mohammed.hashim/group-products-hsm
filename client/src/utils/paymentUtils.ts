import { IPayment } from "@/interfaces/paInterfaces";
import { razorpayCredentials } from "@/config";

export const makePayment = (paymentData: IPayment) => {
  let options = {
    key: razorpayCredentials.uat.CLIENT_ID, // Enter the Key ID generated from the Dashboard
    amount: paymentData.amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    currency: "INR",
    name: "Coverfox",
    description: "Coverfox Transaction",
    order_id: paymentData.order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    prefill: paymentData.userData,
    readonly: { email: true, contact: true },
    handler: paymentData.handler,
    notes: {},
    theme: {
      color: "#6B7AFE",
    },
    modal: {
      ondismiss: paymentData.onModaldismiss,
    },
  };
  let rzp1 = new (window as any).Razorpay(options);
  rzp1.open();
};
