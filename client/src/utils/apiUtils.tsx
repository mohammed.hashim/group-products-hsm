import axios from "axios";

export const get = (url: string, config?: any) => {
  return axios.get(url, config);
};

export const post = (url: string, data: any) => {
  return axios.post(url, data);
};
