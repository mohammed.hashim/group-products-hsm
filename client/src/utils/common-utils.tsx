import { memberList } from "@/constants/groupConstant";
import dayjs from "dayjs";
import * as numberToWords from "number-to-words";
import { KeyboardEvent } from "react";

export function getImageURL(path: string) {
  const imgModule = require("@/assets/images/" + path);
  return imgModule;
}

export function calculateTotalPremium(price: number) {
  return {
    gst: (price * 18) / 100,
    totalPremium: (price * 18) / 100 + price,
  };
}

export function onlyNumbers(e: KeyboardEvent<HTMLInputElement>) {
  const element = e.target as HTMLInputElement;
  return (element.value = element.value.replaceAll(/[^\d]/g, ""));
}

export function onlyAlphabets(e: KeyboardEvent<HTMLInputElement>) {
  const element = e.target as HTMLInputElement;
  element.value = element.value.replaceAll(/[^a-zA-Z\s]/g, "");
}

export function showMembers(memberObj: any, proposerGender: string) {
  let string = "You";
  var childCount = 0,
    word = "";
  Object.keys(memberObj).map((key: any) => {
    if (key === "wife" && proposerGender === "male") {
      string = string + `${memberObj[key] > 0 ? ", Spouse" : ""}`;
    } else if (key === "husband" && proposerGender === "female") {
      string = string + `${memberObj[key] > 0 ? ", Spouse" : ""}`;
    } else if (key === "son" || key === "daughter") {
      //setting kid or kids depending on child count
      childCount += memberObj[key];
      if (childCount == 1) {
        let regex = new RegExp(", Kids" + "|" + ", Kid", "g");
        word = string.replace(regex, "");
        string = word + ", Kid";
      }
      if (childCount > 1) {
        word = string.replace(", Kid", "");
        string = word + ", Kids";
      }
    } else if (
      (key === "father" || key === "mother") &&
      !string.includes("Parents")
    ) {
      string = string + `${memberObj[key] > 0 ? ", Parents" : ""}`;
    }
  });
  const arr = string.split(", ");
  let spouseIndex = arr.indexOf("Spouse");
  if (spouseIndex !== -1) {
    // Remove "Spouse" from its current
    let spouse = arr.splice(spouseIndex, 1)[0];
    // Add "Spouse" in the second position
    arr.splice(1, 0, spouse);
  }
  string = arr.join(", ");
  return string;
}

export function rupeeFormat(number: number) {
  const rupee = new Intl.NumberFormat("en-IN", {
    style: "currency",
    currency: "INR",
    minimumFractionDigits: 0,
  });
  const formattedInRupee = rupee.format(number);
  const currencySymbol = formattedInRupee.charAt(0);
  const numbers = formattedInRupee.substring(1);
  return `${currencySymbol} ${numbers}`;
}

export function formatNumberToLakhs(number: number) {
  const lakh = 100000;
  let str = "";
  if (number > lakh) {
    str = " lakhs";
  } else str = " lakh";
  const formattedNumber = number / lakh + str;
  return <span>&#8377;{formattedNumber}</span>;
}

export function mapMembersTitle(id: string, key: string) {
  let title = "";
  if (key === "daughter" || key === "son") {
    const number = id.split(key)[1];
    switch (number) {
      case "1":
        title = `First ${key}`;
        break;
      case "2":
        title = `Second ${key}`;
        break;
      case "3":
        title = `Third ${key}`;
        break;
      case "4":
        title = `Fourth ${key}`;
        break;
      default:
        title = `${number}th ${key}`;
        break;
    }
  } else title = id;
  return title;
}

export function convertNumberToWords(num: number) {
  const words = numberToWords.toWords(num);
  return words.charAt(0).toUpperCase() + words.slice(1);
}

export const detectUserAgentInfo = async () => {
  const userAgent = navigator.userAgent;
  const browser = {
    name: navigator.appName,
    version: navigator.appVersion,
    codeName: navigator.appCodeName,
  };
  let os;
  switch (true) {
    case userAgent.indexOf("Win") !== -1:
    case navigator.platform.indexOf("Win") !== -1:
      os = "Windows";
      break;
    case userAgent.indexOf("Mac") !== -1:
      os = "macOS";
      break;
    case userAgent.indexOf("Linux") !== -1:
    case navigator.platform.indexOf("Linux") !== -1:
      os = "Linux";
      break;
    case userAgent.indexOf("Android") !== -1:
      os = "Android";
      break;
    case userAgent.indexOf("iOS") !== -1:
      os = "iOS";
      break;
    default:
      os = "Unknown";
  }
  return {
    platform: navigator.platform,
    os,
    browser,
  };
};

export const generateLogEventPayload = (
  widget_name: string,
  pageId: string,
  type?: string,
  data?: any
) => {
  let win = (window as any)["sharedConstants"];
  const logPayload = {
    sub_kind: win.product.cancer,
    widget_name,
    pageId,
    type,
    data: data,
  };
  return logPayload;
};

// generate members array for shareable proposal page
export const generateMembersArrayFromProposalData = (proposal: any) => {
  let memberDetails = proposal?.member_details || [];
  let temp: any = [];
  let youObj = {
    name: proposal?.full_name || "",
    phone: proposal?.contact || null,
    id: proposal?.gender === "male" ? "husband" : "wife",
    image:
      proposal?.gender === "male" ? memberList[0].image : memberList[1].image,
    title: "You",
    age: dayjs(new Date()).diff(proposal?.dob?.split("T")[0], "year"),
  };
  temp?.push(youObj);
  if (memberDetails.length) {
    memberDetails.forEach((member: any) => {
      if (Object.keys(member).includes("son_data")) {
        let sonData = member["son_data"].map((data: any, index: any) => ({
          ...data,
          title: mapMembersTitle(`son${index + 1}`, "Son"),
          age: dayjs(new Date()).diff(data.dob.split("T")[0], "year"),
          id: "son",
          image: memberList[3].image,
        }));
        temp = [...temp, ...sonData];
      } else if (Object.keys(member).includes("daughter_data")) {
        let daughterData = member["daughter_data"].map(
          (data: any, index: any) => ({
            ...data,
            title: mapMembersTitle(`daughter${index + 1}`, "Daughter"),
            age: dayjs(new Date()).diff(data.dob.split("T")[0], "year"),
            id: "daughter",
            image: memberList[4].image,
          })
        );
        temp = [...temp, ...daughterData];
      } else if (member.hasOwnProperty("wife_data")) {
        let wifeData = member["wife_data"];
        wifeData["title"] = proposal?.gender !== "male" ? "You" : "Wife";
        wifeData["age"] = dayjs(new Date()).diff(
          member["wife_data"]?.dob?.split("T")[0],
          "year"
        );
        wifeData["id"] = "wife";
        wifeData["image"] = memberList[1].image;
        temp?.push(wifeData);
      } else if (member.hasOwnProperty("husband_data")) {
        let husbandData = member["husband_data"];
        husbandData["title"] = proposal?.gender === "male" ? "You" : "Husband";
        husbandData["age"] = dayjs(new Date()).diff(
          member["husband_data"]?.dob?.split("T")[0],
          "year"
        );
        husbandData["id"] = "husband";
        husbandData["image"] = memberList[0].image;
        temp?.push(husbandData);
      } else if (member.hasOwnProperty("mother_data")) {
        let motherData = member["mother_data"];
        motherData["title"] = "Mother";
        motherData["age"] = dayjs(new Date()).diff(
          motherData?.dob?.split("T")[0],
          "year"
        );
        motherData["id"] = "mother";
        motherData["image"] = memberList[5].image;
        temp?.push(motherData);
      } else if (member.hasOwnProperty("father_data")) {
        let fatherData = member["father_data"];
        fatherData["title"] = "Father";
        fatherData["age"] = dayjs(new Date()).diff(
          fatherData?.dob?.split("T")[0],
          "year"
        );
        fatherData["id"] = "father";
        fatherData["image"] = memberList[4].image;
        temp?.push(fatherData);
      }
    });
    // temp.unshift(youObj);
  }
  return temp;
};
