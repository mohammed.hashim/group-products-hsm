import { downloadPolicyPdf } from "./actions/group-cancer/actions";
import { API_URL } from "./config";

export const rootUrl = API_URL;

export const apiUrls = {
  pa: {
    getPlansUrl: `${rootUrl}/plans/getPlans`,
    getSinglePlanUrl: `${rootUrl}/plans/getPlans/plan_id`,
    postProposalDataUrl: `${rootUrl}/plans/saveProposal`,
    checkoutUrl: `${rootUrl}/api/pg/checkout`,
    transactionUrl: `${rootUrl}/api/pg/transaction`,
  },
  cancer: {
    getPlansUrl: `${rootUrl}/cancer/plans/getPlans/ages&page`,
    getSinglePlanUrl: `${rootUrl}/cancer/plans/getPlans/plan_id`,
    postProposalDataUrl: `${rootUrl}/cancer/plans/saveProposal`,
    checkoutUrl: `${rootUrl}/api/pg/checkout`,
    transactionUrl: `${rootUrl}/api/pg/transaction`,
    getProposalUrl: `${rootUrl}/cancer/plans/getProposal/proposal_id`,
    downloadPolicyPdf: `${rootUrl}/getPdf`,
    getStateDataUrl: `${rootUrl}/states`,
    getConfigUrl: `${rootUrl}/getConfig`,
  },
  groupPa: {
    getPlansUrl: `${rootUrl}/groupPa/plans/getPlans/ages&page`,
    getSinglePlanUrl: `${rootUrl}/groupPa/plans/getPlans/plan_id`,
    postProposalDataUrl: `${rootUrl}/groupPa/plans/saveProposal`,
    checkoutUrl: `${rootUrl}/api/pg/checkout`,
    transactionUrl: `${rootUrl}/api/pg/transaction`,
    getProposalUrl: `${rootUrl}/groupPa/plans/getProposal/proposal_id`,
    downloadPolicyPdf: `${rootUrl}/getPdf`,
  },
};
