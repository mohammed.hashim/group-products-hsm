export interface IBenefits {
  id: string;
  name: string;
  title: string;
  description: string[];
  cover: number;
}

export interface IUser {
  name: string;
  email: string;
  dob: Date;
  contact: string;
}

export interface IPayment {
  amount: number;
  source: string;
  order_id: string;
  userData: IUser;
  handler: Function;
  onModaldismiss: any;
}

export interface IPlan {
  planId: string;
  sumInsured: number;
  premiumAmount: number;
  isPopular: boolean;
  minimumIncomeReq: boolean;
  minimumIncome: number;
  benefits: IBenefits[];
}

export interface proposerData {
  address: string;
  city: string;
  dob: undefined;
  email_id: string;
  first_name: string;
  landmark: string;
  last_name: string;
  mobile_no: string;
  nominee_age: string;
  nominee_name: string;
  nominee_relation: string;
  pan_id: string;
  pin_code: string;
  state: string;
  gender?: string;
}
