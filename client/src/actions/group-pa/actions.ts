import {
  INCREMENT_CURRENT_STEP,
  DECREMENT_CURRENT_STEP,
  MEMBER_ARRAY,
  GET_PLANS,
  GET_SINGLE_PLAN,
  SET_PROPOSAL_DATA,
  CANCER_FAIL,
  CANCER_LOADING,
  PROPOSAL_DATA_SUCCESS,
  PREQUOTE_DATA,
  DOWNLOAD_POLICY_PDF,
  MEMBER_OBJECT,
  SELECTED_GENDER,
  EDIT_CLICKED,
  RESET_STATE,
  SET_TOTAL_STEPS,
  SET_STATES_DATA,
} from "./types";
import { Dispatch } from "redux";
import { get, post } from "@/utils/apiUtils";
import { apiUrls } from "@/services";
import { IPayment, IPlan } from "@/interfaces/paInterfaces";
import dayjs from "dayjs";
import { saveAs } from "file-saver";
import { SET_CONFIG_VARIABLES } from "../group-cancer/types";
import { generateMembersArrayFromProposalData } from "@/utils/common-utils";

export const goToPrequoteNextScreen = (dispatch: Dispatch) => {
  dispatch({
    type: INCREMENT_CURRENT_STEP,
  });
};

export const goToPrequotePreviousScreen = (dispatch: Dispatch) => {
  dispatch({
    type: DECREMENT_CURRENT_STEP,
  });
};

export const editButtonClicked = (dispatch: Dispatch) => {
  dispatch({
    type: EDIT_CLICKED,
  });
};

export const getGenderDetails = (genderDetails: any, dispatch: Dispatch) => {
  dispatch({
    type: SELECTED_GENDER,
    payload: genderDetails,
  });
};

export const getSelectedMembersObject = (
  memberObject: any,
  dispatch: Dispatch
) => {
  console.log(memberObject);
  dispatch({
    type: MEMBER_OBJECT,
    payload: memberObject,
  });
};

export const getSelectedMemberArray = (
  memberArray: any,
  dispatch: Dispatch
) => {
  dispatch({
    type: MEMBER_ARRAY,
    payload: memberArray,
  });
};

export const getSelectedMemberDetails = (
  prequoteData: any,
  dispatch: Dispatch
) => {
  dispatch({
    type: PREQUOTE_DATA,
    payload: prequoteData,
  });
};

// api actions
export const getPlans = (ages: Array<number>, currentPage: number) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    let url = apiUrls.cancer.getPlansUrl;
    url = url.replace("ages", ages.toString());
    url = url.replace("page", currentPage.toString());
    get(url)
      .then((res) => {
        dispatch({
          type: GET_PLANS,
          payload: res.data.planData,
        });
      })
      .catch((e) => {
        dispatch({
          type: CANCER_FAIL,
          payload: true,
        });
      });
  };
};

export const getSinglePlan = (plan: any) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: GET_SINGLE_PLAN,
      payload: plan,
    });
  };
};

export const getProposalData = (
  id: string,
  cb?: any,
  memberArrayLength?: any,
  setSelectedState?: any,
  setLoading?: any
) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    get(`${apiUrls.groupPa.getProposalUrl.replace("proposal_id", id)}`)
      .then((res) => {
        if (res.data.success && Object.keys(res.data.proposal).length) {
          const { full_name, dob, email, contact, other_user_details } =
            res.data.proposal;
          dispatch({
            type: SET_PROPOSAL_DATA,
            payload: res.data,
          });
          if (memberArrayLength === 0) {
            let obj = generateMembersArrayFromProposalData(res.data.proposal);
            dispatch({
              type: MEMBER_ARRAY,
              payload: obj,
            });
          }
          const proposerFormData = JSON.parse(
            localStorage.getItem("proposalFormData") || "{}"
          );
          // sets the form fields from cb for shareable link
          if (Object.keys(proposerFormData).length === 0) {
            setSelectedState(other_user_details?.state || "");
            cb({
              first_name: full_name?.split(" ")[0] || "",
              last_name: full_name?.split(" ")[1] || "",
              dob: dob === null ? dayjs(new Date()) : dayjs(dob),
              email_id: email || "",
              mobile_no: contact || null,
              address: other_user_details?.address || "",
              city: other_user_details?.city || "",
              landmark: other_user_details?.landmark || "",
              nominee_age: other_user_details?.nominee_age || "",
              nominee_name: other_user_details?.nominee_name || "",
              nominee_relation: other_user_details?.nominee_relation || "",
              pan_id: other_user_details?.pan_id || "",
              pin_code: other_user_details?.pin_code || "",
              state: other_user_details?.state || "",
            });
            setTimeout(() => {
              setLoading(false);
            }, 100);
          }
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  };
};

export const postProposalData = (data: any) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    post(`${apiUrls.groupPa.postProposalDataUrl}`, data)
      .then((res) => {
        if (res.data.success) {
          const { proposal } = res.data;
          localStorage.setItem("localProposalData", JSON.stringify(proposal));
          dispatch({
            type: SET_PROPOSAL_DATA,
            payload: res.data,
          });
          dispatch({
            type: PROPOSAL_DATA_SUCCESS,
            payload: res.data.success,
          });
        }
      })
      .catch((e) => {
        dispatch({
          type: CANCER_FAIL,
          payload: true,
        });
      });
  };
};

export const proposalCheckout = (
  data: any,
  cb: any,
  postPaymentHandler: any,
  setPaymentProcess: any
) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_FAIL,
      payload: false,
    });
    post(`${apiUrls.groupPa.checkoutUrl}`, data)
      .then((res) => {
        if (res.data.success) {
          let paymentRequest: IPayment = {
            amount: data.amount,
            source: "flipkart",
            order_id: res.data.order.id,
            handler: postPaymentHandler,
            userData: {
              name: res.data.proposalData.full_name,
              email: res.data.proposalData.email,
              dob: res.data.proposalData.dob.slice(0, 10),
              contact: res.data.proposalData.contact,
            },
            onModaldismiss: () => setPaymentProcess(false),
          };
          cb(paymentRequest);
        }
      })
      .catch((e) => {
        console.log(e.message);
        dispatch({
          type: CANCER_FAIL,
          payload: true,
        });
      });
  };
};

export const setProposalStatus = (status: boolean) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    dispatch({
      type: PROPOSAL_DATA_SUCCESS,
      payload: status,
    });
  };
};

export const downloadPolicyPdf = () => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    get(`${apiUrls.groupPa.downloadPolicyPdf}`, { responseType: "blob" })
      .then((res) => {
        const pdfBlob = new Blob([res.data], { type: "application/pdf" });
        saveAs(pdfBlob, "COI.pdf");
        dispatch({
          type: DOWNLOAD_POLICY_PDF,
        });
      })
      .catch((e) => {
        dispatch({
          type: CANCER_FAIL,
          payload: true,
        });
      });
  };
};

export const getConfig = () => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    get(`${apiUrls.cancer.getConfigUrl}`)
      .then((res) => {
        dispatch({
          type: SET_CONFIG_VARIABLES,
          payload: res.data,
        });
      })
      .catch((e) => {
        dispatch({
          type: CANCER_FAIL,
          payload: true,
        });
      });
  };
};
export const setTotalSteps = (payload: number) => {
  return {
    payload: payload,
    type: SET_TOTAL_STEPS,
  };
};

export const getStatesData = () => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: CANCER_LOADING,
    });
    get(`${apiUrls.cancer.getStateDataUrl}`)
      .then((res) => {
        dispatch({
          type: SET_STATES_DATA,
          payload: res.data.data,
        });
      })
      .catch((e) => {
        dispatch({
          type: CANCER_FAIL,
          payload: true,
        });
      });
  };
};

export const resetState = () => {
  return {
    type: RESET_STATE,
  };
};
