import {
  GET_PLANS,
  GET_SINGLE_PLAN,
  SET_PROPOSAL_DATA,
  PA_FAIL,
  PA_LOADING,
  PROPOSAL_DATA_SUCCESS,
} from "./types";
import { get, post } from "@/utils/apiUtils";
import { Dispatch } from "redux";
import { apiUrls } from "@/services";
import { IPayment, IPlan } from "@/interfaces/paInterfaces";

export const getPlans = () => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: PA_LOADING,
    });
    get(`${apiUrls.pa.getPlansUrl}`)
      .then((res) => {
        dispatch({
          type: GET_PLANS,
          payload: res.data.data,
        });
      })
      .catch((e) => {
        dispatch({
          type: PA_FAIL,
          payload: true,
        });
      });
  };
};

export const getSinglePlan = (planId: string) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: PA_LOADING,
    });
    get(`${apiUrls.pa.getSinglePlanUrl.replace("plan_id", planId)}`)
      .then((res) => {
        dispatch({
          type: GET_SINGLE_PLAN,
          payload: res.data.plan,
        });
      })
      .catch((e) => {
        dispatch({
          type: PA_FAIL,
          payload: true,
        });
      });
  };
};

export const postProposalData = (data: any) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: PA_LOADING,
    });
    post(`${apiUrls.pa.postProposalDataUrl}`, data)
      .then((res) => {
        if (res.data.success) {
          dispatch({
            type: SET_PROPOSAL_DATA,
            payload: res.data,
          });
          dispatch({
            type: PROPOSAL_DATA_SUCCESS,
            payload: res.data.success,
          });
        }
      })
      .catch((e) => {
        dispatch({
          type: PA_FAIL,
          payload: true,
        });
      });
  };
};

export const proposalCheckout = async (
  data: any,
  cb: any,
  postPaymentHandler: any
) => {
  try {
    await post(`${apiUrls.pa.checkoutUrl}`, data).then((res) => {
      if (res.data.success) {
        let paymentRequest: IPayment = {
          amount: data.amount,
          source: "flipkart",
          order_id: res.data.order.id,
          handler: postPaymentHandler,
          userData: {
            name: "Test Name",
            email: "test@test.com",
            dob: new Date("07-04-1989"),
            contact: "9898989898",
          },
          onModaldismiss: () => {},
        };
        cb(paymentRequest);
      }
    });
  } catch (error) {}
};
