/* RAZORPAY CREDENTIALS */
export const razorpayCredentials: any = {
  uat: {
    CLIENT_ID: "rzp_test_9PyDROgP6Dl9MG",
    ACCOUNT_ID: "acc_ABNG75iuj2gSfE",
  },
  production: {},
};
const localPort = 8090;
const PORT = process.env.PORT || localPort;
export const API_URL =
  process.env.REACT_APP_API_URL || `http://localhost:${PORT}`;
