import { Space, Spin } from "antd";
const content = {
  margin: "20px 0",
  marginBottom: "20px",
  padding: "30px 50px",
  textalign: "center",
  borderRadius: "4px",
};

const AppLoader = () => (
  <div
    style={{
      textAlign: "center",
      height: "100vh",
    }}
  >
    <Space
      direction="vertical"
      style={{
        width: "100%",
      }}
    >
      <Spin tip="Loading..." size="large">
        <div style={content} />
      </Spin>
    </Space>
  </div>
);

export default AppLoader;
