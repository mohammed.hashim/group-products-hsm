import React from "react";

interface Iprops {
  imgSrc?: any;
  hideImage?: boolean;
  subtitle1?: string;
  subtitle2?: string;
  isMobile?: boolean;
}

const AdInfoImage = (props: Iprops) => {
  return (
    <div
      className={
        props?.isMobile
          ? "d-flex flex-direction-col"
          : "d-flex flex-direction-col align-item-center"
      }
    >
      {!props.hideImage && (
        <div>
          <img src={props?.imgSrc} height={"40px"} alt="logo" />
        </div>
      )}
      <span
        hidden={!props.subtitle1}
        className={props?.isMobile ? "fs-12 font-bold" : "fs-16"}
      >
        {props.subtitle1}
      </span>
      <span
        hidden={!props.subtitle2}
        className={
          props?.isMobile ? "text-muted fs-10" : "text-muted text-center"
        }
      >
        {props.subtitle2}
      </span>
    </div>
  );
};

export default AdInfoImage;
