import { getImageURL } from "@/utils/common-utils";
import { Fragment } from "react";
import { Link } from "react-router-dom";

import "./footer.css";
const coverstackLogo = getImageURL("icons/coverstack_blue.svg");
interface Iprops {}

function Footer(props: Iprops) {
  return (
    <Fragment>
      <div className="footer footer-flex">
        <div>
          Copyright © 2023 <span> | </span> Powered by{" "}
        </div>
        <Link to="https://www.coverstack.in/">
          <img
            className="footer-logo"
            src={coverstackLogo}
            alt="CoverStack.in"
          />
        </Link>
        <div>
          <span className="mobile-hide"> | </span> All Rights Reserved
        </div>
      </div>
    </Fragment>
  );
}

export default Footer;
