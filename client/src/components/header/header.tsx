import { goToPrequotePreviousScreen } from "@/actions/group-cancer/actions";
import { useProductState } from "@/hooks/useProductState";
import { RootStore } from "@/store";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button, Drawer, Grid } from "antd";
import { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import "./header.css";

interface Iprops {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  title?: string;
  showBackIcon?: boolean;
  showInfoIcon?: boolean;
  editFunction?: any;
  lhsDropdown?: JSX.Element;
  rhsDropdown?: any;
  filter?: any;
  hideLogo?: boolean;
}

function Header(props: Iprops) {
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);
  const navigate = useNavigate();
  const location = useLocation();
  const productState = useProductState(props.type);
  const { currentScreen, plans } = productState;
  const dispatch = useDispatch();
  const { useBreakpoint } = Grid;
  const isMobile = useBreakpoint().xs;
  const cancerState = useSelector((state: RootStore) => state.cancer);
  const paState = useSelector((state: RootStore) => state.groupPa);
  const setPathName = (type: string) => {
    switch (type) {
      case "CANCER":
        return "/group-cancer/";
        break;
      case "CRITICAL":
        return "/critical-illness/";
        break;
      case "CARDIAC":
        return "/group-cancer/";
        break;
      case "HEALTH":
        return "/group-health/";
        break;
      case "HOSPICASH":
        return "/hospicash/";
        break;
    }
  };
  const handleBack = () => {
    if (
      props.type == "CANCER" ||
      props.type == "CRITICAL" ||
      props.type == "CARDIAC" ||
      props.type == "HEALTH" ||
      props.type == "HOSPICASH"
    ) {
      if (
        currentScreen > 0 &&
        location.pathname == `${setPathName(props.type)}`
      ) {
        goToPrequotePreviousScreen(dispatch);
      } else if (
        currentScreen == 4 &&
        location.pathname == `${setPathName(props.type)}results`
      ) {
        navigate(`${setPathName(props.type)}`);
      } else {
        navigate(-1);
      }
    }
    console.log(location);

    if (props.type == "PA") {
      if (currentScreen > 0 && location.pathname == "/personal-accident/") {
        goToPrequotePreviousScreen(dispatch);
      } else if (
        currentScreen == 3 &&
        location.pathname == "/personal-accident/results"
      ) {
        navigate("/personal-accident/");
      } else {
        navigate(-1);
      }
    }
  };

  return (
    <Fragment>
      <div className="headerContainer headerContainer-m">
        <div className="d-flex gap-16 align-item-center">
          {props.showBackIcon && (
            <div className="backIcon" onClick={handleBack}>
              {cancerState.currentScreen != 1 && <ArrowLeftOutlined />}
            </div>
          )}
          {!props.hideLogo && (
            <div>
              <img src="/logo-white.png" className="header-logo" />
            </div>
          )}
        </div>
        <div className="backAndTitleContainer">
          {props.title && <div className="title text-capital">{props.title.toLocaleLowerCase()}</div>}
        </div>

        <div className="header-right">
          {isMobile && (
            <div className="infoIcon">
              <a onClick={() => setOpenDrawer(!openDrawer)}>X</a>
            </div>
          )}
        </div>
      </div>
      <Drawer
        placement={"top"}
        width={500}
        height={"auto"}
        closable={false}
        open={openDrawer}
        onClose={() => setOpenDrawer(!openDrawer)}
        className="fk-border"
      >
        <p onClick={() => setOpenDrawer(!openDrawer)} className="fk-close">X</p>
        <p className="fk-msg1" >
          This will redirect to
        </p>
        <p className="fk-msg2" >
          Flipkart's Insurance page.
        </p>
        <div className="d-flex justify-content-center">
          <Button
            className="fk-btn"
            href="https://www.flipkart.com/rv/insurance/home"
          >
            Yes, Proceed
          </Button>
        </div>
      </Drawer>
      <div className="below-header">{props.filter && props.filter}</div>
    </Fragment>
  );
}

export default Header;
