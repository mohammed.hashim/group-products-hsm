import React from "react";
import "./card.css";

type CardProps = {
  children: React.ReactNode;
  style?: any;
};
//UNUSED custom card component
const Card = (props: CardProps) => {
  return (
    <div style={props.style}>
      <div
        style={{
          fontSize: "16px",
          //  marginTop: '16px',
          //  minHeight: '80px',
          borderRadius: "10px",
          boxShadow: "0 8px 24px -8px rgb(0 0 0 / 11%)",
          backgroundColor: "white",
          //  paddingTop: '8px',
          padding: "2vh 1vw",
          position: "relative",
        }}
      >
        {props.children}
      </div>
    </div>
  );
};

export default Card;
