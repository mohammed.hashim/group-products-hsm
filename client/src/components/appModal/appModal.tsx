import React, { useState } from "react";
import { CloseCircleOutlined } from "@ant-design/icons";
import { Button, Result, Typography, Modal } from "antd";

const { Paragraph, Text } = Typography;

const AppModal = (props: any) => {
  const [modal, setModal] = useState(false);

  React.useEffect(() => {
    setModal(props.status);
  }, []);

  return (
    <>
      <Modal
        title=""
        centered
        open={modal}
        onOk={() => {
          props.setPaymentProcess(false);
          setModal(false);
        }}
        onCancel={() => {
          props.setPaymentProcess(false);
          setModal(false);
        }}
      >
        <Result
          status="error"
          title="Something went wrong"
          subTitle="Please try again."
          extra={[]}
        ></Result>
      </Modal>
    </>
  );
};

export default AppModal;
