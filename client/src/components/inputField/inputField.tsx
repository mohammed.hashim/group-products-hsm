import React, { useRef } from "react";

import "./inputField.css";
interface Iprops {
  handleChange: Function;
  value: string;
  id: string;
  type: string;
  icon?: any;
  label: string;
  required?: boolean;
}

function InputField(props: Iprops) {
  const ref = useRef();
  return (
    <div className="inputBox">
      <input
        onChange={(e) => props.handleChange(e)}
        value={props.value}
        className={`${props.value ? "has-value" : ""}`}
        onFocus={(e) => (props.id === "dob" ? (e.target.type = "date") : "")}
        onBlur={(e) => (e.target.type = "text")}
        id={props.id}
        type={props.type}
        required={props.required || false}
      />
      {props.icon && <span>icon</span>}
      <label htmlFor={props.id}>{props.label}</label>
    </div>
  );
}

export default InputField;
