import React, { Fragment, useState } from "react";

const InputUsers = () => {
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");

  const onSubmit = async (e) => {
    e.preventDefault();
    try {
      const body = { name, location };
      const response = await fetch("http://localhost:8080/users", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });
      window.location = "/";
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Fragment>
      <h1>Input Users</h1>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          placeholder="Name"
          value={name}
          required
          onChange={(e) => setName(e.target.value)}
        />
        <input
          type="text"
          placeholder="Location"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
          required
        />
        <button>Add</button>
      </form>
    </Fragment>
  );
};

export default InputUsers;
