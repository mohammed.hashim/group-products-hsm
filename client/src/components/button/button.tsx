import React from "react";
import "./button.css";

interface Iprops {
  title?: string;
  customClass?: string;
  handleOnClick?: any;
  icon?: string;
}

export default function Button(props: Iprops) {
  return (
    <button
      className={
        !props.customClass && !props.icon ? "default" : `${props.customClass}`
      }
      onClick={props.handleOnClick}
    >
      {props.icon && <img src={props.icon} width="16px" height="16px" />}
      {props.title}
    </button>
  );
}
