import {
  PA_FAIL,
  PA_LOADING,
  GET_PLANS,
  GET_SINGLE_PLAN,
  SET_PROPOSAL_DATA,
  PROPOSAL_DATA_SUCCESS,
} from "../actions/pa/types";

interface DefaultStateI {
  loading: boolean;
  plans?: any;
  singlePlan?: any;
  isProposalSuccess?: boolean;
  proposalData?: any;
  paFail: boolean;
  plansPagination: any
}

const defaultState: DefaultStateI = {
  loading: false,
  plans: [],
  singlePlan: null,
  isProposalSuccess: false,
  proposalData: null,
  paFail: false,
  plansPagination: {
    size: 1,
    currentPage: 1
  },
};

const paReducer = (
  state: DefaultStateI = defaultState,
  action: any
): DefaultStateI => {
  switch (action.type) {
    case PA_FAIL:
      return {
        ...state,
        paFail: action.payload,
      };
    case PA_LOADING:
      return {
        ...state,
        loading: true,
      };
    case GET_PLANS:
      return {
        ...state,
        loading: false,
        plans: action.payload,
      };
    case GET_SINGLE_PLAN:
      return {
        ...state,
        loading: false,
        singlePlan: action.payload,
      };
    case SET_PROPOSAL_DATA:
      return {
        ...state,
        loading: false,
        proposalData: action.payload,
      };
    case PROPOSAL_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        isProposalSuccess: action.payload,
      };
    default:
      return state;
  }
};

export default paReducer;
