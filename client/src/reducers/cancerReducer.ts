import {
  INCREMENT_CURRENT_STEP,
  DECREMENT_CURRENT_STEP,
  MEMBER_ARRAY,
  CANCER_FAIL,
  CANCER_LOADING,
  GET_PLANS,
  GET_SINGLE_PLAN,
  SET_PROPOSAL_DATA,
  PROPOSAL_DATA_SUCCESS,
  CANCER_NOT_LOADING,
  PREQUOTE_DATA,
  DOWNLOAD_POLICY_PDF,
  SELECTED_GENDER,
  MEMBER_OBJECT,
  EDIT_CLICKED,
  RESET_STATE,
  SET_CONFIG_VARIABLES,
  SET_STATES_DATA,
  SET_PAGINATION,
  RESET_PAGINATION,
} from "@/actions/group-cancer/types";

interface DefaultStateI {
  currentScreen: number;
  totalSteps: number;
  selectedGender: any;
  selectedMembersObject: any;
  selectedMembersArray: any;
  prequoteData?: any;
  loading: boolean;
  plans?: any;
  singlePlan?: any;
  isProposalSuccess?: boolean;
  proposalData?: any;
  cancerFailState: boolean;
  isEditClicked: boolean;
  configVariables: any;
  statesData: any;
  plansPagination: any;
}

const defaultState: DefaultStateI = {
  loading: false,
  currentScreen: 1,
  totalSteps: 4,
  selectedGender: null,
  selectedMembersObject: {
    son: 0,
    daughter: 0,
    husband: 0,
    wife: 0,
    father: 0,
    mother: 0,
  },
  selectedMembersArray: [],
  prequoteData: null,
  plans: [],
  singlePlan: null,
  isProposalSuccess: false,
  proposalData: null,
  cancerFailState: false,
  isEditClicked: false,
  configVariables: false,
  plansPagination: {
    size: 1,
    currentPage: 1,
    totalPage: 1,
  },
  statesData: [],
};

const cancerReducer = (
  state: DefaultStateI = defaultState,
  action: any
): DefaultStateI => {
  switch (action.type) {
    case INCREMENT_CURRENT_STEP:
      if (state.currentScreen >= 4) {
        return { ...state, currentScreen: (state.currentScreen = 1) };
      } else {
        return { ...state, currentScreen: state.currentScreen + 1 };
      }
    case DECREMENT_CURRENT_STEP:
      if (state.currentScreen <= 1) {
        return state;
      } else {
        return {
          ...state,
          currentScreen: state.currentScreen - 1,
        };
      }
    case EDIT_CLICKED:
      return { ...state, isEditClicked: !state.isEditClicked };
    case SELECTED_GENDER:
      return {
        ...state,
        selectedGender: action.payload,
      };
    case MEMBER_OBJECT:
      return {
        ...state,
        selectedMembersObject: action.payload,
      };
    case MEMBER_ARRAY:
      return {
        ...state,
        loading: false,
        selectedMembersArray: action.payload,
      };
    case PREQUOTE_DATA:
      return {
        ...state,
        prequoteData: action.payload,
      };
    case CANCER_FAIL:
      return {
        ...state,
        cancerFailState: action.payload,
      };
    case CANCER_LOADING:
      return {
        ...state,
        loading: true,
      };
    case CANCER_NOT_LOADING:
      return {
        ...state,
        loading: false,
      };
    case GET_PLANS:
      return {
        ...state,
        loading: false,
        plans: action.payload,
      };
    case GET_SINGLE_PLAN:
      return {
        ...state,
        loading: false,
        singlePlan: action.payload,
      };
    case SET_PROPOSAL_DATA:
      return {
        ...state,
        loading: false,
        proposalData: action.payload,
      };
    case PROPOSAL_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        isProposalSuccess: action.payload,
      };
    case SET_CONFIG_VARIABLES:
      return {
        ...state,
        loading: false,
        configVariables: action.payload,
      };
    case SET_STATES_DATA:
      return {
        ...state,
        loading: false,
        statesData: action.payload,
      };
    case SET_PAGINATION:
      return {
        ...state,
        plansPagination: action.payload,
      };
    case RESET_PAGINATION:
      return {
        ...state,
        plansPagination: {
          size: 1,
          currentPage: 1,
          totalPage: 1,
        },
      };
    case RESET_STATE:
      localStorage.clear();
      return defaultState;
    default:
      return state;
  }
};

export default cancerReducer;
