import { combineReducers } from "redux";
import cancerReducer from "./cancerReducer";
import paReducer from "./paReducer";
import groupPaReducer from "./groupPaReducer";

const RootReducer = combineReducers({
  pa: paReducer,
  cancer: cancerReducer,
  groupPa: groupPaReducer,
});

export default RootReducer;
