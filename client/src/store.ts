import { legacy_createStore as createStore, applyMiddleware } from "redux";
import RootReducer from "./reducers/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import logger from "redux-logger";

const middlewares = [thunk, logger];

const saveToLocalStorage = (state: any) => {
  try {
    localStorage.setItem("state", JSON.stringify(state));
  } catch (e) {
    console.error(e);
  }
};

const loadFromLocalStorage = () => {
  try {
    const stateStr = localStorage.getItem("state");
    return stateStr ? JSON.parse(stateStr) : undefined;
  } catch (e) {
    console.error(e);
    return undefined;
  }
};

const persistedStore = loadFromLocalStorage();

const store = createStore(
  RootReducer,
  persistedStore,
  composeWithDevTools(applyMiddleware(...middlewares))
);

store.subscribe(() => {
  saveToLocalStorage(store.getState());
});

export type RootStore = ReturnType<typeof RootReducer>;

export default store;
