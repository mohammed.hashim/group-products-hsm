export const ROOT = {
  logType: {
    view: "viewed",
    submit: "submitted",
  },
  logKind: {
    group_product: "group_product",
  },
  product: {
    cancer: "cancer",
    personal_accident: "personal_accident",
    cardiac: "cardiac",
    critical_illness: "critical_illness",
  },
  widget: {
    gender_view: "gender_view",
    gender_gender_submit: "gender_gender_submit",
    member_select_view: "member_select_view",
    member_select_submit: "member_select_submit",
    member_age_view: "member_age_view",
    member_age_submit: "member_age_submit",
    user_details_view: "user_details_view",
    user_details_submit: "user_details_submit",
    view_quotes: "view_quotes",
    plan_details: "plan_details",
    buy_now: "buy_now",
    proposer_details: "proposer_details",
    members_details: "members_details",
    review: "review",
    review_proposer_details_edit: "review_proposer_details_edit",
    review_member_details_edit: "review_member_details_edit",
    make_payment_click: "make_payment_click",
    payment_details: "payment_details",
    download_pdf: "download_pdf",
  },
  pageId: {
    pre_quote_page: "pre_quote_page",
    quote_page: "quote_page",
    proposal_form: "proposal_form",
  },
};

(window as any)["sharedConstants"] = ROOT;
