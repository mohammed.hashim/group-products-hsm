import { API_URL } from "@/config";

export const pg_checkout: string = API_URL + "/api/pg/checkout";
export const pg_transaction_confirmation: string =
  API_URL + "/api/pg/paymentverification";
