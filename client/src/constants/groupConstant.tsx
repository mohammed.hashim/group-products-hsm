export const products = [
  {
    id: 1,
    title: "Personal Accident",
    name: "personalAccident",
  },
  {
    id: 2,
    title: "Group Cancer",
    name: "groupCancer",
  },
  {
    id: 3,
    title: "Critical Illness",
    name: "criticalIllness",
  },
  {
    id: 4,
    title: "Cardiac",
    name: "cardiac",
  },
  {
    id: 5,
    title: "Group Health",
    name: "groupHealth",
  },
  {
    id: 4,
    title: "Hospicash",
    name: "hospicash",
  },
];

export const gender = [
  {
    id: "male",
    title: "Male",
    image: "/static/img/icons/health/member-male.svg",
  },
  {
    id: "female",
    title: "Female",
    image: "/static/img/icons/health/member-female.svg",
  },
];

export const memberList = [
  {
    id: "husband",
    title: "Husband",
    image: "/static/img/icons/health/member-male.svg",
    age: "",
  },
  {
    id: "wife",
    title: "Wife",
    image: "/static/img/icons/health/member-female.svg",
    age: "",
  },
  {
    id: "daughter",
    title: "Daughter",
    image: "/static/img/icons/health/member-daughter.svg",
    age: "",
  },
  {
    id: "son",
    title: "Son",
    image: "/static/img/icons/health/member-son.svg",
    age: "",
  },
  {
    id: "father",
    title: "Father",
    image: "/static/img/icons/health/member-father.svg",
    age: "",
  },
  {
    id: "mother",
    title: "Mother",
    image: "/static/img/icons/health/member-mother.svg",
    age: "",
  },
];
