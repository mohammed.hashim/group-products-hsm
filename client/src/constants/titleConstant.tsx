export const titleConstant = {
  CANCER: "Get the best Cancer protect Health Insurance Policies",
  PA: "Get the best Personal Accident Insurance Policies",
  CRITICAL: "Get the best Critical illness Insurance Policies",
  CARDIAC: "Get the best Cardiac Insurance Policies",
  HEALTH: "Get the best Group Health Insurance Policies",
  HOSPICASH: "Get the best Hospicash Insurance Policies",
};
