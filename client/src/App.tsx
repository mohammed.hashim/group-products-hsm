import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import GcMainRoutes from "./Views/group-cancer/groupCancerRoutes";
// import PaGrpMainRoutes from "./Views/group-pa/screens/gcProposal/gcProposal/groupCancerRoutes";
import Homepage from "./Views/homepage/Homepage";
import "@/assets/css/app.scss";

function App() {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(
      window.matchMedia && window.matchMedia("(max-width: 820px)").matches
    );
  }, []);
  return (
    <div className={isMobile ? "mobile-view" : "desktop-view"}>
      <Routes>
        <Route path="/" element={<Homepage isMobile={isMobile} />} />
        <Route
          path="/group-cancer/*"
          element={<GcMainRoutes type={"CANCER"} />}
        />
        <Route
          path="/personal-accident/*"
          element={<GcMainRoutes type={"PA"} />}
        />
        <Route
          path="/critical-illness/*"
          element={<GcMainRoutes type={"CRITICAL"} />}
        />
        <Route path="/cardiac/*" element={<GcMainRoutes type={"CARDIAC"} />} />
        <Route
          path="/group-health/*"
          element={<GcMainRoutes type={"HEALTH"} />}
        />
        <Route
          path="/hospicash/*"
          element={<GcMainRoutes type={"HOSPICASH"} />}
        />
      </Routes>
    </div>
  );
}

export default App;
