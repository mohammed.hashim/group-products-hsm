import { RootStore } from "@/store";
import { useSelector } from "react-redux";

export const useProductState = (
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH"
) => {
  const state = useSelector((state: RootStore) => {
    switch (type) {
      case "CANCER":
        return state.cancer;
      case "PA":
        return state.groupPa;
      case "CRITICAL":
        return state.cancer;
      case "CARDIAC":
        return state.cancer;
      case "HEALTH":
        return state.cancer;
      case "HOSPICASH":
        return state.cancer;
    }
  });
  return state;
};
