import React, { useState } from "react";
import "./ageModalItem.css";

interface Iprops {
  member?: string;
  onCloseModal?: any;
  isOpen?: boolean;
  selectedAge?: number;
  handleAge?: any;
  id?: string;
  isAgeError?: boolean;
}

export default function AgeModalItem(props: Iprops) {
  var adultAge = [];
  var childAge = [];

  for (let i = 18; i <= 100; i++) {
    adultAge.push(i);
  }

  for (let i = 1; i <= 30; i++) {
    childAge.push(i);
  }

  return (
    <div className="age-modal">
      <div className="modal-box">
        <div className="modal-header">
          <div onClick={() => props.onCloseModal(false)} className="close-btn">
            <img
              src="/static/img/icons/health/close-bold.svg"
              width="18px"
              height="18px"
            />
          </div>
          <div className="modal-heading">Select Age</div>
        </div>
        <div className="age-modal-box">
          <div className="age-select-container">
            {props.id == "daughter" || props.id == "son"
              ? childAge.map(function (item, i) {
                  return (
                    <div
                      onClick={() => props.handleAge(item)}
                      className={`age-select-div ${
                        props.selectedAge === item ? "selected-age" : "null"
                      }`}
                      key={i}
                    >
                      <div className="select-item">{item}</div>
                      <div className="select-sub-heading">yrs</div>
                    </div>
                  );
                })
              : adultAge.map(function (item, i) {
                  return (
                    <div
                      onClick={() => props.handleAge(item)}
                      className={`age-select-div ${
                        props.selectedAge === item ? "selected-age" : "null"
                      }`}
                      key={i}
                    >
                      <div className="select-item">{item}</div>
                      <div className="select-sub-heading">yrs</div>
                    </div>
                  );
                })}
          </div>
        </div>
      </div>
    </div>
  );
}
