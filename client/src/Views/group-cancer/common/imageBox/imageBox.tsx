import * as React from "react";
import { useState } from "react";
import "./imageBox.css";

interface Iprops {
  id: string;
  image?: string;
  isHavingCount?: boolean;
  title?: string;
  handleActive?: Function;
  selectedGender?: string;
  handleGenderSelect?: Function;
  members?: any;
  handleMemberChange?: Function;
  setIsShowError?: Function;
  count?: number;
}

export default function ImageBox(props: Iprops) {
  const [disableIncrement, setDisableIncrement] = useState(false);

  const handleDecrement = (
    e: React.MouseEvent<HTMLDivElement, MouseEvent>,
    id: string
  ) => {
    e.stopPropagation();
    props.handleMemberChange &&
      props.handleMemberChange(id, props.members[id] - 1);
  };

  const handleIncrement = (
    e: React.MouseEvent<HTMLDivElement, MouseEvent>,
    id: string
  ) => {
    e.stopPropagation();
    props.handleMemberChange &&
      props.handleMemberChange(id, props.members[id] + 1);
  };

  const selectCard = () => {
    const temp = { ...props.members };
    let count = temp[props.id] > 0 ? 0 : 1;
    if (props.id == "male" || props.id == "female") {
      props.handleGenderSelect && props.handleGenderSelect(props.id, count);
    } else {
      props.handleMemberChange && props.handleMemberChange(props.id, count);
    }
  };

  return (
    <div
      className={
        props.selectedGender == props.id ||
        (props.members && props.members[props.id] > 0)
          ? "box-model selected"
          : "box-model"
      }
      onClick={selectCard}
    >
      <div className="box-image">
        <img src={props.image} width="48px" height="48px" />
      </div>
      <div className="box-title">{props.title}</div>
      {props.isHavingCount && props.members && props.members[props.id] > 0 ? (
        <div className="count-box">
          <div
            onClick={(e) => handleDecrement(e, props.id)}
            className="count-box-action"
          >
            -
          </div>
          <div className="count">{props.members[props.id]}</div>
          <div
            className={
              !disableIncrement
                ? "count-box-action"
                : "count-box action disable"
            }
            onClick={(e) => handleIncrement(e, props.id)}
          >
            +
          </div>
        </div>
      ) : null}
    </div>
  );
}
