import React, { useState, FC } from "react";
import { Input, DatePicker, Select } from "antd";
import dayjs from "dayjs";
import "./style.scss";
import { onlyNumbers, onlyAlphabets } from "@/utils/common-utils";

interface OptionType {
  value: string;
  label: string;
}

interface Props {
  label: string;
  value?: any;
  placeholder?: string;
  className?: string;
  inputType?: "input" | "date" | "select" | "number" | "alphabet";
  options?: OptionType[];
  onChange?: (value: string) => void;
  onBlur?: (value: string) => void;
  size?: any;
  type?: any;
  format?: string;
  style?: any;
  maxLength?: number;
  pattern?: any;
  defaultValue?: string | number;
  disabled?: boolean;
  onInput?: any;
  showSearch?: boolean;
}

const { Option } = Select;

const FloatInput: FC<Props> = ({
  label,
  value,
  placeholder,
  className = "",
  inputType = "input",
  options = [],
  onChange,
  onBlur,
  style,
  size,
  type,
  format,
  maxLength,
  pattern,
  disabled,
  showSearch,
}) => {
  const [focus, setFocus] = useState(false);
  if (!placeholder) placeholder = label;
  const isOccupied = focus || (value && value.length !== 0);
  const labelClass = isOccupied ? "label as-label" : "label as-placeholder";
  let floatingComponent;
  switch (inputType) {
    case "input":
      floatingComponent = (
        <Input
          className="text-input"
          style={style}
          type={type}
          maxLength={maxLength}
          size={size}
          onChange={(event) => onChange?.(event.target.value)}
          onBlur={(event) => onBlur?.(event.target.value)}
          defaultValue={value}
        />
      );
      break;
    case "date":
      floatingComponent = (
        <DatePicker
          onChange={(date, dateString) => onChange?.(dateString)}
          defaultValue={value ? dayjs(value) : dayjs(new Date())}
          format={format}
          className="date-input"
          placeholder=""
        />
      );
      break;
    case "select":
      floatingComponent = (
        <Select
          showSearch={showSearch}
          defaultValue={value}
          value={value}
          onChange={(value) => onChange?.(value)}
          className="select-input"
          disabled={disabled}
        >
          {options.map((option) => (
            <Option key={option.value} value={option.value}>
              {option.label}
            </Option>
          ))}
        </Select>
      );
      break;
    case "number":
      floatingComponent = (
        <Input
          onChange={(event) => onChange?.(event.target.value)}
          size={size}
          style={style}
          maxLength={maxLength}
          pattern={pattern}
          value={value}
          className="text-input"
          onInput={onlyNumbers}
          type="tel"
        />
      );
      break;
    case "alphabet":
      floatingComponent = (
        <Input
          onChange={(event) => onChange?.(event.target.value)}
          size={size}
          style={style}
          maxLength={maxLength}
          pattern={pattern}
          value={value}
          className="text-input"
          onInput={onlyAlphabets}
        />
      );
      break;
    default:
      floatingComponent = (
        <Input
          onChange={(event) => onChange?.(event.target.value)}
          defaultValue={value}
          maxLength={maxLength}
          pattern={pattern}
          type={type}
          size={size}
        />
      );
  }

  return (
    <div
      className={`float-label ${className}`}
      onBlur={() => setFocus(false)}
      onFocus={() => setFocus(true)}
    >
      {floatingComponent}
      <label className={labelClass}>{isOccupied ? label : placeholder}</label>
    </div>
  );
};

export default FloatInput;
