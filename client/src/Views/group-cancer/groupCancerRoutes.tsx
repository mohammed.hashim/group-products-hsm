import { getConfig, getStatesData } from "@/actions/group-cancer/actions";
import {
  getConfig as paGetConfig,
  getStatesData as paGetStatesData,
} from "@/actions/group-pa/actions";
import { Grid } from "antd";
import { useDispatch } from "react-redux";
import { Route, Routes } from "react-router-dom";
import "./gcProposal.scss";
import GcProposal from "./screens/gcProposal/gcProposal/gcProposal";
import GcProposalPayment from "./screens/gcProposal/gcProposal/gcProposalPayment";
import GcProposarDetails from "./screens/gcProposal/gcProposerDetail/gcProposarDetails";
import GcTransactionSuccess from "./screens/gcProposal/gcTransactionSuccess/gcTransactionSuccess";
import Prequotes from "./screens/preQuotes/preQuotes";
import { useEffect } from "react";

const GCMainRoutes = ({
  type,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
}) => {
  const { useBreakpoint } = Grid;
  const isMobile = useBreakpoint().xs;
  const dispatch = useDispatch();

  useEffect(() => {
    if (
      type === "CANCER" ||
      type === "CRITICAL" ||
      type === "CARDIAC" ||
      type === "HEALTH" ||
      type === "HOSPICASH"
    ) {
      dispatch(getConfig());
      dispatch(getStatesData());
    }
    if (type === "PA") {
      dispatch(paGetConfig());
      dispatch(paGetStatesData());
    }
  }, []);

  return (
    <div className={`plans product_type_${type}`}>
      <Routes>
        <Route
          path=""
          element={<Prequotes type={type} isMobile={isMobile} />}
        />
        {/* quotes page */}
        <Route
          path="results"
          element={<GcProposal isMobile={isMobile} type={type} />}
        />
        {/* proposal page */}
        <Route
          path="proposal/:proposal_id?"
          element={<GcProposarDetails isMobile={isMobile} type={type} />}
        />
        {/* payment page */}
        <Route
          path="/:proposal_id?/make-payment"
          element={<GcProposalPayment isMobile={isMobile} type={type} />}
        />
        <Route
          path="transaction-success/:order_id"
          element={<GcTransactionSuccess isMobile={isMobile} type={type} />}
        />
      </Routes>
    </div>
  );
};

export default GCMainRoutes;
