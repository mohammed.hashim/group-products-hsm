import {
  getSelectedMembersObject,
  goToPrequoteNextScreen,
} from "@/actions/group-cancer/actions";
import Button from "@/components/button/button";
import { memberList } from "@/constants/groupConstant";
import { useProductState } from "@/hooks/useProductState";
import { RootStore } from "@/store";
import { generateLogEventPayload } from "@/utils/common-utils";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ImageBox from "../../common/imageBox/imageBox";

interface Iprops {
  selectedUser?: string;
  handleSelectedMembers?: Function;
}

export default function MemberList(props: Iprops) {
  const [members, setMemberObj] = useState<any>({
    son: 0,
    daughter: 0,
    husband: 0,
    wife: 0,
    father: 0,
    mother: 0,
  });
  const [disabled, setDisabled] = useState(true);
  const [isShowError, setIsShowError] = useState(false);
  const dispatch = useDispatch();
  const productState = useProductState("CANCER");
  const { selectedGender, selectedMembersObject, configVariables } =
    productState;
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];

  const handleMemberlistObject = (val: any) => {
    if (val == "male") {
      return memberList.filter((obj) => obj.id != "husband");
    } else if (val == "female") {
      return memberList.filter((obj) => obj.id != "wife");
    }
  };
  var newMemberList = handleMemberlistObject(selectedGender);

  const handleMemberSelect = (id: string, value: number) => {
    const temp = { ...members };
    temp[id] = value;
    setMemberObj(temp);
    const isMemberAdded = Object.keys(temp).some(
      (member: string) => temp[member] > 0
    );
    setDisabled(!isMemberAdded);
    // getSelectedMembersObject(members,dispatch)
  };

  const handleSkipBtn = () => {
    setMemberObj({
      son: 0,
      daughter: 0,
      husband: 0,
      wife: 0,
      father: 0,
      mother: 0,
    });
    goToPrequoteNextScreen(dispatch);
    getSelectedMembersObject(
      { son: 0, daughter: 0, husband: 0, wife: 0, father: 0, mother: 0 },
      dispatch
    );
  };

  const handleClick = () => {
    let membersPresent = Object.keys(members).some((key) => members[key] > 0);
    if (membersPresent) {
      goToPrequoteNextScreen(dispatch);
      getSelectedMembersObject(members, dispatch);

      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["member_select_submit"],
          win["pageId"]["pre_quote_page"],
          win["logType"]["submit"],
          members
        )
      );
    }
  };

  useEffect(() => {
    setMemberObj(selectedMembersObject);
    const isMemberAdded = Object.keys(selectedMembersObject).some(
      (member: string) => selectedMembersObject[member] > 0
    );
    setDisabled(!isMemberAdded);
  }, []);

  return (
    <div className="member-list-box-container">
      <div>
        <div className="member-list-heading">
          Select other members you want to insure
        </div>
        <div className="member-list-sub-heading">
          Who would you like to insure?
        </div>
        <div className="member-list-container">
          <div className="members-div">
            {newMemberList &&
              newMemberList.slice(0, 3).map((ele) => {
                return (
                  <ImageBox
                    key={ele.id}
                    id={ele.id}
                    image={ele.image}
                    title={ele.title}
                    count={members[ele.id]}
                    isHavingCount={
                      ele.id == "daughter" || ele.id == "son" ? true : false
                    }
                    members={members}
                    handleMemberChange={handleMemberSelect}
                    setIsShowError={setIsShowError}
                  />
                );
              })}
          </div>
          <div className="members-div bottomRow">
            {newMemberList &&
              newMemberList.slice(3, 5).map((ele) => {
                return (
                  <ImageBox
                    key={ele.id}
                    id={ele.id}
                    image={ele.image}
                    title={ele.title}
                    count={members[ele.id]}
                    isHavingCount={
                      ele.id == "daughter" || ele.id == "son" ? true : false
                    }
                    members={members}
                    handleMemberChange={handleMemberSelect}
                    setIsShowError={setIsShowError}
                  />
                );
              })}
          </div>
        </div>
        <div>
          <p className="age-validation-error">
            {isShowError && "You can only select up to 4 children"}
          </p>
        </div>
        <div className="d-flex flex-direction-col align-center">
          <Button
            customClass={disabled ? "default disabled" : "default"}
            handleOnClick={() => handleClick()}
            title="Next"
          />
          <Button
            handleOnClick={() => handleSkipBtn()}
            title="Skip"
            customClass="skip-button"
          />
        </div>
      </div>
    </div>
  );
}
