import {
  editButtonClicked,
  getGenderDetails,
  goToPrequotePreviousScreen,
} from "@/actions/group-cancer/actions";
import { useProductState } from "@/hooks/useProductState";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

interface IState {
  totalStepsLocal: number;
  currentStep: number;
}

export default function ProgressSteps({
  type,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
}) {
  const dispatch = useDispatch();
  const productState = useProductState(type);
  const { totalSteps, currentScreen } = productState;
  const [totalStepsLocal, setTotalSteps] = useState(0);
  const [currentStep, setCurrentStep] = useState(0);

  useEffect(() => {
    setTotalSteps(totalSteps);
    setCurrentStep(currentScreen);
  }, [totalStepsLocal, currentScreen]);

  const handleBackBtn = () => {
    editButtonClicked(dispatch);
    goToPrequotePreviousScreen(dispatch);
    if (currentStep == 2) {
      getGenderDetails("", dispatch);
    }
  };
  const getStepJsx = () => {
    const stepArray = [];
    for (let i = 1; i <= totalSteps; i++) {
      let stepClass = "step step-" + i;
      stepClass += currentScreen == i ? " active " : " inactive ";
      stepClass += currentScreen > i ? "done" : "";
      stepArray.push(
        <>
          <li className={stepClass}>
            <img
              className={currentScreen > i ? "" : "display-none"}
              src="/static/img/icons/health/checkmark-white.svg"
              height={"12"}
              align-items='center'
            />
          </li>
          {i != totalStepsLocal && (
            <li
              className={
                "connecter " + (currentScreen > i ? " active-connector " : "")
              }
            ></li>
          )}
        </>
      );
    }
    return stepArray;
  };

  return (
    <div className="card-header-container d-flex">
      <div className={`back-icon-container ${currentScreen === 1 && "hide"}`}>
        <img
          onClick={handleBackBtn}
          src="/static/img/icons/health/arrow-left-bold.svg"
          width="18px"
          height="18px"
        />
      </div>
      <ul className="progress-bar align-center">{getStepJsx()}</ul>
    </div>
  );
}
