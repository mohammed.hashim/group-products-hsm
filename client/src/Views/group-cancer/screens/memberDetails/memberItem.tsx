import React, { useState } from "react";
import AgeModalItem from "../../common/dropdown/ageModalItem";
import { mapMembersTitle } from "@/utils/common-utils";
import { Modal } from "antd";

interface Iprops {
  id: string;
  image?: string;
  title: string;
  age: number;
  selectedMemberList?: any;
  handleSelectedMemberList?: any;
  validateAge?: any;
  ageError?: boolean;
  showDelete?: boolean;
  handleDelete?: any;
}

export default function MemberItem(props: Iprops) {
  const [selectedAge, setSelectedAge] = useState<number>(props.age);
  const [open, setOpen] = useState(false);
  const { confirm } = Modal;
  const openModal = () => {
    setOpen(true);
  };
  const handleAgeSelect = (val: any) => {
    props.handleSelectedMemberList(props.title, val);
    setSelectedAge(val);
    setOpen(false);
  };

  const showDeleteConfirm = () => {
    confirm({
      title: `Are you sure remove ${mapMembersTitle(props.title, props.id)} ?`,
      content: `Members will be updated`,
      okText: "Delete",
      okType: "danger",
      cancelText: "Cancel",
      onOk() {
        props.handleDelete(props.title, props.id);
      },
      onCancel() {},
    });
  };
  return (
    <div className="d-flex align-center gap-8 mem-item-container">
      <div className="mem-img-box">
        <img src={props.image} />
      </div>
      <div className="mem-title">{mapMembersTitle(props.title, props.id)}</div>
      <div className={props.ageError ? "age-error" : ""}>
        <div
          onClick={openModal}
          className="select-container d-flex gap-8 align-center"
        >
          <p className="dropdown-title">
            {selectedAge > 0 ? `${selectedAge} yrs` : "Select"}
          </p>
          <img
            src="/static/img/icons/health/caret-down.svg"
            width="24px"
            height="24px"
            className="mr-10"
          />
        </div>
        {open && (
          <AgeModalItem
            id={props.id}
            member="adult"
            selectedAge={props.age ? props.age : selectedAge}
            handleAge={handleAgeSelect}
            onCloseModal={setOpen}
            isAgeError={props.ageError}
          />
        )}
      </div>
      {props.showDelete && (
        <div
          title="Delete"
          className={`cursor-pointer ${props.title === "You" ? "hide" : ""}`}
          onClick={showDeleteConfirm}
        >
          <img src={require("@/assets/images/svg/trash-bold.svg").default} />
        </div>
      )}
    </div>
  );
}
