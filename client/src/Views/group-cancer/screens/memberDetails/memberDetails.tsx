import {
  getSelectedMemberArray,
  goToPrequoteNextScreen,
  goToPrequotePreviousScreen,
} from "@/actions/group-cancer/actions";
import {
  getSelectedMemberArray as paGetSelectedMemberArray,
  goToPrequoteNextScreen as paGoToPrequoteNextScreen,
  goToPrequotePreviousScreen as paGoToPrequotePreviousScreen,
} from "@/actions/group-pa/actions";
import Button from "@/components/button/button";
import { memberList } from "@/constants/groupConstant";
import { useProductState } from "@/hooks/useProductState";
import { StopOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import MemberItem from "./memberItem";
import { RootStore } from "@/store";
import { generateLogEventPayload } from "@/utils/common-utils";
import { debug } from "console";

interface IProps {
  selectedMemberList?: any;
  selectedUser?: string;
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
}

export default function MemberDetails(props: IProps) {
  const [isMemberAgeValid, setIsMemberAgeValid] = useState(false);
  const [renderArray, setRenderArray] = useState([]);
  const [ageError, setAgeError] = useState("");
  const dispatch = useDispatch();
  const productState = useProductState(props.type);
  const {
    selectedMembersObject,
    selectedGender,
    selectedMembersArray,
    configVariables,
  } = productState;
  const [listOfMembers, setListOfMembers] = useState(["you"]);
  let ChildrenCount = 0;
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];

  var obj = selectedMembersObject;
  var memberarray: string[] = [];

  if (
    props.type === "CANCER" ||
    props.type === "CRITICAL" ||
    props.type === "CARDIAC" ||
    props.type === "HEALTH" ||
    props.type === "HOSPICASH"
  ) {
    for (let mem in obj) {
      if (obj[mem] > 0) {
        if (mem == "daughter" || mem == "son") {
          for (let i = 1; i <= obj[mem]; i++) {
            memberarray.push(`${mem}${i}`);
          }
        } else {
          memberarray.push(mem);
        }
      }
    }
  } else if (props.type === "PA") {
    memberarray.push(obj[0]);
  }

  // list of members to display with edit button

  const handleEditBtn = () => {
    if (
      props.type === "CANCER" ||
      props.type === "CRITICAL" ||
      props.type === "CARDIAC" ||
      props.type === "HEALTH" ||
      props.type === "HOSPICASH"
    ) {
      goToPrequotePreviousScreen(dispatch);
    } else if (props.type === "PA") {
      paGoToPrequotePreviousScreen(dispatch);
    }
  };

  const handleSelectedMemberList = (id: any, age: number) => {
    var memberAgeArray = [...renderArray];
    memberAgeArray.map((member: any) => {
      if (member.title == id) {
        member.age = age;
      }
    });
    setRenderArray(memberAgeArray);

    setAgeError("");
  };

  const validateAgeDifference = () => {
    const isAnyAgeZero = renderArray.some((item: any) => item.age === 0);
    setAgeError(isAnyAgeZero ? "Please select age" : "");

    if (!isAnyAgeZero) {
      let isAgeValid = false;
      var temp: any = [];
      renderArray.map((item: any) => {
        temp.push({ ...item, isAgeValid: false });
      });
      temp.map((member: any) => {
        switch (member.id) {
          case "father":
          case "mother":
            const parentsList = temp.filter(
              (item: any) => item.id === "husband" || item.id === "wife"
            );
            const childrensList = temp.filter(
              (item: any) => item.id === "daughter" || item.id === "son"
            );
            const isparentsAgeValid = parentsList.every(
              (item: any) => member.age - item.age >= 18
            );
            const ischildrensAgeValid = childrensList.every(
              (item: any) => member.age - item.age >= 18
            );

            member.isAgeValid = isparentsAgeValid && ischildrensAgeValid;

            break;

          case "husband":
          case "wife":
            const grandparentList = temp.filter(
              (item: any) => item.id === "father" || item.id === "mother"
            );
            const childrenList = temp.filter(
              (item: any) => item.id === "daughter" || item.id === "son"
            );
            const isGrandparentAgeValid = grandparentList.every(
              (item: any) => item.age - member.age >= 18
            );
            const ischildrenAgeValid = childrenList.every(
              (item: any) => member.age - item.age >= 18
            );

            member.isAgeValid = isGrandparentAgeValid && ischildrenAgeValid;

            break;

          case "daughter":
          case "son":
            const parentList = temp.filter(
              (item: any) => item.id === "husband" || item.id === "wife"
            );
            member.isAgeValid = parentList.every(
              (item: any) => item.age - member.age >= 18
            );
            break;
        }
      });
      const allAgeValid = temp.every((item: any) => item.isAgeValid === true);
      if (!allAgeValid) {
        setAgeError("Parents should be atleast 18 years old than kids");
      } else {
        setIsMemberAgeValid(true);
      }
    }
  };

  const handleClick = () => {
    validateAgeDifference();
  };

  useEffect(() => {
    if (isMemberAgeValid) {
      if (
        props.type === "CANCER" ||
        props.type === "CRITICAL" ||
        props.type === "CARDIAC" ||
        props.type === "HEALTH" ||
        props.type === "HOSPICASH"
      ) {
        getSelectedMemberArray(renderArray, dispatch);
        goToPrequoteNextScreen(dispatch);
      }
      if (props.type === "PA") {
        paGetSelectedMemberArray(renderArray, dispatch);
        paGoToPrequoteNextScreen(dispatch);
      }

      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["member_age_submit"],
          win["pageId"]["pre_quote_page"],
          win["logType"]["submit"],
          renderArray
        )
      );
    }
  }, [isMemberAgeValid]);

  useEffect(() => {
    const newSelectedMemberArray: any = [];

    if (selectedGender === "male") {
      newSelectedMemberArray.push({
        id: memberList[0].id,
        image: memberList[0].image,
        title: "You",
        age: selectedMembersArray[0]?.age || 0,
        ageError: false,
        name: "",
        phone: null,
      });
    } else {
      newSelectedMemberArray.push({
        id: memberList[1].id,
        image: memberList[1].image,
        title: "You",
        age: selectedMembersArray[0]?.age || 0,
        ageError: false,
        name: "",
        phone: null,
      });
    }

    memberarray.map((key: string) => {
      memberList.map((item: any) => {
        if (key && key?.includes(item.id)) {
          newSelectedMemberArray.push({
            id: item.id,
            image: item.image,
            title: key,
            age:
              selectedMembersArray.find((item: any) => item.title === key)
                ?.age || 0,
            ageError: false,
          });
        }
      });
    });
    var moveSpouseToFirst = "";
    if (selectedGender == "male") {
      moveSpouseToFirst = "wife";
    } else {
      moveSpouseToFirst = "husband";
    }

    const getSpouseObj = newSelectedMemberArray.find(
      (item: any) => item.id === moveSpouseToFirst
    );

    if (getSpouseObj) {
      const index = newSelectedMemberArray.indexOf(getSpouseObj);
      newSelectedMemberArray.splice(index, 1);
      newSelectedMemberArray.splice(1, 0, getSpouseObj);
    }

    setRenderArray(newSelectedMemberArray);

    Object.keys(obj).map((mem: string) => {
      if ((mem === "husband" || mem === "wife") && obj[mem] > 0) {
        listOfMembers.push(" spouse");
      } else if ((mem === "father" || mem === "mother") && obj[mem] > 0) {
        listOfMembers.push(` ${mem}`);
      } else {
        if (mem === "daughter" || mem === "son") {
          ChildrenCount = ChildrenCount + obj[mem];
        }
      }
    });

    if (ChildrenCount === 1) {
      listOfMembers.push(" kid");
    } else if (ChildrenCount > 1) {
      listOfMembers.push(" kids");
    }
  }, []);

  return (
    <div className="member-details-box-container">
      <div className="selected-members">
        {listOfMembers.toString()}
        <Button
          handleOnClick={handleEditBtn}
          icon="/static/img/icons/health/pencil-simple-bold.svg"
          customClass="edit-button"
        />
      </div>
      <div className="member-details-heading">
        {props.type === "CANCER" ||
          props.type === "CRITICAL" ||
          props.type === "CARDIAC" ||
          props.type === "HEALTH" ||
          props.type === "HOSPICASH"
          ? "Tell us the ages of your family members"
          : "Tell us your age"}
      </div>
      <div className="d-flex member-details-container box-shadow flex-direction-col">
        {renderArray.map((ele: any, idx: number) => {
          return (
            <MemberItem
              key={idx}
              id={ele.id}
              image={ele.image}
              title={ele.title}
              selectedMemberList={selectedMembersArray}
              handleSelectedMemberList={handleSelectedMemberList}
              validateAge={validateAgeDifference}
              ageError={ele.ageError}
              age={ele.age}
            />
          );
        })}
        <div>
          {ageError && (
            <p className="age-validation-error">
              <StopOutlined className="stop-icon" width={16} height={16} />
              {ageError}
            </p>
          )}
          <Button handleOnClick={handleClick} title="Continue" />
        </div>
      </div>
    </div>
  );
}
