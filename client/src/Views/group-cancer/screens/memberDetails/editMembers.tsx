import {
  getSelectedMemberArray,
  getSelectedMembersObject,
} from "@/actions/group-cancer/actions";
import { memberList } from "@/constants/groupConstant";
import { useProductState } from "@/hooks/useProductState";
import { StopOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import MemberItem from "./memberItem";

const EditMembers = ({
  type,
  edit,
  closeModal,
  updatePlans,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  edit?: boolean;
  closeModal: any;
  updatePlans: any;
}) => {
  const productState = useProductState(type);
  const { selectedMembersArray, selectedMembersObject } = productState;
  const dispatch = useDispatch();

  const [membersArr, setMembersArr] = useState(selectedMembersArray);
  const [addMembers, setAddMembers] = useState([]);
  const [editMembers, setEditMembers] = useState<boolean>();
  const [childCount, setChildCount]: any = useState({
    son: selectedMembersObject.son,
    daughter: selectedMembersObject.daughter,
  });
  const [memberCount, setMemberCount] = useState<any>({
    father: selectedMembersObject.father,
    husband: selectedMembersObject.husband,
    mother: selectedMembersObject.mother,
    wife: selectedMembersObject.wife,
  });

  const availableMembers = () => {
    const memList = memberList.map((mem: any) => mem.id);
    const membersId = [...new Set(membersArr.map((v: any) => v.id))];
    let newArr: any = ["son", "daughter"];
    let arr = memList.filter((member: any) => {
      if (
        !membersId.includes(member) &&
        !["son", "daughter"].includes(member)
      ) {
        return newArr.push(member);
      }
    });
    return newArr;
  };
  useEffect(() => {
    setAddMembers(availableMembers());
  }, [membersArr]);
  const handleSelectedMemberList = (id: any, age: number) => {
    var memberAgeArray = [...membersArr];
    memberAgeArray.map((member: any) => {
      if (member.title == id) {
        member.age = age;
      }
    });
    setMembersArr(memberAgeArray);
  };
  const handleUpdate = (val: any) => {
    const newMember = memberList.find((member: any) => member.id === val);
    if (val === "son" || val === "daughter") {
      setChildCount({ ...childCount, [val]: childCount[val] + 1 });
      setMembersArr([
        ...membersArr,
        {
          age: null,
          id: val,
          image: newMember!.image || "",
          title: `${val}${childCount[val] + 1}`,
        },
      ]);
    } else {
      setMemberCount({ ...memberCount, [val]: 1 });
      setMembersArr([...membersArr, newMember]);
    }
  };

  const handleDelete = async (title: string, id: string) => {
    const updatedData = membersArr.filter(
      (member: any) => member.title !== title
    );

    updatedData.map((item: any) => {
      if (item.id === id) {
        const number = Number(item.title.split(id)[1]);
        item.title = number >= 2 ? `${id}${number - 1}` : `${id}${number}`;
      }
    });
    if (id === "son" || id === "daughter") {
      await setChildCount({ ...childCount, [id]: childCount[id] - 1 });
    } else await setMemberCount({ ...memberCount, [id]: memberCount[id] - 1 });

    await setMembersArr(updatedData);
  };
  const submitUpdatedList = async () => {
    if (validateAgeDifference(membersArr).isValid) {
      await getSelectedMemberArray(membersArr, dispatch);
      await getSelectedMembersObject(
        { ...selectedMembersObject, ...childCount, ...memberCount },
        dispatch
      );
      setEditMembers(false);
      closeModal();
      updatePlans(membersArr);
    }
  };
  const validateAgeDifference = (array: any) => {
    const isAnyAgeZero = array.some(
      (item: any) => item.age === 0 || item.age === null
    );
    let validateResponse: any = [];
    if (isAnyAgeZero) {
      validateResponse.push("Please select age");
    }

    if (!isAnyAgeZero) {
      let isAgeValid = false;
      var temp = [...array];
      temp.map((member: any) => {
        switch (member.id) {
          case "husband":
          case "wife":
            const grandparentList = temp.filter(
              (item: any) => item.id === "father" || item.id === "mother"
            );
            const childrenList = temp.filter(
              (item: any) => item.id === "daughter" || item.id === "son"
            );
            const isGrandparentAgeValid = grandparentList.every(
              (item: any) => item.age - member.age >= 18
            );
            const ischildrenAgeValid = childrenList.every(
              (item: any) => member.age - item.age >= 18
            );

            isAgeValid = isGrandparentAgeValid && ischildrenAgeValid;
            if (!isAgeValid) {
              validateResponse.push(
                "Parents should be atleast 18 years old than kids"
              );
            }
            break;

          case "daughter":
          case "son":
            const parentList = temp.filter(
              (item: any) => item.id === "husband" || item.id === "wife"
            );
            isAgeValid = parentList.every(
              (item: any) => item.age - member.age >= 18
            );
            if (!isAgeValid) {
              validateResponse.push(
                "Parents should be atleast 18 years old than kids"
              );
            }
            break;
        }
      });
    }
    return validateResponse.length === 0
      ? { errorMsg: null, isValid: true }
      : { errorMsg: [...new Set(validateResponse)], isValid: false };
  };
  return (
    <div className="d-flex flex-direction-col">
      <div className="member-details-container remove-boxShadow">
        {membersArr.map((mem: any, idx: number) => {
          return (
            <MemberItem
              key={idx}
              id={mem.id}
              image={mem.image}
              title={mem.title}
              age={mem.age}
              handleSelectedMemberList={handleSelectedMemberList}
              // validateAge={validateAgeDifference}
              ageError={mem.ageError}
              showDelete={editMembers}
              handleDelete={(title: any, id: any) => handleDelete(title, id)}
            />
          );
        })}
      </div>
      {type != "PA" && (
        <div>
          {!editMembers ? (
            <Button onClick={() => setEditMembers(!editMembers)} type="link">
              Add remove members
            </Button>
          ) : (
            <div className="d-flex align-item-center wrap">
              {addMembers.map((val: any, idx: any) => (
                <div
                  key={idx}
                  className="ml-10 mb-5 addMember-btn"
                  onClick={() => handleUpdate(val)}
                >
                  <img
                    height={"15px"}
                    className="stop-icon"
                    src={
                      require("@/assets/images/svg/plus-add-bold.svg").default
                    }
                  />
                  <span>{val}</span>
                </div>
              ))}
              {/* <Button onClick={() => setEditMembers(!editMembers)} type="link">
                Cancel
              </Button> */}
            </div>
          )}
        </div>
      )}

      <div className="d-flex flex-direction-col p-10 update-btn">
        {!validateAgeDifference(membersArr).isValid && (
          <p className="age-validation-error">
            <StopOutlined className="stop-icon" width={16} height={16} />
            {validateAgeDifference(membersArr).errorMsg?.toString()}
          </p>
        )}
        <Button className="fs-16 " onClick={submitUpdatedList}>
          Update
        </Button>
      </div>
    </div>
  );
};

export default EditMembers;
