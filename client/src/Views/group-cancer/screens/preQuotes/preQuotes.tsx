import { RESET_STATE } from "@/actions/group-cancer/types";
import { RESET_STATE as PA_RESET_STATE } from "@/actions/group-pa/types";
import Header from "@/components/header/header";
import { titleConstant } from "@/constants/titleConstant";
import { useProductState } from "@/hooks/useProductState";
import AddressDetails from "@/Views/group-cancer/screens/addressDetails/addressDetails";
import ProgressSteps from "@/Views/group-cancer/screens/cardHeader/ProgressSteps";
import GenderDetails from "@/Views/group-cancer/screens/genderDetails/genderDetails";
import MemberDetails from "@/Views/group-cancer/screens/memberDetails/memberDetails";
import MemberList from "@/Views/group-cancer/screens/memberList/memberList";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { generateLogEventPayload } from "@/utils/common-utils";

export default function Prequotes({
  type,
  isMobile,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  isMobile: boolean | undefined;
}) {
  const [isGenderDetails, setIsGenderDetails] = useState(true);
  const [isMemberList, setIsMemberList] = useState(false);
  const [isMemberDetails, setIsMemberDetails] = useState(false);
  const [isAddressDetails, setIsAddressDetails] = useState(false);
  const [selectedUser, setSelectedUser] = useState("");
  const [selectedMemberList, setSelectedMemberList] = useState([]);
  const dispatch = useDispatch();
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];

  const productState = useProductState(type);
  const { currentScreen, configVariables } = productState;

  useEffect(() => {
    localStorage.removeItem("localMemberDetails");
    localStorage.removeItem("proposalFormData");

    if (currentScreen == 1) {
      dispatch({ type: RESET_STATE });
      dispatch({ type: PA_RESET_STATE });
    }
  }, []);

  useEffect(() => {
    if (
      type === "CANCER" ||
      type === "CRITICAL" ||
      type === "CARDIAC" ||
      type === "HEALTH" ||
      type === "HOSPICASH"
    ) {
      if (currentScreen == 1) {
        setIsGenderDetails(true);
        setIsMemberList(false);
        setIsMemberDetails(false);
        setIsAddressDetails(false);

        logEvent(
          configVariables.logEvent,
          generateLogEventPayload(
            win["widget"]["gender_view"],
            win["pageId"]["pre_quote_page"]
          )
        );
      } else if (currentScreen == 2) {
        setIsGenderDetails(false);
        setIsMemberList(true);
        setIsMemberDetails(false);
        setIsAddressDetails(false);

        logEvent(
          configVariables.logEvent,
          generateLogEventPayload(
            win["widget"]["member_select_view"],
            win["pageId"]["pre_quote_page"]
          )
        );
      } else if (currentScreen == 3) {
        setIsGenderDetails(false);
        setIsMemberList(false);
        setIsMemberDetails(true);
        setIsAddressDetails(false);

        logEvent(
          configVariables.logEvent,
          generateLogEventPayload(
            win["widget"]["member_age_view"],
            win["pageId"]["pre_quote_page"]
          )
        );
      } else if (currentScreen == 4) {
        setIsGenderDetails(false);
        setIsMemberList(false);
        setIsMemberDetails(false);
        setIsAddressDetails(true);

        logEvent(
          configVariables.logEvent,
          generateLogEventPayload(
            win["widget"]["user_details_view"],
            win["pageId"]["pre_quote_page"]
          )
        );
      }
    }
    if (type === "PA") {
      if (currentScreen == 1) {
        setIsGenderDetails(true);
        setIsMemberList(false);
        setIsMemberDetails(false);
        setIsAddressDetails(false);

        logEvent(
          configVariables.logEvent,
          generateLogEventPayload(
            win["widget"]["gender_view"],
            win["pageId"]["pre_quote_page"]
          )
        );
      } else if (currentScreen == 2) {
        setIsGenderDetails(false);
        setIsMemberList(false);
        setIsMemberDetails(true);
        setIsAddressDetails(false);
      } else if (currentScreen == 3) {
        setIsGenderDetails(false);
        setIsMemberList(false);
        setIsMemberDetails(false);
        setIsAddressDetails(true);
      }
    }
  }, [productState]);

  const handleGender = (val: any) => {
    if (val == "male" || val == "female") {
      setSelectedUser(val);
    }
  };

  const handleMemberList = (val: any) => {
    setSelectedMemberList(val);
  };

  return (
    <div>
      <Header type={type} showBackIcon={isMobile} />
      <div className="d-flex flex-direction-col outer-container below-header">
        <div
          className={
            currentScreen != 1 ? "display-none-onmobile heading" : "heading"
          }
        >
          <p className="main-heading">{titleConstant[type]}</p>
        </div>
        <div className="d-flex gap-16 inner-container">
          <div className="col-1">
            <div className="card">
              <div
                className={currentScreen == 1 ? "display-none-onmobile" : ""}
              >
                <ProgressSteps type={type} />
              </div>
              {isGenderDetails ? (
                <GenderDetails handleGenderDetails={handleGender} type={type} />
              ) : isMemberList ? (
                <MemberList
                  handleSelectedMembers={handleMemberList}
                  selectedUser={selectedUser}
                />
              ) : isMemberDetails ? (
                <MemberDetails type={type} />
              ) : (
                isAddressDetails && <AddressDetails type={type} />
              )}
            </div>
          </div>
          {!isMobile && (
            <div className="info-container">
              <img
                src="/static/img/icons/health/ic_tax_investments.svg"
                width="50px"
                height="50px"
              />
              <p className="main-text">
                A health insurance plan also saves you upto ₹75,000 under 80D*
              </p>
              <p className="sub-text">Upto ₹25,000 for you, spouse & kids</p>
              <p className="sub-text">
                Additionally, upto ₹50,000 for your senior citizen parents
              </p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
