import {
  getGenderDetails,
  getSelectedMemberArray,
  getSelectedMembersObject,
  goToPrequoteNextScreen,
} from "@/actions/group-cancer/actions";
import {
  getGenderDetails as paGetGenderDetails,
  getSelectedMemberArray as paGetSelectedMemberArray,
  goToPrequoteNextScreen as paGoToPrequoteNextScreen,
} from "@/actions/group-pa/actions";
import Button from "@/components/button/button";
import { gender } from "@/constants/groupConstant";
import { useProductState } from "@/hooks/useProductState";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import ImageBox from "../../common/imageBox/imageBox";
import { RootStore } from "@/store";
import { generateLogEventPayload } from "@/utils/common-utils";

interface Iprops {
  handleGenderDetails?: any;
  handleBackBtn?: Function;
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
}

export default function GenderDetails(props: Iprops) {
  const [selectedGender, setSelectedGender] = useState("");
  const productState = useProductState(props.type);
  const { configVariables } = productState;
  const [error, setError] = useState("");
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];

  const dispatch = useDispatch();

  const handleClick = () => {
    props.handleGenderDetails(selectedGender);
    if (selectedGender) {
      if (
        props.type === "CANCER" ||
        props.type === "CRITICAL" ||
        props.type === "CARDIAC" ||
        props.type === "HEALTH" ||
        props.type === "HOSPICASH"
      ) {
        getGenderDetails(selectedGender, dispatch);
        goToPrequoteNextScreen(dispatch);
        getSelectedMembersObject(
          {
            son: 0,
            daughter: 0,
            husband: 0,
            wife: 0,
            father: 0,
            mother: 0,
          },
          dispatch
        );
        getSelectedMemberArray([], dispatch);
      }
      if (props.type === "PA") {
        paGetGenderDetails(selectedGender, dispatch);
        paGoToPrequoteNextScreen(dispatch);
        paGetSelectedMemberArray([], dispatch);
      }

      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["gender_gender_submit"],
          win["pageId"]["pre_quote_page"],
          win["logType"]["submit"]
        )
      );
    } else {
      setError("Please select your gender");
    }
  };

  return (
    <div className="gender-details-box-container">
      <div className="gender-heading">Select your gender</div>
      <div className="d-flex gender-container">
        {gender.map((ele) => {
          return (
            <ImageBox
              key={ele.id}
              id={ele.id}
              title={ele.title}
              image={ele.image}
              selectedGender={selectedGender}
              handleGenderSelect={(gender: string) => {
                setError("");
                setSelectedGender(gender);
              }}
            />
          );
        })}
      </div>
      <div>
        <Button handleOnClick={handleClick} title="Get Started" />
        <p className="age-validation-error">{error}</p>
      </div>
    </div>
  );
}
