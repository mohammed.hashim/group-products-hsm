import { downloadPolicyPdf } from "@/actions/group-cancer/actions";
import { downloadPolicyPdf as paDownloadPolicyPdf } from "@/actions/group-pa/actions";
import AppLoader from "@/components/appLoader";
import Header from "@/components/header/header";
import { useProductState } from "@/hooks/useProductState";
import { formatNumberToLakhs } from "@/utils/common-utils";
import { Button, Col, Divider, Row, Typography } from "antd";
import dayjs from "dayjs";
import * as React from "react";
import { useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";

const GcTransactionSuccess = ({
  type,
  isMobile,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  isMobile: boolean | undefined;
}) => {
  const [loading, setLoading] = React.useState(true);
  const [isPdfDownloading, setIsPdfDownloading] = React.useState(false);
  const dispatch = useDispatch();
  const { Text } = Typography;
  const routerParams = useParams();
  const productState = useProductState(type);
  const { singlePlan, proposalData } = productState;
  const proposal = proposalData?.proposal;

  React.useEffect(() => {
    setLoading(false);
  }, []);

  const downloadPolicy = async () => {
    setIsPdfDownloading(true);
    console.log(isPdfDownloading);
    setTimeout(() => {
      if (
        type === "CANCER" ||
        type === "CRITICAL" ||
        type === "CARDIAC" ||
        type === "HEALTH" ||
        type === "HOSPICASH"
      ) {
        dispatch(downloadPolicyPdf());
      } else if (type === "PA") {
        dispatch(paDownloadPolicyPdf());
      }

      setIsPdfDownloading(false);
    }, 1000);
    // dispatch(downloadPolicyPdf());
  };
  const purchaseStatement = async () => {
    setTimeout(() => {
      dispatch(downloadPolicyPdf());
    }, 1000);
  };

  const getInsuredMembers = () => {
    const insuredMembers: any = [];
    let temp: any;
    insuredMembers.push(proposal?.full_name);
    proposal.member_details?.forEach((val: any, index: number, arr: []) => {
      if (
        Object.keys(val)[0] === "son_data" ||
        Object.keys(val)[0] === "daughter_data"
      ) {
        Object.values(val).forEach((obj: any) => {
          Object.values(obj).forEach((obj: any) => {
            insuredMembers.push(`${obj.fname} ${obj.lname}`);
          });
        });
      } else {
        Object.values(val).forEach((obj: any) => {
          insuredMembers.push(`${obj.fname} ${obj.lname}`);
        });
      }
    });
    return insuredMembers;
  };

  return (
    <div>
      <Header type={type} />
      {loading ? (
        <AppLoader />
      ) : (
        <>
          <div className="transaction-success-section below-header">
            <div className="tick-icon">
              <img
                src="/static/img/icons/health/check-bold.svg"
                height={"48px"}
                alt="done"
              />
            </div>
            <div>
              <div className="ts-title">Payment successful</div>
              <div className="ts-transactionId">
                Transaction ID: {routerParams.order_id}
              </div>
            </div>
          </div>
          <div className="detailsCard">
            <div>
              <Row gutter={24} justify={"center"}>
                <Col className="text-right" span={8}>
                  <img
                    src="/static/insurers/Care_Health_Insurance_Logo_new.png"
                    height={"40px"}
                    alt="logo"
                  />
                </Col>
                <Col className="d-flex flex-direction-col" span={16}>
                  <Text strong className="insurer-title">
                    Care Health Insurance (formerly Religare)
                  </Text>
                  <Text strong>{type} Care plan</Text>
                  <Text>
                    Sum Assured
                    <Text className="font-bold ml-10">
                      {formatNumberToLakhs(singlePlan?.plan?.sum_insured)}
                    </Text>
                  </Text>
                </Col>
              </Row>
            </div>
            <Divider />
            <div>
              <Row justify={"center"}>
                <Col>
                  <div className="d-flex flex-direction-col p-top-bottom-10">
                    <Text className="sub-title-heading">Policy No:</Text>
                    <Text strong>{singlePlan?.plan?.id}</Text>
                  </div>
                  <div className="d-flex flex-direction-col p-top-bottom-10">
                    <Text className="sub-title-heading">Policy Expiry:</Text>
                    <Text strong>
                      {dayjs(singlePlan?.plan?.createdAt)
                        .add(1, "year")
                        .format("MMM DD YYYY")}
                    </Text>
                  </div>
                  <div className="d-flex flex-direction-col p-top-bottom-10">
                    <Text className="sub-title-heading">Insured Members:</Text>
                    <Text className="text-capitalize" strong>
                      {getInsuredMembers().join(" , ")}
                    </Text>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="text-center">
              <Button
                className="downloadPolicy-btn"
                loading={isPdfDownloading}
                size="middle"
                onClick={() => downloadPolicy()}
              >
                DOWNLOAD POLICY
              </Button>
            </div>
            <div className="link-container">
              <Link className="link-homepage" to="#"
                onClick={() => purchaseStatement()}>
                <img
                  className="mr-10"
                  src="/static/img/icons/health/arrow-line-down-bold.svg"
                  height="13px"
                />
                {`INSURANCE PURCHASE STATEMENT`}
              </Link>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default GcTransactionSuccess;
