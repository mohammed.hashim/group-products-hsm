import AdInfoImage from "@/components/AdInfoImage";
import { useProductState } from "@/hooks/useProductState";
import {
  calculateTotalPremium,
  formatNumberToLakhs,
  rupeeFormat,
  generateLogEventPayload,
} from "@/utils/common-utils";
import { Button, Card, Col, Divider, Row, Typography, Modal } from "antd";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { createContext, Suspense, lazy, useState, useEffect } from "react";
import AppLoader from "@/components/appLoader";
import { useDispatch } from "react-redux";
import { getPlans } from "@/actions/group-cancer/actions";
import { getPlans as paGetPlans } from "@/actions/group-pa/actions";

const setPathName = (type: string) => {
  switch (type) {
    case "CANCER":
      return "/group-cancer/";
      break;
    case "CRITICAL":
      return "/critical-illness/";
      break;
    case "CARDIAC":
      return "/group-cancer/";
      break;
    case "HEALTH":
      return "/group-health/";
      break;
    case "HOSPICASH":
      return "/hospicash/";
      break;
  }
};
const GcSummaryCard = ({
  type,
  isMobile,
}: {
  isMobile: boolean | undefined;
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
}) => {
  const navigate = useNavigate();
  const { Text, Title } = Typography;
  const productState = useProductState(type);
  const {
    singlePlan,
    selectedMembersArray,
    proposalData,
    configVariables,
    plansPagination,
  } = productState;
  const logEvent = (window as any)["sharedUtils"]["logEvent"];
  const [open, setOpen] = useState(false);
  const EditMemberContext = createContext(false);
  const EditMembers = lazy(() => import("../../memberDetails/editMembers"));
  const dispatch = useDispatch();
  const [edit, setEdit] = useState(false);
  const win = (window as any)["sharedConstants"];
  const callGetPlans = (arr: any) => {
    const ages = arr.map((member: any) => member.age);
    const currPage = plansPagination.currentPage;
    if (type !== "PA") {
      dispatch(getPlans(ages, currPage));
    } else if (type === "PA") {
      dispatch(paGetPlans(ages, currPage));
    }
    logEvent(
      configVariables.logEvent,
      generateLogEventPayload(
        win["widget"]["view_quotes"],
        win["pageId"]["quote_page"]
      )
    );
    if (arr != selectedMembersArray) {
      if (
        type == "CANCER" ||
        type == "CRITICAL" ||
        type == "CARDIAC" ||
        type == "HEALTH" ||
        type == "HOSPICASH"
      )
        navigate(`${setPathName(type)}results`);
    }
  };

  return (
    <Card className={!isMobile ? "ml-10" : "border-radius-0 below-header"}>
      <React.Fragment>
        <div hidden={isMobile} className="p-5 border-bottom">
          <Row justify={"space-between"} align={"middle"}>
            <Text className="text-muted medium-bold-text">YOUR PLAN</Text>
            <Link to={"../results"}>
              <Button type="link" className="font-bold fs-16">
                CHANGE PLAN
              </Button>
            </Link>
          </Row>
        </div>
        {!isMobile ? (
          <>
            <Row
              className="p-10 summaryCard"
              wrap={true}
              justify="space-between"
            >
              <Col>
                <div>
                  <img
                    src="/static/insurers/Care_Health_Insurance_Logo_new.png"
                    className="insurerLogo"
                    alt="logo"
                  />
                </div>
              </Col>
              <Col className="d-flex flex-direction-col pl-20">
                <Text className="medium-bold-text">
                  Care Health Insurance (formerly Religare)
                </Text>
                <Text className="font-bold">{type} Care plan</Text>
                <Text>
                  Sum Assured{" "}
                  <Text className="font-bold">
                    {formatNumberToLakhs(
                      singlePlan?.plan?.sum_insured ||
                        proposalData?.proposal?.sum_insured
                    )}
                  </Text>{" "}
                </Text>
              </Col>
            </Row>
            <div className="p-10 border-bottom">
              <Row justify={"space-between"}>
                <Text className="text-muted">Base Premium</Text>
                <Text>
                  {rupeeFormat(
                    singlePlan?.premium ||
                      proposalData?.proposal?.premium_amount
                  )}
                </Text>
              </Row>
              <Row justify={"space-between"}>
                <Text className="text-muted">{"GST(18%)"}</Text>
                <Text>
                  {rupeeFormat(
                    calculateTotalPremium(
                      singlePlan?.premium ||
                        proposalData?.proposal?.premium_amount
                    ).gst
                  )}
                </Text>
              </Row>
              <Divider dashed className="bw-3" />
              <Row justify={"space-between"}>
                <Text className="medium-bold-text">Total Premium</Text>
                <Text className="medium-bold-text">
                  {rupeeFormat(
                    calculateTotalPremium(
                      singlePlan?.premium ||
                        proposalData?.proposal?.premium_amount
                    ).totalPremium
                  )}
                </Text>
              </Row>
            </div>
          </>
        ) : (
          <div className="p-10 ptb-10">
            <Row align={"middle"} justify={"space-between"}>
              <Col className="d-flex flex-direction-col">
                <img
                  src="/static/insurers/Care_Health_Insurance_Logo_new.png"
                  className="insurerLogo__mobView"
                />
                <div className="text-muted text-left insurerName__mobView">
                  Care Supreme
                </div>
                <Link to={"../results"} className="font-bold fs-16">
                  Change Plan
                </Link>
              </Col>
              <Col>
                <div className="d-flex flex-direction-col align-item-center">
                  <Text className="text-muted">Sum Assured</Text>
                  <Text className="fs-amount">
                    {formatNumberToLakhs(
                      singlePlan?.plan?.sum_insured ||
                        proposalData?.proposal?.sum_insured
                    )}
                  </Text>
                </div>
              </Col>
              <Col>
                <div className="d-flex flex-direction-col align-item-center">
                  <Text className="text-muted">Yearly Premium</Text>
                  <Text className="fs-amount">
                    {" "}
                    {rupeeFormat(
                      calculateTotalPremium(
                        singlePlan?.premium ||
                          proposalData?.proposal?.premium_amount
                      ).totalPremium
                    )}
                  </Text>
                </div>
              </Col>
            </Row>
          </div>
        )}
        <div className="bg-lightgrey">
          <div className={!isMobile ? "p-10" : "p-2 mob-summary-info"}>
            <Text className="ml-5">
              {selectedMembersArray?.length}{" "}
              {selectedMembersArray?.length > 1 ? "Members" : "Member"}
            </Text>
            <span
              onClick={() => {
                setOpen(true);
              }}
              className="font-primary ml-190"
            >
              View Details
            </span>
            <EditMemberContext.Provider value={edit}>
              <Modal
                className="edit-members"
                title={
                  <Text className="d-flex justify-content-center fs-18">
                    Member Details
                  </Text>
                }
                open={open}
                width={"420px"}
                footer={null}
                onCancel={() => {
                  setOpen(false);
                }}
              >
                <Suspense fallback={<AppLoader />}>
                  <EditMembers
                    type={type}
                    closeModal={() => setOpen(false)}
                    updatePlans={(arr: any) => callGetPlans(arr)}
                  />
                </Suspense>
              </Modal>
            </EditMemberContext.Provider>
          </div>
        </div>
      </React.Fragment>
    </Card>
  );
};

export default GcSummaryCard;
