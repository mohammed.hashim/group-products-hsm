import { Card, Typography } from "antd";
import React from "react";

const AddParentsInfoCard = () => {
  return (
    <div style={{ paddingTop: "10px" }}>
      <Card className="ml-10">
        <React.Fragment>
          <div className="p-10 border-bottom">
            <Typography.Text
              className="fs-16 medium-bold-text"
              type="secondary"
            >
              ADD YOUR PARENTS
            </Typography.Text>
          </div>
          <div className="p-10">
            You can additional tax benefit of ₹50,000 for health insurance plans
            for your parents under 80D{" "}
            <span className="font-primary">more</span>
          </div>
        </React.Fragment>
      </Card>
    </div>
  );
};

export default AddParentsInfoCard;
