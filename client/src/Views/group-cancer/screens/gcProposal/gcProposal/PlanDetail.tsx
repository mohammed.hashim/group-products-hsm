import { useProductState } from "@/hooks/useProductState";
import { generateLogEventPayload } from "@/utils/common-utils";
import { Typography } from "antd";
import { useEffect } from "react";

const PlanDetail = ({
  isMobile,
  type,
}: {
  isMobile?: boolean | undefined;
  type?: any;
}) => {
  const { Text } = Typography;
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];

  const productState = useProductState(type);
  const {
    selectedMembersArray,
    selectedGender,
    proposalData,
    singlePlan,
    loading,
    cancerFailState,
    plans,
    configVariables,
  } = productState;

  useEffect(() => {
    logEvent(
      configVariables.logEvent,
      generateLogEventPayload(
        win["widget"]["plan_details"],
        win["pageId"]["quote_page"]
      )
    );
  }, []);

  return (
    <div>
      <h3 hidden={isMobile}>Plan Details</h3>
      {isMobile && (
        <div className={"mob-plan-logo p-5"}>
          <div>
            <span className={isMobile ? "text-muted fs-14" : ""}>
              Care Health Insurance
            </span>{" "}
            <br />
            <span className="fs-16">Cancer Care Plans</span>
          </div>

          <div>
            <img
              src="/static/insurers/Care_Health_Insurance_Logo_new.png"
              height={!isMobile ? "60px" : "40px"}
              alt="logo"
            />
          </div>
        </div>
      )}
      <Text type="secondary">COVERAGE</Text> <br />
      <Text>
        Cancer Can be defined as:
        <br />
        I. A malignant tumor characterized by the uncontrolled growth and spread
        of malignant cells with invasion and destruction of normal tissues. This
        diagnosis must be supported by histological evidence of malignancy and
        confirmed by a pathologist
        <p>
          II. The term cancer includes: A. leukemia, lymphoma, and sarcoma B.
          Tumor’s showing the malignant changes of carcinoma in situ and tumors
          which are histologically described as pre-malignant or non-invasive,
          including but not limited to: Carcinoma in situ of breasts, Cervical
          dysplasia CIN-1, CIN -2 & CIN-3
        </p>
      </Text>
      <Text type="secondary">EXCLUSION</Text>
      <br />
      <Text>
        Hospitalization resulting other than due to Cancer.
        <br /> Any PED; Term Cancer Excludes:
        <br /> a) Benign lesions
        <br /> b) All tumors of the prostate unless histologically classified as
        having a Gleason score greater than 6 or having progressed to at least
        clinical TNM classification T2N0M0;
        <br /> c) Papillary micro - carcinoma of the thyroid less than 1 cm in
        diameter;
        <br /> d) Microcarcinoma of the bladder;
        <br /> e) All tumors in the presence of HIV infection
      </Text>
    </div>
  );
};

export default PlanDetail;
