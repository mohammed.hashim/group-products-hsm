import {
  getProposalData,
  proposalCheckout,
} from "@/actions/group-cancer/actions";
import { CANCER_FAIL } from "@/actions/group-cancer/types";
import {
  proposalCheckout as paProposalCheckout,
  getProposalData as paGetProposalData,
} from "@/actions/group-pa/actions";
import { CANCER_FAIL as PA_CANCER_FAIL } from "@/actions/group-pa/types";
import AppLoader from "@/components/appLoader";
import AppModal from "@/components/appModal/appModal";
import Header from "@/components/header/header";
import { useProductState } from "@/hooks/useProductState";
import { rootUrl } from "@/services";
import { post } from "@/utils/apiUtils";
import {
  calculateTotalPremium,
  generateLogEventPayload,
  rupeeFormat,
} from "@/utils/common-utils";
import { makePayment } from "@/utils/paymentUtils";
import { Button, Card, Checkbox, Col, Grid, Row, Typography } from "antd";
import { CheckboxChangeEvent } from "antd/es/checkbox";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import GcSummaryCard from "../gcSummaryCard/gcSummaryCard";

const GcProposalPayment = ({
  type,
  isMobile,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  isMobile: boolean | undefined;
}) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const { Text } = Typography;
  const { useBreakpoint } = Grid;
  const navigate = useNavigate();
  const [paymentProcess, setPaymentProcess] = useState(false);
  const [checked, setChecked] = useState(false);
  const [localLoading, setLocalLoading] = useState(true);
  const productState = useProductState(type);
  const {
    selectedMembersArray,
    selectedGender,
    proposalData,
    singlePlan,
    loading,
    cancerFailState,
    plans,
    configVariables,
  } = productState;
  const proposal = proposalData ? proposalData.proposal : {};
  const purchaseAmount = calculateTotalPremium(
    proposalData?.proposal?.premium_amount
  );
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];
  const routerParams: any = useParams();

  const handleCheckbox = (e: CheckboxChangeEvent) => {
    setChecked(e.target.checked);
  };

  useEffect(() => {
    if (paymentProcess) {
      pgRedirect(Number(purchaseAmount.totalPremium));
    }
  }, [paymentProcess]);

  useEffect(() => {
    if (
      type === "CANCER" ||
      type === "CRITICAL" ||
      type === "CARDIAC" ||
      type === "HEALTH" ||
      type === "HOSPICASH"
    ) {
      dispatch(
        getProposalData(routerParams.proposal_id, selectedMembersArray.length)
      );
      setLocalLoading(false);
    } else if (type === "PA") {
      dispatch(paGetProposalData(routerParams.proposal_id));
      setLocalLoading(false);
    }
  }, []);

  useEffect(() => {
    // const planId = location.state.planId;
    // let singlePlan = plans.find((plan: any) => planId === plan.id)
    // dispatch(getSinglePlan(singlePlan, dispatch));
    if (
      type === "CANCER" ||
      type === "CRITICAL" ||
      type === "CARDIAC" ||
      type === "HEALTH" ||
      type === "HOSPICASH"
    ) {
      dispatch({
        type: CANCER_FAIL,
        payload: false,
      });
    } else if (type === "PA") {
      dispatch({
        type: PA_CANCER_FAIL,
        payload: false,
      });
    }

    logEvent(
      configVariables.logEvent,
      generateLogEventPayload(
        win["widget"]["review"],
        win["pageId"]["proposal_form"]
      )
    );
  }, []);

  const pgRedirect = (amount: number) => {
    let requestBody = {
      amount: amount,
      proposal_id: proposalData?.proposal?.id,
    };
    if (
      type === "CANCER" ||
      type === "CRITICAL" ||
      type === "CARDIAC" ||
      type === "HEALTH" ||
      type === "HOSPICASH"
    ) {
      dispatch(
        proposalCheckout(
          requestBody,
          makePayment,
          postPaymentHandler,
          setPaymentProcess
        )
      );
    } else if (type === "PA") {
      dispatch(
        paProposalCheckout(
          requestBody,
          makePayment,
          postPaymentHandler,
          setPaymentProcess
        )
      );
    }
  };

  const saveProposalCtxData = (
    paymentSuccess: boolean,
    transactionResp: any
  ) => {
    const { planId } = proposalData?.proposal;
    const reqBody = {
      proposal_data: proposalData?.proposal,
      transaction_data: transactionResp,
      planId: location.state.planId || planId,
      payment_success: paymentSuccess,
    };
    post(`${rootUrl}/cancer/plans/saveProposalCtx`, reqBody)
      .then((res: any) => {
        if (res.data.success) {
          navigate(
            `../transaction-success/${transactionResp.razorpay_order_id}`,
            {
              state: {
                transactionSuccess: res.data.success,
              },
            }
          );
        } else {
          setPaymentProcess(false);
        }
      })
      .catch(() => {
        if (
          type === "CANCER" ||
          type === "CRITICAL" ||
          type === "CARDIAC" ||
          type === "HEALTH" ||
          type === "HOSPICASH"
        ) {
          dispatch({
            type: CANCER_FAIL,
            payload: true,
          });
        } else if (type === "PA") {
          dispatch({
            type: PA_CANCER_FAIL,
            payload: true,
          });
        }
        setPaymentProcess(false);
      });
  };

  const proposalTransaction = (response: any) => {
    const reqBody = {
      proposal_id: proposalData?.proposal?.id,
      request: singlePlan || {},
      response: response || {},
    };
    post(`${rootUrl}/api/pg/transaction`, reqBody).then((res: any) => {
      if (res.data.success) {
        post(`${rootUrl}/api/pg/paymentverification`, response).then(
          (resp: any) => {
            saveProposalCtxData(res.data.success, response);
          }
        );
      }
    });
  };

  const buyPlan = (e: any) => {
    e.preventDefault();
    setPaymentProcess(true);
  };

  const postPaymentHandler = (resp: any) => {
    if (resp.hasOwnProperty("razorpay_payment_id")) {
      proposalTransaction(resp);
    } else {
      proposalTransaction((resp = {}));
    }
  };

  const memberArray = () => {
    let memberArr: any = [];
    const temp = {
      dob: proposalData?.proposal.dob,
      full_name: proposalData?.proposal.full_name,
      title: "Self",
      image: `/static/img/icons/health/member-${selectedGender}.svg`,
    };

    memberArr.push(temp);
    proposal.member_details?.forEach((val: any, index: number, arr: []) => {
      if (
        Object.keys(val)[0] === "son_data" ||
        Object.keys(val)[0] === "daughter_data"
      ) {
        Object.values(val).forEach((obj: any) => {
          return memberArr.push(obj);
        });
      } else {
        memberArr.push(Object.values(val));
      }
    });
    return memberArr;
  };

  return (
    <>
      {isMobile ? (
        <Header type={type} title="REVIEW AND BUY" showBackIcon hideLogo />
      ) : (
        <Header type={type} title="REVIEW AND BUY" />
      )}
      {isMobile && <GcSummaryCard isMobile={isMobile} type={type} />}
      <div className="d-flex justify-content-center ">
        <div className="planBody">
          {loading || localLoading ? (
            <AppLoader />
          ) : (
            <Row>
              <Col xs={24} sm={16} lg={16}>
                <Card>
                  <>
                    <Row className="border-bottom" justify={"center"}>
                      <Text className="important-details-text">
                        Important Details to Review before Payment
                      </Text>
                    </Row>
                    <div className={isMobile ? "p-left p-right" : "p-10"}>
                      <Row className="align-item-center">
                        <Col span={22}>
                          <Text type="secondary">PROPOSER DETAILS</Text>
                        </Col>
                        <Col span={2}>
                          <Button
                            type="link"
                            className="edit-btn-text"
                            onClick={() =>
                              navigate(
                                `../proposal/${routerParams.proposal_id}`,
                                {
                                  state: {
                                    defaultKey: 1,
                                    edit: true,
                                  },
                                }
                              )
                            }
                          >
                            EDIT
                          </Button>
                        </Col>
                      </Row>
                      {!isMobile ? (
                        <Row>
                          <Col className="d-flex flex-direction-col" span={14}>
                            <Text strong className="text-capitalize">
                              {proposal.full_name}
                            </Text>
                            <Text strong hidden={type === "PA"}>
                              {proposal.email}
                            </Text>
                            <Text strong>{proposal.contact}</Text>
                          </Col>
                          <Col className="d-flex flex-direction-col" span={10}>
                            {proposal.hasOwnProperty("panId") ||
                              (true && (
                                <span>
                                  PAN:
                                  <Text className="text-uppercase" strong>
                                    {` ${proposal?.other_user_details?.pan_id}`}
                                  </Text>
                                </span>
                              ))}
                            <Text className="d-flex align-item-center">
                              <span className="mr-5">Date of Birth:</span>
                              <b>
                                {`  ${dayjs(proposal.dob).format(
                                  "DD-MM-YYYY"
                                )}`}
                              </b>
                            </Text>
                            <Text className="d-flex align-item-center">
                              {proposal?.other_user_details?.nominee_name &&
                                proposal?.other_user_details?.nominee_age &&
                                proposal?.other_user_details
                                  ?.nominee_relation && (
                                  <>
                                    <span className="mr-5">Nominee:</span>
                                    <Text strong>
                                      {`${proposal.other_user_details.nominee_name}, ${proposal.other_user_details.nominee_age} yrs, ${proposal.other_user_details.nominee_relation}`}
                                    </Text>
                                  </>
                                )}
                            </Text>
                          </Col>
                        </Row>
                      ) : (
                        <div className="p-top-bottom-10 border-bottom">
                          <Row>
                            <Col span={6}>
                              <Text>Full Name:</Text>
                            </Col>
                            <Col className="text-right" span={18}>
                              <Text strong>{proposal.full_name}</Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={6}>
                              <Text>Date of birth:</Text>
                            </Col>
                            <Col className="text-right" span={18}>
                              <Text strong>{`  ${dayjs(proposal.dob).format(
                                "DD/MM/YYYY"
                              )}`}</Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={6}>
                              <Text>PAN:</Text>
                            </Col>
                            <Col
                              className="text-right text-uppercase"
                              span={18}
                            >
                              <Text strong>
                                {proposal.other_user_details.pan_id}
                              </Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={6}>
                              <Text>Mobile:</Text>
                            </Col>
                            <Col className="text-right" span={18}>
                              <Text strong>{`+91 ${proposal.contact}`}</Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={6}>
                              <Text>Email:</Text>
                            </Col>
                            <Col className="text-right" span={18}>
                              <Text strong>{proposal.email}</Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={12}>
                              <Text>Nominee Name:</Text>
                            </Col>
                            <Col className="text-right" span={12}>
                              <Text strong>
                                {proposal.other_user_details.nominee_name}
                              </Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={18}>
                              <Text>Nominee Age:</Text>
                            </Col>
                            <Col className="text-right" span={6}>
                              <Text strong>
                                {`${proposal.other_user_details.nominee_age} yrs`}
                              </Text>
                            </Col>
                          </Row>
                          <Row>
                            <Col span={12}>
                              <Text>Nominee Relationship:</Text>
                            </Col>
                            <Col className="text-right" span={12}>
                              <Text strong>
                                {proposal.other_user_details.nominee_relation}
                              </Text>
                            </Col>
                          </Row>
                        </div>
                      )}
                    </div>
                    <div className={isMobile ? "p-left p-right" : "p-10"}>
                      <Row className="align-item-center">
                        <Col span={22}>
                          <Text type="secondary">COMMUNICATION ADDRESS</Text>
                        </Col>
                        <Col span={2}>
                          <Button
                            type="link"
                            className="edit-btn-text"
                            onClick={() =>
                              navigate(
                                `../proposal/${routerParams.proposal_id}`,
                                {
                                  state: {
                                    defaultKey: 1,
                                    edit: true,
                                  },
                                }
                              )
                            }
                          >
                            EDIT
                          </Button>
                        </Col>
                      </Row>
                      <div
                        className={
                          isMobile ? "p-top-bottom-10 border-bottom" : ""
                        }
                      >
                        {proposal.other_user_details.address && (
                          <Text strong>
                            {proposal.other_user_details.address}
                          </Text>
                        )}
                        <br />
                        <Text strong>
                          {`
                        ${
                          proposal.other_user_details.pin_code &&
                          proposal.other_user_details.pin_code + ","
                        } ${
                            proposal.other_user_details.city &&
                            proposal.other_user_details.city + ","
                          } ${
                            proposal.other_user_details.state &&
                            proposal.other_user_details.state
                          }`}
                        </Text>
                      </div>
                    </div>
                    <div
                      className={
                        isMobile ? "p-left p-right" : "p-10 border-bottom"
                      }
                    >
                      <div>
                        <Row className="align-item-center">
                          <Col span={22}>
                            <Text type="secondary">INSURED MEMBERS</Text>
                          </Col>
                          <Col span={2}>
                            <Button
                              type="link"
                              className="edit-btn-text"
                              onClick={() =>
                                navigate(
                                  `../proposal/${routerParams.proposal_id}`,
                                  {
                                    
                                    state: {
                                      defaultKey: (proposal?.member_details?.length >= 1) ? 2 : 1,
                                      edit: true,
                                    },
                                  }
                                )
                              }
                            >
                              EDIT
                            </Button>
                          </Col>
                        </Row>
                        {memberArray()
                          .flat()
                          .map((mem: any, idx: any) => (
                            <React.Fragment key={idx}>
                              {!isMobile ? (
                                <Row
                                  className="align-item-center"
                                  gutter={[16, 24]}
                                >
                                  <Col span={12}>
                                    <Row className="align-item-center">
                                      <Col span={4}>
                                        <img
                                          src={mem.image}
                                          width="24px"
                                          height="24px"
                                        />
                                      </Col>
                                      <Col span={8}>
                                        <Text className="text-uppercase">
                                          {mem.title === "husband" ||
                                          mem.title === "wife"
                                            ? "Spouse"
                                            : mem.title}
                                        </Text>
                                      </Col>
                                      <Col
                                        span={12}
                                        className="text-right text-capitalize "
                                      >
                                        <Text strong>
                                          {mem.full_name
                                            ? mem.full_name
                                            : mem.fname + " " + mem.lname}
                                        </Text>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col span={12}>
                                    <span>
                                      <img
                                        className="mr-10"
                                        src="/static/img/icons/health/cake-bold.svg"
                                        width="16px"
                                        height="16px"
                                      />
                                      <span>
                                        {dayjs(mem.dob).format("DD MMMM YYYY")}
                                      </span>
                                    </span>
                                  </Col>
                                </Row>
                              ) : (
                                <Row className="p-top-bottom-10">
                                  <>
                                    <Col span={3}>
                                      <img
                                        src={mem.image}
                                        width="24px"
                                        height="24px"
                                      />
                                    </Col>
                                    <Col span={21}>
                                      <Col>
                                        <Text className="p-top-bottom-8 text-uppercase">
                                          {mem.title === "husband" ||
                                          mem.title === "wife"
                                            ? "Spouse"
                                            : mem.title}
                                        </Text>
                                      </Col>
                                      <Row>
                                        <Col span={12}>
                                          <Text>Full Name:</Text>
                                        </Col>
                                        <Col
                                          className="text-right text-capitalize"
                                          span={12}
                                        >
                                          <Text strong>
                                            {mem.full_name
                                              ? mem.full_name
                                              : mem.fname + " " + mem.lname}
                                          </Text>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col span={12}>
                                          <Text>Date of Birth:</Text>
                                        </Col>
                                        <Col
                                          className="text-right text-uppercase"
                                          span={12}
                                        >
                                          <Text strong>
                                            {dayjs(mem.dob).format(
                                              "DD MMM YYYY"
                                            )}
                                          </Text>
                                        </Col>
                                      </Row>
                                    </Col>
                                  </>
                                </Row>
                              )}
                            </React.Fragment>
                          ))}
                      </div>
                    </div>

                    <div className="p-10">
                      <Checkbox onChange={(e) => handleCheckbox(e)} />
                      <Text className="text-muted ml-10">
                        I declare that the information provided above is true
                        and accept that if it is found to be false, it may
                        impact claims. I authorize Coverfox Insurance Broking
                        Pvt. Ltd. to represent me at insurance companies for my
                        insurance needs.
                      </Text>
                    </div>

                    <Row justify={"center"}>
                      <Button
                        className={`proposer-btn ${
                          !checked && "proposer-btn-disable"
                        }`}
                        disabled={!checked}
                        loading={paymentProcess}
                        onClick={(e) => {
                          // make_payment_click

                          logEvent(
                            configVariables.logEvent,
                            generateLogEventPayload(
                              win["widget"]["make_payment_click"],
                              win["pageId"]["proposal_form"],
                              win["logType"]["submit"]
                            )
                          );
                          buyPlan(e);
                        }}
                      >
                        Confirm & Pay (
                        {rupeeFormat(purchaseAmount.totalPremium)})
                      </Button>
                    </Row>
                  </>
                </Card>
              </Col>
              <Col xs={0} sm={8} lg={8}>
                <GcSummaryCard isMobile={isMobile} type={type} />
                {/* <AddParentsInfoCard /> */}
              </Col>
            </Row>
          )}
        </div>
      </div>
      {cancerFailState && (
        <AppModal
          status={cancerFailState}
          setPaymentProcess={setPaymentProcess}
        />
      )}
    </>
  );
};

export default GcProposalPayment;
