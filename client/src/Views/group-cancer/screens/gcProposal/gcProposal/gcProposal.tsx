import {
  getPlans,
  getSinglePlan,
  postProposalData,
  resetPaginationData,
} from "@/actions/group-cancer/actions";
import {
  getPlans as paGetPlans,
  getSinglePlan as paGetSinglePlan,
} from "@/actions/group-pa/actions";
import AdInfoImage from "@/components/AdInfoImage";
import AppLoader from "@/components/appLoader";
import Header from "@/components/header/header";
import { useProductState } from "@/hooks/useProductState";
import {
  formatNumberToLakhs,
  rupeeFormat,
  generateLogEventPayload,
  showMembers,
  calculateTotalPremium,
} from "@/utils/common-utils";
import "@/Views/group-cancer/gcProposal.scss";
import {
  Button,
  Card,
  Checkbox,
  Col,
  Drawer,
  Dropdown,
  Modal,
  Pagination,
  Row,
  Space,
  Typography,
} from "antd";
import { createContext, lazy, Suspense, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import PlanDetail from "./PlanDetail";
import "@/Views/group-cancer/gcProposal.scss";
import dayjs from "dayjs";

const GcProposal = ({
  type,
  isMobile,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  isMobile: boolean | undefined;
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [openPlanDetail, setOpenPlanDetail] = useState<boolean>(false);
  const productState = useProductState(type);
  const {
    plans,
    loading,
    selectedMembersArray,
    selectedMembersObject,
    selectedGender,
    proposalData,
    configVariables,
    plansPagination,
  } = productState;
  const [open, setOpen] = useState(false);
  const [edit, setEdit] = useState(false);
  const EditMembers = lazy(() => import("../../memberDetails/editMembers"));
  const { Text } = Typography;
  const EditMemberContext = createContext(false);
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];
  const [currentPage, setCurrentPage] = useState(plansPagination.currentPage);
  const callGetPlans = (arr: any) => {
    const ages = arr.map((member: any) => member.age);
    if (type !== "PA") {
      dispatch(getPlans(ages, currentPage));
    } else if (type === "PA") {
      dispatch(paGetPlans(ages, currentPage));
    }

    logEvent(
      configVariables.logEvent,
      generateLogEventPayload(
        win["widget"]["view_quotes"],
        win["pageId"]["quote_page"]
      )
    );
  };

  useEffect(() => {
    dispatch(resetPaginationData());
    // callGetPlans(selectedMembersArray);
  }, []);
  useEffect(() => {
    callGetPlans(selectedMembersArray);
  }, [plansPagination.currentPage]);

  const showMoreBtn = currentPage < plansPagination.totalPage;
  const Filter = () => {
    return (
      <div
        className={`d-flex justify-space-between bg-white ${
          isMobile ? "p-5 sub-header" : "desktop-padding align-item-center"
        } `}
      >
        {isMobile ? (
          <div
            className="d-flex flex-direction-col cursor-pointer"
            onClick={() => {
              setOpen(true);
            }}
          >
            <Text>{showMembers(selectedMembersObject, selectedGender)}</Text>
            <span className="font-primary fs-14">View Details</span>
          </div>
        ) : (
          <div
            className="desktop-filter-pill cursor-pointer"
            onClick={() => {
              setOpen(true);
            }}
          >
            <Text className="text-muted p-5">Members Covered</Text>
            <Text className="font-bold p-5">
              {" "}
              {showMembers(selectedMembersObject, selectedGender)}
            </Text>
            <span className="mr-10">
              <img src="/static/img/icons/health/caret-down-bold.svg" />
            </span>
          </div>
        )}

        <div
          className={!isMobile ? "display-none" : "d-flex align-items-center"}
        >
          <span className="border-left-grey p-5 text-capitalize">
            {type.toLocaleLowerCase()} Plan
          </span>
          {/* <img src="/static/img/icons/health/info-bold.svg" /> */}
        </div>
      </div>
    );
  };

  return (
    <div>
      {isMobile ? (
        <Header type={type} showBackIcon filter={<Filter />} />
      ) : (
        <Header type={type} filter={<Filter />} />
      )}
      <div>
        <div className="planBody">
          {!isMobile && (
            <Row>
              <Col xs={24} sm={16} lg={16}>
                <div className="d-flex justify-space-between p-5">
                  <span className="text-capitalize fs-16">
                    <b>{type.toLocaleLowerCase()} Protect plans</b>
                  </span>
                  <div className="d-flex align-item-center gap-8 text-muted fs-14">
                    <img src="/static/img/icons/health/info-bold.svg" /> All
                    premiums are inclusive of taxes
                  </div>
                </div>
              </Col>
            </Row>
          )}
          <Card className="p-0">
            <Row>
              <Col xs={24} sm={16} lg={16}>
                <Row
                  className={
                    isMobile
                      ? "d-flex flex-direction-col"
                      : "plan-body-padding border-bottom"
                  }
                >
                  <Row
                    gutter={{ xs: 8, sm: 16, md: 24 }}
                    className={
                      isMobile ? "d-flex justify-space-between pl-20 pt-10" : ""
                    }
                    wrap={false}
                  >
                    <Col className={isMobile ? "mob-plan-logo" : ""}>
                      <div className="d-flex flex-direction-col">
                        <span className={isMobile ? "text-muted fs-12" : ""}>
                          Care Health Insurance
                        </span>{" "}
                        <span
                          className="text-capitalize fs-12"
                          hidden={!isMobile}
                        >
                          {type.toLocaleLowerCase()} Protect Plans
                        </span>
                      </div>

                      <div>
                        <img
                          src="/static/insurers/Care_Health_Insurance_Logo_new.png"
                          height={!isMobile ? "60px" : "40px"}
                          alt="logo"
                        />
                      </div>
                    </Col>
                    {!isMobile && (
                      <>
                        <Col>
                          <AdInfoImage
                            imgSrc={
                              require("@/assets/images/svg/ic_hosp-ic_f_hospi_exp.svg")
                                .default
                            }
                            subtitle1={"In-patient"}
                            subtitle2={"Hospitalization"}
                          />
                        </Col>
                        <Col>
                          <AdInfoImage
                            imgSrc={
                              require("@/assets/images/svg/ic_inclusion_preHosp.svg")
                                .default
                            }
                            subtitle1={"30 days"}
                            subtitle2={"Pre-Hospitalization"}
                          />
                        </Col>
                        <Col>
                          <AdInfoImage
                            imgSrc={
                              require("@/assets/images/svg/ic_inclusion_postHosp.svg")
                                .default
                            }
                            subtitle1={"60 days"}
                            subtitle2={"Post-Hospitalization"}
                          />
                        </Col>
                      </>
                    )}
                    <Col>
                      {isMobile && (
                        <div
                          onClick={() => setOpenPlanDetail(!openPlanDetail)}
                          className="p-5 mb-0 font-primary cursor-pointer d-flex align-item-center flex-col-reverse"
                        >
                          Details
                          <img
                            src={
                              require("@/assets/images/svg/arrow-right.svg")
                                .default
                            }
                          />
                        </div>
                      )}
                    </Col>
                  </Row>
                  {isMobile && (
                    <Row
                      align={"middle"}
                      className={
                        isMobile ? "pl-20 plan-benifits-div" : "d-flex p-10"
                      }
                    >
                      <Col span={8}>
                        <AdInfoImage
                          // imgSrc={
                          //   require("@/assets/images/svg/ic_hosp-outline.svg")
                          //     .default
                          // }
                          hideImage
                          subtitle1={"234"}
                          subtitle2={"Network Hospitals"}
                          isMobile={isMobile}
                        />
                      </Col>
                      <Col span={8}>
                        <AdInfoImage
                          // imgSrc={
                          //   require("@/assets/images/svg/ic_hosp.svg").default
                          // }
                          hideImage
                          subtitle1={"98%"}
                          subtitle2={"Claim Settlement"}
                          isMobile={isMobile}
                        />
                      </Col>
                      <Col span={8}>
                        <AdInfoImage
                          // imgSrc={
                          //   require("@/assets/images/svg/ic_TaxSaving.svg")
                          //     .default
                          // }
                          hideImage
                          subtitle1={"Save Tax"}
                          subtitle2={"as per section 80D"}
                          isMobile={isMobile}
                        />
                      </Col>
                    </Row>
                  )}
                </Row>
                {isMobile && (
                  <>
                    <div className="d-flex justify-space-between p-5">
                      <span className="fs-12">
                        <b>{plans.length ? plans.length : ""} Plans</b>
                      </span>
                      {/* <div hidden={isMobile}>
                        <Checkbox
                          checked={configVariables.gst}
                          //onChange={() => setIncludeTax(!includeTax)}
                          // onChange={onChange}
                        >
                          Include taxes
                        </Checkbox>
                      </div> */}
                      <span className="text-muted fs-12">
                        {configVariables.gst
                          ? "All premiums are Inclusive of taxes"
                          : "*premiums Exclusive of 18% GST"}
                      </span>
                    </div>
                  </>
                )}

                <div className="plan-body-padding card-bg">
                  {!isMobile && (
                    <>
                      <Row justify="space-between" align="middle">
                        <h3>{plans?.length} plans</h3>
                        {/* <div>
                          <Checkbox
                            checked={configVariables.gst}
                            // onChange={() => setIncludeTax(configVariables.gst)}
                          >
                            Include taxes
                          </Checkbox>
                        </div> */}
                      </Row>
                    </>
                  )}
                  {loading ? (
                    <AppLoader />
                  ) : (
                    <Row
                      justify="center"
                      align="middle"
                      className="card-container"
                      wrap={true}
                      gutter={!isMobile ? [24, 24] : [8, 8]}
                    >
                      {plans?.map((plan: any) => (
                        <Col
                          className={!isMobile ? "p-5" : "mb-5 full-width"}
                          key={plan.planId}
                        >
                          <Card bordered={false}>
                            <div className="plans-variants">
                              <Text hidden={isMobile} type="secondary">
                                Sum Assured
                              </Text>
                              <Text className="fs-14 sum-assured ">
                                <Text
                                  hidden={!isMobile}
                                  className="p-right-10"
                                  type="secondary"
                                >
                                  Sum Assured:
                                </Text>
                                <b>
                                  {formatNumberToLakhs(plan.plan.sum_insured)}
                                </b>
                              </Text>
                              <Button
                                type={isMobile ? "link" : "default"}
                                className={`d-flex flex-direction-col align-item-center ${
                                  isMobile ? "mob-buy-plan-btn" : "buy-plan-btn"
                                }`}
                                onClick={() => {
                                  if (
                                    type === "CANCER" ||
                                    type === "CRITICAL" ||
                                    type === "CARDIAC" ||
                                    type === "HEALTH" ||
                                    type === "HOSPICASH"
                                  ) {
                                    dispatch(getSinglePlan(plan));
                                  } else if (type === "PA") {
                                    dispatch(paGetSinglePlan(plan));
                                  }

                                  let payload = {
                                    full_name: selectedMembersArray[0].name,
                                    contact: selectedMembersArray[0].phone,
                                    planId: plan.planId,
                                    dob:
                                      dayjs(new Date()).subtract(
                                        selectedMembersArray[0]?.age,
                                        "year"
                                      ) ||
                                      dayjs(new Date()) ||
                                      null,
                                    gender:
                                      selectedGender ||
                                      proposalData?.proposal?.gender,
                                    premium_amount: plan.premium,
                                  };
                                  dispatch(postProposalData(payload, navigate));
                                }}
                              >
                                {isMobile && (
                                  <div className="buy-btn p-5 font-primary font-bold d-flex align-item-center">
                                    <Text className="large-bold-text">
                                      {configVariables.gst
                                        ? rupeeFormat(
                                            calculateTotalPremium(plan.premium)
                                              .totalPremium
                                          )
                                        : rupeeFormat(plan.premium)}
                                      <Text className="text-muted fw-normal">
                                        {" /yr"}
                                      </Text>
                                    </Text>
                                    <img
                                      className="ml-10"
                                      src={
                                        require("@/assets/images/svg/arrow-right-black.svg")
                                          .default
                                      }
                                      width="20px"
                                      height="20px"
                                    />
                                  </div>
                                )}
                                {!isMobile && (
                                  <>
                                    <Text className="fs-16 font-bold">
                                      {configVariables.gst
                                        ? rupeeFormat(
                                            calculateTotalPremium(plan.premium)
                                              .totalPremium
                                          )
                                        : rupeeFormat(plan.premium)}
                                      <Text className="text-muted fw-normal">
                                        {" /yr"}
                                      </Text>
                                    </Text>
                                  </>
                                )}
                              </Button>
                              {/* </Link> */}
                            </div>
                          </Card>
                        </Col>
                      ))}
                    </Row>
                  )}

                  {/* TODO: uncomment this when more plans are available  */}

                  {/* {!loading && !isMobile && showMoreBtn ? (
                    <div className=" pt-10">
                      <Button
                        className=" m-auto fs-14 font-bold more-btn d-flex align-item-center justify-center"
                        // onClick={() => setCurrentPage(currentPage + 1)}
                      >
                        More
                        <img
                          className="ml-10"
                          width={12}
                          height={12}
                          src="/static/img/icons/health/caret-down-bold.svg"
                        />
                      </Button>
                    </div>
                  ) : (
                    isMobile &&
                    showMoreBtn && (
                      <div className="d-flex gap-8 justify-center">
                        <Text className="text-muted fw-normal">more plans</Text>
                        <img src="/static/img/icons/health/caret-down-bold.svg" />
                      </div>
                    )
                  )} */}
                </div>
              </Col>
              <Col xs={0} lg={8} sm={8} className="planDetail">
                <PlanDetail type={type} />
              </Col>
            </Row>
          </Card>
        </div>
      </div>

      <Drawer
        title="Plan Details"
        placement={"bottom"}
        closable={true}
        onClose={() => setOpenPlanDetail(!openPlanDetail)}
        open={openPlanDetail}
        height={"100%"}
        key="bottom"
      >
        <PlanDetail isMobile={isMobile} type={type} />
      </Drawer>
      <EditMemberContext.Provider value={edit}>
        <Modal
          className="edit-members"
          title={
            <Text className="d-flex justify-content-center fs-18">
              Member Details
            </Text>
          }
          open={open}
          width={"420px"}
          footer={null}
          onCancel={() => {
            setOpen(false);
          }}
        >
          <Suspense fallback={<AppLoader />}>
            <EditMembers
              type={type}
              closeModal={() => setOpen(false)}
              updatePlans={(arr: any) => callGetPlans(arr)}
            />
          </Suspense>
        </Modal>
      </EditMemberContext.Provider>
    </div>
  );
};

export default GcProposal;
