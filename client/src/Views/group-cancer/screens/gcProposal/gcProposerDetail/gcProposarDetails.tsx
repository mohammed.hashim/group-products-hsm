import {
  getProposalData,
  getSelectedMemberArray,
  postProposalData,
} from "@/actions/group-cancer/actions";
import {
  getProposalData as paGetProposalData,
  postProposalData as paPostProposalData,
} from "@/actions/group-pa/actions";
import AppLoader from "@/components/appLoader";
import Header from "@/components/header/header";
import { useProductState } from "@/hooks/useProductState";
import { calculateTotalPremium, rupeeFormat } from "@/utils/common-utils";
import {
  Avatar,
  Button,
  Col,
  Collapse,
  DatePicker,
  Divider,
  Form,
  Modal,
  Radio,
  Row,
  Typography,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import FloatInput from "../../../common/floatInput";
import AddParentsInfoCard from "../gcSummaryCard/addParentsInfoCard";
import GcSummaryCard from "../gcSummaryCard/gcSummaryCard";
import { PremiumChangeModal } from "./premiumChangeModal";
import MemberDetails from "./memberDetails";
import { logEvent } from "@/utils/sharedUtils";
import { generateLogEventPayload } from "@/utils/common-utils";
interface Iprops {
  isMobile?: boolean | undefined;
}
const panelStyleDesktop = {
  marginBottom: 12,
  background: "#2873f0",
  borderRadius: "5px",
  border: "none",
};
const panelStyleMobile = {
  marginBottom: "12px",
  background: "white",
  borderRadius: "5px",
  border: "none",
};
const dateFormat = "DD-MM-YYYY";

const GcProposarDetails = ({
  type,
  isMobile,
}: {
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
  isMobile: boolean | undefined;
}) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const productState = useProductState(type);
  const navigate = useNavigate();
  const routerParams: any = useParams();
  const logEvent = (window as any)["sharedUtils"]["logEvent"];
  const {
    loading,
    proposalData,
    selectedGender,
    plans,
    selectedMembersArray,
    singlePlan,
    configVariables,
    statesData,
  } = productState;
  const proposerData = selectedMembersArray.filter(
    (key: any) => key.title === "You"
  );
  const proposerFormData = JSON.parse(
    localStorage.getItem("proposalFormData") || "{}"
  );

  const [gender, setGender] = useState(selectedGender);
  const [defaultKey, setDefaultKey] = useState(1);
  const [localLoading, setLoading] = useState(true);
  const [onlyProposer, setOnlyProposer] = useState<boolean>(false);
  const [proposerDetails, setProposerDetails] = useState<any>({
    first_name: proposerData[0]?.name.split(" ")[0] || "",
    last_name: proposerData[0]?.name.split(" ")[1] || "",
    mobile_no: proposerData[0]?.phone || null,
    dob:
      dayjs(new Date()).subtract(proposerData[0]?.age, "year") ||
      dayjs(new Date()) ||
      null,
  });
  const [form] = Form.useForm();

  const { Text } = Typography;

  const [nomineeAgeError, setNomineeAgeError] = useState("");
  const [isNomineeAgeValid, setIsNomineeAgeValid] = useState(true);
  const [selectedState, setSelectedState] = useState("");

  const win = (window as any)["sharedConstants"];

  // const premiumAmount =
  //   plans?.find((plan: any) => plan.planId === singlePlan.planId).premium ||
  //   proposalData?.proposal?.premium_amount;
  const purchaseAmount = calculateTotalPremium(
    proposalData?.proposal?.premium_amount
  );

  let payload = {
    full_name: proposerDetails?.first_name + " " + proposerDetails?.last_name,
    email: proposerDetails?.email_id,
    dob: proposerDetails?.dob,
    planId: singlePlan?.planId,
    premium_amount: proposalData?.proposal?.premium_amount,
    contact: proposerDetails?.mobile_no,
    member_details: proposalData?.proposal?.member_details || [],
    other_user_details:
      {
        address: proposerDetails?.address,
        city: proposerDetails?.city,
        landmark: proposerDetails?.landmark,
        nominee_age: proposerDetails?.nominee_age,
        nominee_name: proposerDetails?.nominee_name,
        nominee_relation: proposerDetails?.nominee_relation,
        pan_id: proposerDetails?.pan_id,
        pin_code: proposerDetails?.pin_code,
        state: proposerDetails?.state,
      } || null,
  };

  useEffect(() => {
    if (Object.keys(proposerFormData).length > 0) {
      setSelectedState(proposerFormData.state);
      setProposerDetails({
        ...proposerFormData,
        first_name: proposerData[0]?.name.split(" ")[0] || "",
        last_name: proposerData[0]?.name.split(" ")[1] || "",
        mobile_no: proposerData[0]?.phone || null,
        dob:
          dayjs(new Date()).subtract(proposerData[0]?.age, "year") ||
          dayjs(new Date()) ||
          null,
      });
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    const defaultKeyFromPaymentEdit = location?.state?.defaultKey || 1;
    const editFromPaymentEdit = location?.state?.edit || false;
    if (
      type === "CANCER" ||
      type === "CRITICAL" ||
      type === "CARDIAC" ||
      type === "HEALTH" ||
      type === "HOSPICASH"
    ) {
      dispatch(
        getProposalData(
          routerParams.proposal_id,
          form.setFieldsValue,
          selectedMembersArray.length,
          setSelectedState,
          setLoading
        )
      );
    } else if (type === "PA") {
      dispatch(
        paGetProposalData(
          routerParams.proposal_id,
          form.setFieldsValue,
          selectedMembersArray.length,
          setSelectedState,
          setLoading
        )
      );
    }
    setDefaultKey(defaultKeyFromPaymentEdit);
    // edit from payment page
    if (editFromPaymentEdit) {
      let otherDetails = proposalData?.proposal?.other_user_details || null;
      // edit mode set values
      setProposerDetails({
        ...proposerDetails,
        address: proposalData?.proposal?.other_user_details?.address || "",
        city: proposalData?.proposal?.other_user_details?.city || "",
        dob: dayjs(proposalData?.proposal?.dob) || dayjs(new Date()),
        email_id: proposalData?.proposal?.email || "",
        first_name: proposalData?.proposal?.full_name?.split(" ")[0] || "",
        landmark: proposalData?.proposal?.other_user_details?.landmark || "",
        last_name: proposalData?.proposal?.full_name?.split(" ")[1] || "",
        mobile_no: proposalData?.proposal?.contact || "",
        nominee_age:
          proposalData?.proposal?.other_user_details?.nominee_age || "",
        nominee_name:
          proposalData?.proposal?.other_user_details?.nominee_name || "",
        nominee_relation:
          proposalData?.proposal?.other_user_details?.nominee_relation || "",
        pan_id: otherDetails?.pan_id || "",
        pin_code: proposalData?.proposal?.other_user_details?.pin_code || "",
        state: proposalData?.proposal?.other_user_details?.state || "",
      });
    }

    logEvent(
      configVariables.logEvent,
      generateLogEventPayload(
        win["widget"]["proposer_details"],
        win["pageId"]["proposal_form"]
      )
    );
  }, [selectedMembersArray]);

  useEffect(() => {
    if (defaultKey === 2) {
      let updatedPayload = {
        ...payload,
        proposal_id: proposalData?.proposal?.id || proposerDetails.id,
      };
      if (
        type === "CANCER" ||
        type === "CRITICAL" ||
        type === "CARDIAC" ||
        type === "HEALTH" ||
        type === "HOSPICASH"
      ) {
        dispatch(postProposalData(updatedPayload));
      } else if (type === "PA") {
        dispatch(paPostProposalData(updatedPayload));
      }
    }
  }, [defaultKey]);

  useEffect(() => {
    if (onlyProposer) {
      let updatedPayload = {
        ...payload,
        proposal_id: proposalData?.proposal?.id || proposerDetails.id,
      };
      if (
        type === "CANCER" ||
        type === "CRITICAL" ||
        type === "CARDIAC" ||
        type === "HEALTH" ||
        type === "HOSPICASH"
      ) {
        dispatch(postProposalData(updatedPayload));
      } else if (type === "PA") {
        dispatch(paPostProposalData(updatedPayload));
      }
      navigate(
        `../${
          proposalData?.proposal?.id || routerParams.proposal_id
        }/make-payment`,
        {
          state: {
            proposalData: payload,
          },
        }
      );
    }
  }, [onlyProposer]);

  const headerClassname = (keyVal: number) => {
    if (!isMobile) {
      if (defaultKey === keyVal) return "collapse-header-text";
      else return "header-text-inactive";
    }
  };

  const ProposerHeader = () => {
    const showAddress = () => {
      if (
        proposerDetails.address ||
        proposerDetails.pin_code ||
        proposerDetails.city ||
        proposerDetails.state
      )
        return true;
    };
    const showNominee = () => {
      if (
        proposerDetails.nominee_name ||
        proposerDetails.nominee_age ||
        proposerDetails.nominee_relation
      )
        return true;
    };
    return (
      <div onClick={() => setDefaultKey(1)}>
        <Row
          className={
            defaultKey === 1
              ? "p-top-bottom-8"
              : "p-top-bottom-8 collapse-header-inactive"
          }
          justify={"space-between"}
        >
          <Col xs={12} span={10}>
            <span
              hidden={isMobile}
              className={
                defaultKey === 1 ? "collape-num-active" : "collape-num-inactive"
              }
            >
              1
            </span>{" "}
            <Text
              className={
                !isMobile
                  ? headerClassname(1)
                  : "collapse-header-text-mob ml-10"
              }
            >
              Proposer Details
            </Text>
          </Col>
          <Col xs={6} span={2}>
            {defaultKey === 2 &&
              (isMobile ? (
                <Button className="edit-btn text-uppercase">Edit</Button>
              ) : (
                <Button className="edit-btn-text">Change</Button>
              ))}
          </Col>
        </Row>
        {defaultKey === 2 && (
          <Row className="collapse-inactive-text">
            <Col xs={24} lg={24}>
              <Text strong className="inactive-text">{`${
                proposerDetails.first_name + " " + proposerDetails.last_name
              }`}</Text>
              <Text className="inactive-text">
                {` ${proposerDetails.email_id}`}
              </Text>
              <Text className="inactive-text">
                {`${proposerDetails.mobile_no}`}
              </Text>
              {isMobile && <br />}
              <Text className="inactive-text text-uppercase">
                {`PAN: ${proposerDetails.pan_id}`}
              </Text>
            </Col>
            <Divider className="mt-5 mb-5" />
            <Col xs={24} lg={24}>
              {showAddress() && (
                <>
                  <Text className="inactive-text">
                    {`${proposerDetails.address && proposerDetails.address} `}
                  </Text>
                  <br />
                  <Text className="inactive-text">
                    {`
                  ${
                    proposerDetails.pin_code && proposerDetails.pin_code + ","
                  } ${proposerDetails.city && proposerDetails.city + ","} ${
                      proposerDetails.state && proposerDetails.state
                    }`}
                  </Text>
                </>
              )}
            </Col>
            <Divider className="mt-5 mb-5" />
            <Col>
              {showNominee() && (
                <>
                  <Text className="inactive-text">Nominee:</Text>
                  <Text strong className="inactive-text">
                    {`${proposerDetails.nominee_name}, ${proposerDetails.nominee_age} yrs, ${proposerDetails.nominee_relation}`}
                  </Text>
                </>
              )}
            </Col>
          </Row>
        )}
      </div>
    );
  };

  const MemberHeader = () => {
    return (
      <div>
        <span
          hidden={isMobile}
          className={
            defaultKey === 2 ? "collape-num-active" : "collape-num-inactive"
          }
        >
          2
        </span>
        <Text
          className={
            !isMobile ? headerClassname(2) : "collapse-header-text-mob ml-10"
          }
        >
          Member details
        </Text>
      </div>
    );
  };

  const onProposerFormSubmit = (values: any) => {
    localStorage.setItem("proposalFormData", JSON.stringify(values));
    setProposerDetails(form.getFieldsValue);
    form
      .validateFields()
      .then(() => {
        if (!validateNominee()) {
          if (form.getFieldValue("nominee_relation") === "GrandParent") {
            throw new Error(
              "Grand Parent age should be atleast 36 more than you"
            );
          } else if (form.getFieldValue("nominee_relation") === "Parent") {
            throw new Error("Parent age should be atleast 18 more than you");
          } else if (form.getFieldValue("nominee_relation") === "Child") {
            throw new Error("Parent age should be atleast 18 more than Child");
          } else if (
            form.getFieldValue("nominee_relation") === "Sibiling" ||
            form.getFieldValue("nominee_relation") === "Spouse"
          ) {
            throw new Error("Nominee should be atleast 18 years old");
          }
        }
        if (selectedMembersArray.length === 1) {
          setOnlyProposer(true);
        } else {
          setDefaultKey(2);
        }
      })
      .catch((error) => {
        if (error) {
          setNomineeAgeError(error.message);
        }
      });
  };

  const disabledDates = (current: any) => {
    return (
      current &&
      (current.valueOf() < dayjs().subtract(proposerData[0].age + 1, "year") ||
        current.valueOf() > dayjs().subtract(proposerData[0].age, "year"))
    );
  };

  const handleOnBlur = () => {
    const nomineeAge = form.getFieldValue("nominee_age");
    const nomineeRelation = form.getFieldValue("nominee_relation");
    const nomineeDetails = {
      age: nomineeAge,
      relation: nomineeRelation,
    };
    localStorage.setItem("nominee_details", JSON.stringify(nomineeDetails));
  };

  // validation for selected nominee
  const validateNominee = () => {
    const nomineeDetails = JSON.parse(
      localStorage.getItem("nominee_details") || "{}"
    );
    const proposerAge = proposerData[0].age;

    switch (nomineeDetails.relation) {
      case "Spouse":
      case "Sibilings":
        if (+nomineeDetails.age > 18) {
          return true;
        } else {
          setIsNomineeAgeValid(false);
          return false;
        }
        break;
      case "Parent":
        if (+nomineeDetails.age - proposerAge > 18) {
          return true;
        } else {
          setIsNomineeAgeValid(false);
          return false;
        }
        break;
      case "GrandParent":
        if (+nomineeDetails.age - proposerAge > 36) {
          return true;
        } else {
          setIsNomineeAgeValid(false);
          return false;
        }
        break;
      case "Child":
        if (proposerAge - +nomineeDetails.age > 18) {
          return true;
        } else {
          setIsNomineeAgeValid(false);
          return false;
        }
        break;
      default:
        setIsNomineeAgeValid(true);
        setNomineeAgeError("");
    }
  };

  const handleFormSelectValuesChange = (changedValues: any, allValues: any) => {
    if ("state" in changedValues) {
      form.setFieldsValue({
        city: undefined,
      });
    }
  };

  const statesArray = () => {
    let states = statesData?.map((state: any) => {
      return {
        value: state.slug,
        label: state.name,
      };
    });
    return states || [];
  };

  const getCitiesBasedOnState = (slug = "andhra-pradesh") => {
    let state = statesData?.filter((state: any) => {
      return state.slug === slug;
    });
    let cities = state[0]?.cities?.map((city: any) => {
      return {
        value: city.slug,
        label: city.name,
      };
    });
    return cities || [];
  };
  gender === "male"
    ? (proposerData[0].id = "husband")
    : (proposerData[0].id = "wife");
  proposerData[0].image = `/static/img/icons/health/member-${gender}.svg`;

  return (
    <React.Fragment>
      {isMobile ? (
        <Header
          type={type}
          hideLogo
          title={`${type} INSURANCE PROPOSAL FORM`}
          showBackIcon
          // editFunction={() => navigate("../results")}
        />
      ) : (
        <Header type={type} title={`${type} INSURANCE PROPOSAL FORM`} />
      )}

      {isMobile && <GcSummaryCard isMobile={isMobile} type={type} />}
      <div className="d-flex justify-content-center">
        <div className="planBody">
          {localLoading ? (
            <AppLoader />
          ) : (
            <Row>
              <Col xs={24} sm={16} lg={16}>
                <Collapse
                  bordered={!isMobile}
                  // defaultActiveKey={[defaultKey]}
                  activeKey={defaultKey}
                  className="proposer-collapse"
                  accordion
                >
                  {/* proposer details panel */}
                  <Collapse.Panel
                    showArrow={false}
                    header={<ProposerHeader />}
                    key="1"
                    style={!isMobile ? panelStyleDesktop : panelStyleMobile}
                    className={
                      !isMobile && defaultKey === 1
                        ? "collapse-panel-active"
                        : "collapse-panel-inactive"
                    }
                  >
                    <Form
                      form={form}
                      initialValues={proposerDetails}
                      onFinish={onProposerFormSubmit}
                      onValuesChange={handleFormSelectValuesChange}
                    >
                      <Row wrap={true}>
                        <Col
                          className={`d-flex ${
                            !isMobile
                              ? "flex-direction-col align-item-center"
                              : "mob-avatar"
                          }  `}
                          xs={24}
                          lg={4}
                        >
                          <Avatar
                            className="avatar-rounded"
                            size={{
                              xs: 40,
                              sm: 40,
                              md: 56,
                              lg: 56,
                              xl: 56,
                              xxl: 100,
                            }}
                            src={proposerData[0]?.image}
                          />
                          <Text>You</Text>
                        </Col>
                        <Col xs={24} lg={18}>
                          <Row justify="space-between" gutter={[16, 16]}>
                            <Col span={12}>
                              <Form.Item
                                name="first_name"
                                rules={[
                                  {
                                    required: true,
                                    message: "Please Enter your First Name",
                                  },
                                  {
                                    pattern: /^[a-zA-Z]+$/,
                                    message: "Please Enter a valid first name",
                                  },
                                ]}
                              >
                                <FloatInput
                                  style={{ textTransform: "capitalize" }}
                                  size="large"
                                  label="First Name"
                                  // value={proposerDetails?.first_name || ""}
                                />
                              </Form.Item>
                            </Col>
                            <Col span={12}>
                              <Form.Item
                                name="last_name"
                                rules={[
                                  {
                                    required: true,
                                    message: "Please Enter your Last Name",
                                  },
                                  {
                                    pattern: /^[a-zA-Z]+$/,
                                    message: "Please Enter a valid last name",
                                  },
                                ]}
                              >
                                <FloatInput
                                  style={{ textTransform: "capitalize" }}
                                  size="large"
                                  label="Last Name"
                                  // value={proposerDetails?.last_name || ""}
                                />
                              </Form.Item>
                            </Col>
                            {isMobile && (
                              <Col
                                className="d-flex align-item-center"
                                span={24}
                              >
                                <Text className="font-bold">Gender:</Text>
                                <Form.Item
                                  name="gender"
                                  rules={[
                                    {
                                      required: false,
                                    },
                                  ]}
                                >
                                  <Radio.Group
                                    className="ml-10"
                                    onChange={(e) => setGender(e.target.value)}
                                    value={gender}
                                    defaultValue={gender}
                                  >
                                    <Radio value={"male"}>Male</Radio>
                                    <Radio value={"female"}>Female</Radio>
                                  </Radio.Group>
                                </Form.Item>
                              </Col>
                            )}

                            <Col xs={24} lg={12}>
                              <Row align={"middle"}>
                                <Col xs={8} lg={0}>
                                  <Text className="font-bold">
                                    Date of Birth:
                                  </Text>
                                </Col>
                                <Col xs={16} lg={24}>
                                  <Form.Item
                                    className="date-picker-proposal"
                                    name="dob"
                                    rules={[
                                      {
                                        required: true,
                                        message: "Please input your DOB",
                                      },
                                    ]}
                                  >
                                    <DatePicker
                                      size="large"
                                      className="full-width h-40"
                                      placeholder="Date of Birth"
                                      format={dateFormat}
                                      disabledDate={(current: any) =>
                                        disabledDates(current)
                                      }
                                    />
                                  </Form.Item>
                                </Col>
                              </Row>
                            </Col>
                            <Col xs={24} lg={12}>
                              <Form.Item
                                name="pan_id"
                                rules={[
                                  {
                                    required: true,
                                    message: "Please input your Pan ID",
                                  },
                                  {
                                    pattern: new RegExp(
                                      /^[a-zA-Z]{3}[P|p][a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/
                                    ),
                                    message: "Please input a valid Pan",
                                  },
                                ]}
                              >
                                <FloatInput
                                  className={isMobile ? "mb-10 " : ""}
                                  style={{ textTransform: "uppercase" }}
                                  label="Pan Card"
                                  maxLength={10}
                                  size="large"
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                          {!isMobile && <Divider />}
                          <Row gutter={[16, 16]}>
                            <Col xs={24} lg={12}>
                              <Form.Item
                                name="email_id"
                                rules={[
                                  {
                                    required: true,
                                    type: "email",
                                    message: "Please input your email id",
                                  },
                                ]}
                              >
                                <FloatInput
                                  size="large"
                                  label="Email Address"
                                  // value={proposerDetails?.email_id || ""}
                                />
                              </Form.Item>
                            </Col>
                            <Col
                              className={isMobile ? "mb-20" : ""}
                              xs={24}
                              lg={12}
                            >
                              <Form.Item
                                name="mobile_no"
                                rules={[
                                  {
                                    required: true,
                                    message:
                                      "Please input your valid mobile number",
                                    pattern: new RegExp(/^[6-9]\d{9}$/),
                                  },
                                ]}
                              >
                                <FloatInput
                                  inputType="number"
                                  className="full-width"
                                  defaultValue={proposerDetails?.mobile_no}
                                  type="number"
                                  maxLength={10}
                                  size="large"
                                  label="Mobile Number"
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                          {!isMobile && <Divider />}
                          <div className={isMobile ? "mb-20" : ""}>
                            <Text className="font-bold">
                              Communication Address
                            </Text>
                            <Form.Item
                              name="address"
                              rules={[
                                {
                                  required: true,
                                  message: "Please provide your address",
                                },
                                {
                                  pattern: new RegExp(/[a-zA-Z0-9\s,./]+$/),
                                  message:
                                    "Valid characters include a-Z, 0-9 and .,-, /",
                                },
                                {
                                  max: 60,
                                  message: "Character Limit is 60",
                                },
                              ]}
                            >
                              <TextArea
                                className="mt-5"
                                placeholder="Flat/House no, street"
                                autoSize={{ minRows: 3, maxRows: 6 }}
                              />
                            </Form.Item>

                            <Row className="mt-5" gutter={[16, 16]}>
                              <Col xs={24} lg={12}>
                                <Form.Item
                                  name="landmark"
                                  rules={[
                                    {
                                      pattern: new RegExp(/[a-zA-Z0-9\s,./]+$/),
                                      message:
                                        "Valid characters include a-Z, 0-9 and .,-, /",
                                    },
                                    {
                                      max: 40,
                                      message: "Character Limit is 40",
                                    },
                                  ]}
                                >
                                  <FloatInput size="large" label="Landmark" />
                                </Form.Item>
                              </Col>
                              <Col xs={24} lg={12}>
                                <Form.Item
                                  name="pin_code"
                                  rules={[
                                    {
                                      required: true,
                                      pattern: new RegExp(/^[1-9]{1}[0-9]{5}$/),
                                      message: "Please enter a valid pin code",
                                    },
                                  ]}
                                >
                                  <FloatInput
                                    inputType="number"
                                    maxLength={6}
                                    size="large"
                                    type="number"
                                    label="Pin Code"
                                    value={proposerDetails?.pin_code || ""}
                                  />
                                </Form.Item>
                              </Col>
                              <Col xs={24} lg={12}>
                                <Form.Item
                                  name="state"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please choose your state!",
                                    },
                                  ]}
                                >
                                  <FloatInput
                                    showSearch={true}
                                    inputType="select"
                                    label="Select State"
                                    value={proposerDetails?.state || ""}
                                    options={statesArray()}
                                    onChange={(value) =>
                                      setSelectedState(value)
                                    }
                                  />
                                </Form.Item>
                              </Col>
                              <Col xs={24} lg={12}>
                                <Form.Item
                                  name="city"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please choose your city",
                                    },
                                  ]}
                                >
                                  <FloatInput
                                    showSearch={true}
                                    inputType="select"
                                    label="Select City"
                                    value={proposerDetails?.city || ""}
                                    options={getCitiesBasedOnState(
                                      selectedState
                                    )}
                                    disabled={selectedState ? false : true}
                                  />
                                </Form.Item>
                              </Col>
                            </Row>
                          </div>

                          {!isMobile && <Divider />}
                          <Text className="font-bold">Nominee Details</Text>
                          <Row className="mt-5" wrap={true} gutter={[16, 16]}>
                            <Col xs={24} lg={12}>
                              <Form.Item
                                name="nominee_name"
                                rules={[
                                  {
                                    required: true,
                                    message: "Please provide a nominee!",
                                  },
                                  {
                                    pattern: new RegExp(/[a-zA-Z]+$/),
                                    message: "Only alphabets allowed",
                                  },
                                  {
                                    max: 32,
                                    message: "Character Limit is 32",
                                  },
                                ]}
                              >
                                <FloatInput
                                  inputType="alphabet"
                                  maxLength={32}
                                  size="large"
                                  label="Nominee Name"
                                  value={proposerDetails?.nominee_name || ""}
                                />
                              </Form.Item>
                            </Col>
                            <Col xs={16} lg={8}>
                              <Form.Item
                                name="nominee_relation"
                                rules={[
                                  {
                                    required: true,
                                    message: "Required Field!",
                                  },
                                ]}
                              >
                                <FloatInput
                                  inputType="select"
                                  size="large"
                                  label="Select Relation"
                                  value={
                                    proposerDetails?.nominee_relation || ""
                                  }
                                  onChange={handleOnBlur}
                                  options={[
                                    { value: "Husband", label: "Husband" },
                                    { value: "Wife", label: "Wife" },
                                    { value: "Father", label: "Father" },
                                    { value: "Mother", label: "Mother" },
                                    { value: "Son", label: "Son" },
                                    { value: "Daughter", label: "Daughter" },
                                    { value: "Brother", label: "Brother" },
                                    { value: "Sister", label: "Sister" },
                                    { value: "Other", label: "Other" },
                                  ]}
                                />
                              </Form.Item>
                            </Col>
                            <Col xs={8} lg={4}>
                              <Form.Item
                                name="nominee_age"
                                validateStatus={
                                  isNomineeAgeValid ? "success" : "error"
                                }
                                rules={[
                                  {
                                    required: true,
                                    pattern: new RegExp(/^(?!0)[1-9]\d*$/),
                                    message: "Please enter the nominee age!",
                                  },
                                ]}
                              >
                                <FloatInput
                                  inputType="number"
                                  maxLength={2}
                                  size="large"
                                  label="Age"
                                  value={proposerDetails?.nominee_age || ""}
                                  type="number"
                                  onBlur={handleOnBlur}
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      <Row justify={"center"}>
                        {nomineeAgeError && (
                          <span className="age-validation-error mt-5">
                            {nomineeAgeError}
                          </span>
                        )}
                      </Row>
                      <Row justify={"center"}>
                        <Form.Item>
                          <Button
                            htmlType="submit"
                            className="proposer-btn text-uppercase"
                          >
                            continue
                          </Button>
                        </Form.Item>
                      </Row>
                    </Form>
                  </Collapse.Panel>
                  {/* member details panel */}
                  {type != "PA" && selectedMembersArray.length > 1 && (
                    <Collapse.Panel
                      showArrow={false}
                      header={<MemberHeader />}
                      key="2"
                      style={!isMobile ? panelStyleDesktop : panelStyleMobile}
                      className={
                        !isMobile && defaultKey === 2
                          ? "collapse-panel-active"
                          : "collapse-panel-inactive remove-boxShadow"
                      }
                    >
                      <MemberDetails
                        proposerData={proposerData[0]}
                        isMobile={isMobile}
                        proposerDOB={proposerDetails.dob}
                        type={type}
                      />
                    </Collapse.Panel>
                  )}
                </Collapse>
              </Col>
              <Col xs={0} sm={8} lg={8}>
                <GcSummaryCard isMobile={isMobile} type={type} />
                {/* <AddParentsInfoCard /> */}
              </Col>
            </Row>
          )}
        </div>
        {/* <Modal
          open={true}
          footer={null}
          title={
            <Text className="d-flex justify-content-center fs-18">
              Premium Change Alert!
            </Text>
          }
        >
          <PremiumChangeModal />
        </Modal> */}
      </div>
    </React.Fragment>
  );
};

export default GcProposarDetails;
