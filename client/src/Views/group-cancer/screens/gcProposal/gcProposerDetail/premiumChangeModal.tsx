import { rupeeFormat } from "@/utils/common-utils";
import { Row, Col, Typography, Button } from "antd";
import React from "react";

type Props = {};

export const PremiumChangeModal = (props: Props) => {
  const { Text } = Typography;
  return (
    <div
      className="d-flex flex-direction-col justify-center align-item-middle"
      style={{ backgroundColor: "white" }}
    >
      <div>
        <Typography className="fs-16 age-change-sub-title">
          Based of birthdates of insured member(s), premium of the policy has
          <b className="red ml-5">increased</b>
        </Typography>
      </div>
      <div className="member-age-div">
        <Row className="p-top-bottom-8" align={"middle"} justify={"center"}>
          <Col span={4} className="text-center">
            <img
              className="ml-10"
              src="/static/img/icons/health/member-male.svg"
            />
          </Col>
          <Col span={20}>
            <Text strong>Ramesh Sippy</Text>
            <Row gutter={48}>
              <Col>
                <Text>
                  Prev Age: <b>35 yrs</b>
                </Text>
              </Col>
              <Col>
                <Text>
                  New Age: <b>36 yrs</b>
                </Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div className="premium-change-box">
        <Row className="p-10" align={"middle"} justify={"center"}>
          <Col span={10} className="d-flex flex-direction-col text-center">
            <Text>Old Premium</Text>
            <Text>{`${rupeeFormat(1800)}`}</Text>
          </Col>
          <Col span={4}>
            <img
              className="ml-10"
              src="/static/img/icons/health/arrow-right-bold.svg"
            />
          </Col>
          <Col span={10} className="d-flex flex-direction-col">
            <Text>New Premium</Text>
            <Text strong>{`${rupeeFormat(2800)}`}</Text>
          </Col>
        </Row>
      </div>
      <div className="d-flex flex-direction-col p-top-bottom-8 update-btn">
        <Button className="fs-16">
          <b>Accept and Proceed</b>
        </Button>
      </div>
      <div className="cancel-btn text-center">
        <p>Cancel</p>
      </div>
    </div>
  );
};
