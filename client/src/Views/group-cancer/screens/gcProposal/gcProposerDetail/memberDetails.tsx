import {
  getSelectedMemberArray,
  postProposalData,
  setProposalStatus,
} from "@/actions/group-cancer/actions";
import {
  postProposalData as paPostProposalData,
  setProposalStatus as paSetProposalStatus,
} from "@/actions/group-pa/actions";
import AppLoader from "@/components/appLoader";
import { useProductState } from "@/hooks/useProductState";
import { Avatar, Button, Col, DatePicker, Form, Row, Typography } from "antd";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import FloatInput from "../../../common/floatInput";
import {
  calculateTotalPremium,
  generateLogEventPayload,
  mapMembersTitle,
  rupeeFormat,
} from "@/utils/common-utils";

interface Iprops {
  isMobile: boolean | undefined;
  proposerData: any;
  proposerDOB: any;
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
}
const dateFormat = "DD-MM-YYYY";
const MemberDetails = (props: Iprops) => {
  const dispatch = useDispatch();
  const routerParams: any = useParams();
  const { Text } = Typography;
  const { isMobile } = props;
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const productState = useProductState(props.type);
  const {
    proposalData,
    selectedMembersArray,
    isProposalSuccess,
    selectedGender,
    singlePlan,
    configVariables,
  } = productState;
  const [memberDetails, setMemberDetails] = useState<any>();
  // const [memberDefaultVals, setMemberDefaultVals] = useState({});
  const [isFormSubmit, setFormSubmit] = useState(false);
  const [loading, setLoading] = useState(true);
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];
  // const [isProposer, setIsProposer] = useState<boolean>(false);
  const purchaseAmount = calculateTotalPremium(
    proposalData?.proposal?.premium_amount
  );

  const membersId = [...new Set(selectedMembersArray.map((v: any) => v.id))];
  const proposerData = selectedMembersArray.filter(
    (key: any) => key.title === "You"
  );
  // if (proposerData) {
  //   form.setFieldsValue({
  //     [`${proposerData[0].id}_fName`]: proposerData[0].name.split(" ")[0],
  //     [`${proposerData[0].id}_lName`]: proposerData[0].name.split(" ")[1],
  //     [`${proposerData[0].id}_dob`]: props.proposerDOB,
  //   });
  // }
  const member_details: any = [];
  const checkIfProposer = (key: any) => {
    if (key.title === "You") return true;
  };

  useEffect(() => {
    if (
      props.type === "CANCER" ||
      props.type === "CRITICAL" ||
      props.type === "CARDIAC"
    ) {
      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["members_details"],
          win["pageId"]["proposal_form"]
        )
      );
    }
  }, []);

  useEffect(() => {
    if (isProposalSuccess) {
      if (
        props.type === "CANCER" ||
        props.type === "CRITICAL" ||
        props.type === "CARDIAC" ||
        props.type === "HEALTH" ||
        props.type === "HOSPICASH"
      ) {
        dispatch(setProposalStatus(false));
      } else if (props.type === "PA") {
        dispatch(paSetProposalStatus(false));
      }
    }
  }, [isProposalSuccess]);

  useEffect(() => {
    if (isFormSubmit) {
      updateProposalData();

      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["members_details"],
          win["pageId"]["proposal_form"],
          win["logType"]["submit"],
          {
            proposal_id: proposalData?.proposal?.id || null,
          }
        )
      );
    }
  }, [isFormSubmit]);

  useEffect(() => {
    setLoading(false);
  }, [loading]);

  useEffect(() => {
    if (selectedMembersArray.length) {
      selectedMembersArray.map((member: any) => {
        if (member.id === "son" || member.id === "daughter") {
          form.setFieldValue(
            `${member.title}_dob`,
            dayjs(new Date()).subtract(member.age, "year")
          );
          member?.fname &&
            form.setFieldValue(
              `${member.title.toLowerCase()}_fName`,
              member.fname
            );
          member?.lname &&
            form.setFieldValue(
              `${member.title.toLowerCase()}_lName`,
              member.lname
            );
        } else {
          form.setFieldValue(
            `${member.id}_dob`,
            dayjs(new Date()).subtract(member.age, "year")
          );
          form.setFieldValue(
            `${member.title.toLowerCase()}_fName`,
            member.fname
          );
          form.setFieldValue(
            `${member.title.toLowerCase()}_lName`,
            member.lname
          );
        }
      });
    }
  }, []);

  useEffect(() => {
    const memberDefaultVals = JSON.parse(
      localStorage.getItem("localMemberDetails") || "{}"
    );
    if (memberDefaultVals != "{}") {
      const modifiedObject = Object.entries(memberDefaultVals).reduce(
        (acc: any, [key, value]: any) => {
          if (key.includes("dob")) {
            acc[key] = dayjs(value);
          } else {
            acc[key] = value;
          }
          return acc;
        },
        {}
      );
      form.setFieldsValue(modifiedObject);
    }
  }, []);

  const childrenDetails = (member_obj: any, key: any) => {
    let i: number = 1;
    let tempArray: any = [];
    Object.keys(member_obj).map((item) => {
      if (item.includes(`${key}${i}`)) {
        const tempMember = selectedMembersArray.filter(
          (ele: any) => ele.id === key
        );
        let temp: any = {};
        temp["fname"] = member_obj[`${key}${i}_fName`];
        temp["lname"] = member_obj[`${key}${i}_lName`];
        temp["dob"] = member_obj[`${key}${i}_dob`];
        temp["image"] = tempMember[0].image;
        temp["title"] = `${key} ${i}`;
        tempArray.push(temp);
        i++;
      }
      return tempArray;
    });
    if (key === "son") {
      member_details.push({ son_data: tempArray });
    } else {
      member_details.push({ daughter_data: tempArray });
    }
  };

  const parentDetails = (member_obj: any, key: any) => {
    let tempArray: any = {};
    const tempMember = selectedMembersArray.filter(
      (ele: any) => ele.id === key
    );
    Object.keys(member_obj).forEach((item) => {
      if (item.includes(key)) {
        tempArray["fname"] = member_obj[`${key}_fName`];
        tempArray["lname"] = member_obj[`${key}_lName`];
        tempArray["dob"] = member_obj[`${key}_dob`];
        tempArray["image"] = tempMember[0].image;
        tempArray["title"] = tempMember[0].title;
      }
      return tempArray;
    });
    if (key === "husband") {
      member_details.push({ husband_data: tempArray });
    } else if (key === "wife") {
      member_details.push({ wife_data: tempArray });
    } else if (key === "mother") {
      member_details.push({ mother_data: tempArray });
    } else {
      member_details.push({ father_data: tempArray });
    }
  };

  const updateProposalData = () => {
    let payload = {
      proposal_id: proposalData?.proposal?.id,
      memberDetails: memberDetails,
    };
    if (
      props.type === "CANCER" ||
      props.type === "CRITICAL" ||
      props.type === "CARDIAC" ||
      props.type === "HEALTH" ||
      props.type === "HOSPICASH"
    ) {
      dispatch(postProposalData(payload));
    } else if (props.type === "PA") {
      dispatch(paPostProposalData(payload));
    }
    navigate(`../${proposalData.proposal.id}/make-payment`, {
      state: {
        planId: singlePlan?.planId,
        proposalData: proposalData?.proposal,
      },
    });
  };

  const onMembersFormSubmit = (members_obj: any) => {
    const proposerId =
      selectedGender === "male" || proposalData?.proposal?.gender === "male"
        ? "husband"
        : "wife";
    membersId
      .filter((id: any) => !["son", "daughter", proposerId].includes(id))
      .forEach((id: any) => parentDetails(members_obj, id));
    membersId
      .filter((id: any) => ["son", "daughter"].includes(id))
      .forEach((id: any) => childrenDetails(members_obj, id));

    setMemberDetails(member_details);
    localStorage.setItem("localMemberDetails", JSON.stringify(members_obj));
    setFormSubmit(true);
  };

  const disabledDates = (current: any, age: number) => {
    return (
      current &&
      (current.valueOf() < dayjs().subtract(age + 1, "year") ||
        current.valueOf() > dayjs().subtract(age, "year"))
    );
  };

  const premiumChangeAlertJSX = () => {};

  return (
    <Form
      form={form}
      // initialValues={memberDefaultVals}
      onFinish={onMembersFormSubmit}
    >
      {loading ? (
        <AppLoader />
      ) : (
        selectedMembersArray.map((v: any, idx: any) => {
          return (
            v.title !== "You" && (
              <Row key={idx} wrap={true} className="mt-5">
                <Col
                  className={`d-flex ${
                    !isMobile
                      ? "flex-direction-col align-item-center"
                      : "mob-avatar"
                  }  `}
                  xs={24}
                  lg={4}
                >
                  <Avatar
                    className="avatar-rounded"
                    size={{ xs: 40, sm: 40, md: 56, lg: 56, xl: 56, xxl: 100 }}
                    src={v.image}
                  />
                  <Text
                    style={{ textTransform: "capitalize", textAlign: "center" }}
                  >
                    {mapMembersTitle(v.title, v.id)}
                  </Text>
                </Col>
                <Col xs={24} lg={18}>
                  <Row justify="space-between" gutter={[16, 16]}>
                    <Col span={12}>
                      {" "}
                      <Form.Item
                        className="input-members"
                        rules={[
                          {
                            required: true,
                            message: "Please Enter the First Name",
                          },
                          {
                            pattern: /^[a-zA-Z]+$/,
                            message: "Please Enter a valid first name",
                          },
                        ]}
                        key={idx}
                        // children will have suffix number after key, while parents will not
                        name={`${
                          v.id === "son" || v.id === "daughter" ? v.title : v.id
                        }_fName`}
                      >
                        <FloatInput
                          value={
                            proposerData &&
                            checkIfProposer(v) &&
                            proposerData[0].name.split(" ")[0]
                          }
                          // disabled={checkIfProposer(v)}
                          className="h-40"
                          label="First Name"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      {" "}
                      <Form.Item
                        className="input-members"
                        rules={[
                          {
                            required: true,
                            message: "Please Enter the Last Name",
                          },
                          {
                            pattern: /^[a-zA-Z]+$/,
                            message: "Please Enter a valid last name",
                          },
                        ]}
                        key={idx}
                        name={`${
                          v.id === "son" || v.id === "daughter" ? v.title : v.id
                        }_lName`}
                      >
                        <FloatInput
                          value={
                            proposerData &&
                            checkIfProposer(v) &&
                            proposerData[0].name.split(" ")[1]
                          }
                          // disabled={checkIfProposer(v)}
                          className="h-40"
                          label="Last Name"
                        />
                      </Form.Item>{" "}
                    </Col>

                    <Col span={24}>
                      <Row align={"middle"}>
                        <Col xs={8} lg={0}>
                          <Text className="font-bold">Date of Birth:</Text>
                        </Col>
                        <Col xs={16} lg={12}>
                          {" "}
                          <Form.Item
                            className="date-picker-members"
                            name={`${
                              v.id === "son" || v.id === "daughter"
                                ? v.title
                                : v.id
                            }_dob`}
                            key={idx}
                            rules={[
                              {
                                required: true,
                                message: "Please input your DOB",
                              },
                            ]}
                          >
                            <DatePicker
                              // disabled={checkIfProposer(v)}
                              disabledDate={(current: any) =>
                                disabledDates(current, v.age)
                              }
                              size="large"
                              className="full-width h-40"
                              placeholder="Date of Birth"
                              format={dateFormat}
                            />
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            )
          );
        })
      )}

      <Row justify={"center"}>
        <Form.Item>
          <Button className="proposer-btn text-uppercase" htmlType="submit">
            Confirm & Pay ({rupeeFormat(purchaseAmount.totalPremium)})
          </Button>
        </Form.Item>
      </Row>
    </Form>
  );
};

export default MemberDetails;
