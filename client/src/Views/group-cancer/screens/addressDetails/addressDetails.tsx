import {
  getSelectedMemberArray,
  getSelectedMemberDetails,
  goToPrequotePreviousScreen,
} from "@/actions/group-cancer/actions";
import {
  getSelectedMemberArray as paGetSelectedMemberArray,
  getSelectedMemberDetails as paGetSelectedMemberDetails,
  goToPrequotePreviousScreen as paGoToPrequotePreviousScreen,
} from "@/actions/group-pa/actions";
import Button from "@/components/button/button";
import { useProductState } from "@/hooks/useProductState";
import { RootStore } from "@/store";
import { generateLogEventPayload } from "@/utils/common-utils";
import { Form, Input, InputNumber } from "antd";
import { useState, KeyboardEvent } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

type Iprops = {
  selectedMemberList?: any;
  selectedUser?: string;
  type: "CANCER" | "PA" | "CRITICAL" | "CARDIAC" | "HEALTH" | "HOSPICASH";
};

export default function AddressDetails(props: Iprops) {
  const [isshow, setIsShow] = useState(false);
  const dispatch = useDispatch();
  const productState = useProductState(props.type);
  const {
    selectedMembersObject,
    currentScreen,
    selectedMembersArray,
    configVariables,
    prequoteData,
  } = productState;
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const win = (window as any)["sharedConstants"];
  const logEvent = (window as any)["sharedUtils"]["logEvent"];

  var obj = selectedMembersObject;
  var memberarray: string[] = [];
  let ChildrenCount = 0;
  Object.keys(obj).map((mem: string) => {
    if ((mem === "husband" || mem === "wife") && obj[mem] > 0) {
      memberarray.push(" spouse");
    } else if ((mem === "father" || mem === "mother") && obj[mem] > 0) {
      memberarray.push(` ${mem}`);
    } else {
      if (mem === "daughter" || mem === "son") {
        ChildrenCount = ChildrenCount + obj[mem];
      }
    }
  });

  if (ChildrenCount === 1) {
    memberarray.push(" kid");
  } else if (ChildrenCount > 1) {
    memberarray.push(" kids");
  }

  const handleShowMore = () => {
    setIsShow((prevState: boolean) => !prevState);
  };
  const handleEditBtn = () => {
    if (
      props.type === "CANCER" ||
      props.type === "CRITICAL" ||
      props.type === "CARDIAC" ||
      props.type === "HEALTH" ||
      props.type === "HOSPICASH"
    ) {
      if (currentScreen == 4) {
        goToPrequotePreviousScreen(dispatch);
        goToPrequotePreviousScreen(dispatch);
      }
    }
    if (props.type === "PA") {
      if (currentScreen == 3) {
        paGoToPrequotePreviousScreen(dispatch);
        paGoToPrequotePreviousScreen(dispatch);
      }
    }
  };

  const handleForm = (values: any) => {
    const memberArray = [...selectedMembersArray];
    memberArray.map((member) => {
      if (member.title === "You") {
        member.name = prequoteData?.name || values.name.trim();
        member.phone = prequoteData?.phone || values.phone;
      }
    });
    if (
      props.type === "CANCER" ||
      props.type === "CRITICAL" ||
      props.type === "CARDIAC" ||
      props.type === "HEALTH" ||
      props.type === "HOSPICASH"
    ) {
      getSelectedMemberArray(memberArray, dispatch);
      getSelectedMemberDetails(values, dispatch);

      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["member_select_submit"],
          win["pageId"]["pre_quote_page"],
          win["logType"]["submit"],
          memberArray[0]
        )
      );
    }
    if (props.type === "PA") {
      paGetSelectedMemberArray(memberArray, dispatch);
      paGetSelectedMemberDetails(values, dispatch);

      logEvent(
        configVariables.logEvent,
        generateLogEventPayload(
          win["widget"]["member_select_submit"],
          win["pageId"]["pre_quote_page"],
          win["logType"]["submit"],
          memberArray[0]
        )
      );
    }
    return navigate("./results");
  };

  const onlyNumbers = (e: KeyboardEvent<HTMLInputElement>) => {
    const element = e.target as HTMLInputElement;
    element.value = element.value.replaceAll(/[^\d]/g, "");
  };

  return (
    <div className="address-details-box-container">
      <div className="selected-members">
        {memberarray.length == 0 ? "you" : `you,${memberarray.join(",")}`}
        <Button
          handleOnClick={handleEditBtn}
          icon="/static/img/icons/health/pencil-simple-bold.svg"
          customClass="edit-button"
        />
      </div>
      <div className="member-details-heading">
        Lastly, tell us your contact details
      </div>
      <div className="address-details-card">
        <Form
          form={form}
          initialValues={{
            name: prequoteData?.name,
            phone: prequoteData?.phone,
          }}
          onFinish={handleForm}
        >
          <div className="d-flex flex-direction-col address-box">
            <div className="name-input-box">
              <div className="input-heading">What’s your name</div>
              <Form.Item
                name="name"
                rules={[
                  {
                    required: true,
                    message: "Please Enter your full name",
                  },
                  {
                    pattern: /^[a-zA-Z]+ [a-zA-Z ]+$/,
                    message: "Please enter your lastname",
                  },
                ]}
              >
                <Input
                  size="large"
                  placeholder="Your full name"
                // defaultValue={prequoteData?.name}
                // value={prequoteData?.name}
                />
              </Form.Item>
            </div>
            <div className="phone-input-box">
              <div className="input-heading">Your mobile number</div>

              <Form.Item
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Please enter your mobile number",
                  },
                  {
                    message: "Please input your valid mobile number!",
                    pattern: new RegExp(/^[6-9]\d{9}$/),
                  },
                ]}
              >
                <Input
                  maxLength={10}
                  size="large"
                  type="tel"
                  onInput={onlyNumbers}
                  placeholder="Mobile number"
                  className="phone-number"
                  prefix="+91"
                // value={prequoteData.phone}
                // defaultValue={prequoteData?.phone}
                />
              </Form.Item>
            </div>
            <div className="extra-text">
              {!isshow ? (
                <p>
                  I hereby authorize Flipkart to communicate with me on the
                  given number for my insu…{" "}
                  <a onClick={handleShowMore}>show more</a>
                </p>
              ) : (
                <p>
                  I hereby authorize Flipkart to communicate with me on the
                  given number for my insurance.{" "}
                  <a onClick={handleShowMore}>show less</a>
                </p>
              )}
            </div>
          </div>
          <div>
            <Form.Item>
              <Button title="Continue" />
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
}
