import Header from "@/components/header/header";
import Footer from "@/components/footer/footer";
import "@/assets/css/homepage.css";
import MobileHomePage from "./MobileHomePage";
import DesktopHomePage from "./DesktopHomePage";

function Homepage({ isMobile }: any) {
  return (
    <div className="home-container">
      {/* <Header type="PA" title="Group Products" /> */}
      <section className="fk-main-container">
        <div className="fk-title-section mobile-hide">
          <h1 className="fk-title" data-cms-content="form_heading">
            Insurance Made Simple for You
          </h1>
          <div className="fk-title-sub">
            Compare and buy best insurance plans in minutes
          </div>
        </div>
        {isMobile ? <MobileHomePage /> : <DesktopHomePage />}
      </section>
      <Footer />
    </div>
  );
}

export default Homepage;
