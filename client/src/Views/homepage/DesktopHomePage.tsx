import { Link } from "react-router-dom";
import { getImageURL } from "@/utils/common-utils";
const groupCancerLogo = getImageURL("icons/ic_group_cancer.svg");
const personalAccidentLogo = getImageURL("icons/ic_personal_accident.svg");
const criticalIllnessLogo = getImageURL("icons/ic_critical_illness.svg");
const hospicashLogo = getImageURL("icons/ic_hospicash.svg");
const travelLogo = getImageURL("icons/ic_travel.svg");
const intTravelLogo = getImageURL("icons/ic_int_travel.svg");
const carLogo = getImageURL("icons/ic_car.svg");
const bikeLogo = getImageURL("icons/ic_bike.svg");
const healthLogo = getImageURL("icons/ic_health.svg");
const arogyaLogo = getImageURL("icons/ic_arogya_sanjeevani.svg");

function DesktopHomePage() {
  return (
    <section className="fk-controls-bg">
      <div className="fk-controls-box">
        <div className="fk-controls-title">Group Insurance</div>
        <div className="fk-controls">
          <Link to={"/group-cancer/"}>
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={groupCancerLogo}
                  height="48"
                  width="48"
                  alt="Group Cancer"
                />
              </div>
              <div className="fk-item-ic-name">Group Cancer</div>
            </div>
          </Link>
          <Link to={"/personal-accident/"}>
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={personalAccidentLogo}
                  height="48"
                  width="48"
                  alt="Personal Accident"
                />
              </div>
              <div className="fk-item-ic-name">Personal Accident</div>
            </div>
          </Link>
          <Link to={"/critical-illness/"}>
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={criticalIllnessLogo}
                  height="48"
                  width="48"
                  alt="Critical Illness"
                />
              </div>
              <div className="fk-item-ic-name">Critical illness</div>
            </div>
          </Link>
          <Link to={"/cardiac/"}>
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={criticalIllnessLogo}
                  height="48"
                  width="48"
                  alt="Cardiac"
                />
              </div>
              <div className="fk-item-ic-name">Cardiac</div>
            </div>
          </Link>
          <Link to={"/group-health/"}>
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={criticalIllnessLogo}
                  height="48"
                  width="48"
                  alt="Cardiac"
                />
              </div>
              <div className="fk-item-ic-name">Group Health</div>
            </div>
          </Link>
          <Link to={"/hospicash/"}>
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={hospicashLogo}
                  height="48"
                  width="48"
                  alt="Cardiac"
                />
              </div>
              <div className="fk-item-ic-name">Hospicash</div>
            </div>
          </Link>
        </div>
      </div>
      <div className="fk-controls-box">
        <div className="fk-controls-title">Upcoming Products</div>
        <div className="fk-controls">
          {/* <span className="a-no-link">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={hospicashLogo}
                  height="48"
                  width="48"
                  alt="Hospicash"
                />
              </div>
              <div className="fk-item-ic-name">
                <br />
                Hospicash
              </div>
            </div>
          </span> */}
          <span className="a-no-link">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={travelLogo}
                  height="48"
                  width="48"
                  alt="Domestic Travel"
                />
              </div>
              <div className="fk-item-ic-name">Domestic Travel</div>
            </div>
          </span>
          <span className="a-no-link">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={intTravelLogo}
                  height="48"
                  width="48"
                  alt="International Travel"
                />
              </div>
              <div className="fk-item-ic-name">International Travel</div>
            </div>
          </span>
        </div>
      </div>
      <div className="fk-controls-box">
        <div className="fk-controls-title">Other Insurances</div>
        <div className="fk-controls">
          <Link to="https://flipkart.fk.uat.coverfox.com/arogya-sanjeevani/">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={arogyaLogo}
                  height="48"
                  width="48"
                  alt="Car Insurance"
                />
              </div>
              <div className="fk-item-ic-name">Arogya Sanjeevani</div>
            </div>
          </Link>
          <Link to="http://flipkart.fk.uat.coverfox.com/car-insurance/">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={carLogo}
                  height="48"
                  width="48"
                  alt="Car Insurance"
                />
              </div>
              <div className="fk-item-ic-name">Car</div>
            </div>
          </Link>
          <Link to="http://flipkart.fk.uat.coverfox.com/two-wheeler-insurance/">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={bikeLogo}
                  height="48"
                  width="48"
                  alt="Bike Insurance"
                />
              </div>
              <div className="fk-item-ic-name">Bike</div>
            </div>
          </Link>
          <Link to="https://flipkart.fk.uat.coverfox.com/health-plan/">
            <div className="fk-control-item">
              <div className="fk-item-ic-box">
                <img
                  className="img-responsive"
                  src={healthLogo}
                  height="48"
                  width="48"
                  alt="Car Insurance"
                />
              </div>
              <div className="fk-item-ic-name">Health</div>
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
}
export default DesktopHomePage;
