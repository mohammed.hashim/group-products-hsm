const path = require("path");
const webpack = require("webpack");

module.exports = {
  webpack: {
    alias: {
      "@": path.resolve(__dirname, "src/"),
    },
    configure: (webpackConfig) => {
      webpackConfig.plugins.push(
        new webpack.PrefetchPlugin("@/utils/sharedUtils.tsx"),
        new webpack.PrefetchPlugin("@/constants/sharedConstants.tsx")
      );
      return webpackConfig;
    },
  },
};
