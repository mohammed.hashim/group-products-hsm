import { readRateChart } from "./src/utils/commonUtils";

async function restoreDB() {
  // reading rate chart

  const productName = process.argv.slice(2)[0] || "";
  if (productName === "GroupPa" || productName === "GroupCancer") {
    // await db.sequelize.sync({ force: true });
    readRateChart(productName);
  } else
    console.log(
      `${productName} is not a valid Product Name. Try GroupPa or GroupCancer`
    );
}

restoreDB();
