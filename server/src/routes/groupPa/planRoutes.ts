import express from "express";
import {
  saveProposal,
  getPaPlans,
  saveProposalCtx,
  getProposalById,
} from "../../controllers/cancer/plansController";

const groupPaPlanRoute = express.Router();

// PLAN ROUTES
groupPaPlanRoute.route("/saveProposal").post(saveProposal);
groupPaPlanRoute.route("/saveProposalCtx").post(saveProposalCtx);
groupPaPlanRoute.route("/getPlans/:age&:page").get(getPaPlans);
groupPaPlanRoute.route("/getPlans/:id").get(getPaPlans);
groupPaPlanRoute.route("/getProposal/:id").get(getProposalById);

export default groupPaPlanRoute;
