import express from "express";
import {
  saveProposal,
  getPlans,
  getSinglePlan,
  saveProposalCtx,
  getProposalById,
} from "../../controllers/cancer/plansController";

const cancerPlanRoute = express.Router();

// PLAN ROUTES
cancerPlanRoute.route("/saveProposal").post(saveProposal);
cancerPlanRoute.route("/saveProposalCtx").post(saveProposalCtx);
cancerPlanRoute.route("/getPlans/:age&:page").get(getPlans);
cancerPlanRoute.route("/getPlans/:planId/:age").get(getSinglePlan);
cancerPlanRoute.route("/getProposal/:id").get(getProposalById);

export default cancerPlanRoute;
