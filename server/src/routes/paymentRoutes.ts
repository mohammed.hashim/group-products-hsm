import express from "express";
import {
  checkout,
  paymentVerification,
  postProposalTransaction,
} from "../controllers/paymentController";

const pgRoute = express.Router();

// PAYMENT ROUTES
pgRoute.route("/checkout").post(checkout);
pgRoute.route("/paymentverification").post(paymentVerification);
pgRoute.route("/transaction").post(postProposalTransaction);
pgRoute.route("/test").get((req, res) => {
  res.status(200).send("Test success");
});

export default pgRoute;
