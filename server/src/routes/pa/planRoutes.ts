import express from "express";
import {
  saveProposal,
  getPlans,
  saveProposalCtx,
  getProposalById,
} from "../../controllers/pa/plansController";

const planRoute = express.Router();

// PLAN ROUTES
planRoute.route("/saveProposal").post(saveProposal);
planRoute.route("/saveProposalCtx").post(saveProposalCtx);
planRoute.route("/getPlans").get(getPlans);
planRoute.route("/getPlans/:id").get(getPlans);
planRoute.route("/getProposal/:id").get(getProposalById);
export default planRoute;
