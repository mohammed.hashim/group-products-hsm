import { Request, Response } from "express";
import db from "../../sequelize/models";
const Event = require("../../sequelize/models/event")(
  db.sequelize,
  db.Sequelize.DataTypes
);

export const createEvent = async (req: Request, res: Response) => {
  const { page_id, widget_name, type, kind, sub_kind, user_agent_info } =
    req.body;
  try {
    await Event.create({
      page_id,
      widget_name,
      type,
      kind,
      sub_kind,
      user_agent_info,
    });
    res.status(200).json({
      message: "Event has been logged",
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error");
  }
};
