import { Request, Response } from "express";
import { City, State, Pincode } from "../utils/tables";

export const getStateCityData = async (req: Request, res: Response) => {
  try {
    const states = await State.findAll({
      include: [
        {
          model: City,
        },
      ],
      order: [
        ["name", "asc"],
        [City, "name", "asc"],
      ],
    });
    res.status(200).json({
      success: true,
      data: states,
    });
  } catch (err: any) {
    console.log(err.message);
    res.status(400).json({
      success: false,
      message: err.message,
    });
  }
};
