import { Request, Response } from "express";
import { IPlan, IBenefits } from "../../interfaces/plansInterfaces";
import db from "../../../sequelize/models";
const Proposal = require("../../../sequelize/models/proposal")(
  db.sequelize,
  db.Sequelize.DataTypes
);
const Plan = require("../../../sequelize/models/plan")(
  db.sequelize,
  db.Sequelize.DataTypes
);
const ProposalCtx = require("../../../sequelize/models/proposal_ctx")(
  db.sequelize,
  db.Sequelize.DataTypes
);
const Premium = require("../../../sequelize/models/premium")(
  db.sequelize,
  db.Sequelize.DataTypes
);

const benefits: IBenefits[] = [
  {
    id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgajd",
    name: "Accidental Death",
    title: "Cover for death resulting from accident ",
    description: [
      "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
      "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
      "Your cover starts one day after the date of purchase.",
    ],
    cover: 2000000,
  },
  {
    id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgjjd",
    name: "Permanent Disability",
    title: "Cover for permanent total disablement resulting from accident",
    description: [
      "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
      "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
      "Your cover starts one day after the date of purchase.",
    ],
    cover: 2000000,
  },
];

export const getPlans = async (req: Request, res: Response) => {
  // const premiumnData = db.premium.findAll();

  if (req.params.id) {
    const planData = await Premium.findAll({
      where: {
        planId: req.params.id,
      },
      include: [{ model: Plan }],
    });

    const responseData = res.status(200).json({
      success: true,
      planData,
    });
  } else {
    const planData = await Premium.findAll({ include: [{ model: Plan }] });
    res.status(200).json({
      success: true,
      planData,
    });
  }
};

export const getBenefits = async (req: Request, res: Response) => {
  const planId = req.params.planId;
  const data = [
    {
      id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgajd",
      name: "Accidental Death",
      title: "Cover for death resulting from accident ",
      description: [
        "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
        "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
        "Your cover starts one day after the date of purchase.",
      ],
      cover: 2000000,
    },
    {
      id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgjjd",
      name: "Permanent Disability",
      title: "Cover for permanent total disablement resulting from accident",
      description: [
        "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
        "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
        "Your cover starts one day after the date of purchase.",
      ],
      cover: 2000000,
    },
  ];
  res.status(200).json({
    success: true,
    data,
  });
};

export const getProposalById = async (req: Request, res: Response) => {
  const proposalId = req?.body?.proposal_id;
  try {
    const updatedProposalData = await Proposal.findOne({
      where: { id: proposalId },
    });
    res.status(200).json({
      success: true,
      proposal: updatedProposalData,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: `No proposal data found`,
    });
  }
};

export const saveProposal = async (req: Request, res: Response) => {
  try {
    const proposalData = await Proposal.create(req.body);
    res.status(200).json({
      success: true,
      proposal: proposalData,
    });
  } catch (err: any) {
    console.error(err.message);
    res.status(400).json({
      success: false,
      message: err.message,
    });
  }
};

export const saveProposalCtx = async (req: Request, res: Response) => {
  try {
    const proposalData = await ProposalCtx.create(req.body);
    res.status(200).json({
      success: true,
      message: `Proposal purchased success`,
      proposal: proposalData,
    });
  } catch (err: any) {
    console.error(err.message);
    res.status(400).json({
      success: false,
      message: err.message,
    });
  }
};
