import { Request, Response } from "express";
import { IPlan, IBenefits } from "./../../interfaces/plansInterfaces";
const { Op } = require("sequelize");
import { Plan, Premium, Proposal, ProposalCtx } from "../../utils/tables";

const benefits: IBenefits[] = [
  {
    id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgajd",
    name: "Accidental Death",
    title: "Cover for death resulting from accident ",
    description: [
      "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
      "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
      "Your cover starts one day after the date of purchase.",
    ],
    cover: 2000000,
  },
  {
    id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgjjd",
    name: "Permanent Disability",
    title: "Cover for permanent total disablement resulting from accident",
    description: [
      "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
      "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
      "Your cover starts one day after the date of purchase.",
    ],
    cover: 2000000,
  },
];

export const getPaPlans = async (req: Request, res: Response) => {
  // const premiumnData = db.premium.findAll();
  const ages = req.params.age.split(",").map(Number);
  const pageNo = Number(req.params.page || 1);
  const limit = 5;
  let planData: any = [];
  const promises: any = [];
  console.log(ages);
  ages.map((age: number) => {
    console.log(age);
    promises.push(
      Premium.findAndCountAll({
        limit: limit * pageNo,
        include: [
          {
            model: Plan,
            where: {
              group_product_id: "1",
            },
          },
        ],
        where: {
          ageRange: {
            [Op.contains]: age,
          },
        },
        order: [[Plan, "sum_insured", "asc"]],
      })
    );
  });
  let totalPages = 0;
  await Promise.all(promises).then((response) => {
    // console.log("hi");
    const parsedData = JSON.parse(JSON.stringify(response));
    // console.log(parsedData);
    parsedData.map((eachResponse: any) => {
      totalPages =
        totalPages < eachResponse.count ? eachResponse.count : totalPages;
      eachResponse.rows.map((item: any) => {
        const planAlreadyAdded = planData.find(
          (data: any) => data.planId === item.planId
        );
        if (!planAlreadyAdded) {
          planData.push({
            planId: item.planId,
            premium: item.premium,
            plan: item.plan,
          });
        } else {
          planData.map((eachPlan: any) => {
            if (eachPlan.planId === item.planId) {
              eachPlan.premium += item.premium;
            }
          });
        }
      });
    });
  });

  // console.log(planData);

  res.status(200).json({
    success: true,
    totalPages,
    currentPage: pageNo,
    planData,
  });
};

export const getSinglePlan = async (req: Request, res: Response) => {
  const ages = req.params.age.split(",").map(Number);
  const planId = req.params.planId;
  const promises: any = [];
  let planData: any = [];

  ages.map((age: number) => {
    promises.push(
      Premium.findAll({
        include: [
          {
            model: Plan,
            where: {
              group_product_id: "2",
            },
          },
        ],
        where: {
          ageRange: {
            [Op.contains]: age,
          },
          planId: planId,
        },
        order: [[Plan, "sum_insured", "asc"]],
      })
    );
  });
  await Promise.all(promises).then((response) => {
    // console.log("hi");
    const parsedData = JSON.parse(JSON.stringify(response));
    // console.log(parsedData);
    parsedData.map((eachResponse: any) => {
      eachResponse.map((item: any) => {
        const planAlreadyAdded = planData.find(
          (data: any) => data.planId === item.planId
        );
        if (!planAlreadyAdded) {
          planData.push({
            planId: item.planId,
            premium: item.premium,
            plan: item.plan,
          });
        } else {
          planData.map((eachPlan: any) => {
            if (eachPlan.planId === item.planId) {
              eachPlan.premium += item.premium;
            }
          });
        }
      });
    });
  });
  res.status(200).json({
    success: true,
    planData,
  });
};

export const getPlans = async (req: Request, res: Response) => {
  // const premiumnData = db.premium.findAll();
  const ages = req.params.age.split(",").map(Number);
  const pageNo = Number(req.params.page || 1);
  const limit = 5;
  let planData: any = [];
  const promises: any = [];
  ages.map((age: number) => {
    promises.push(
      //Limit: pageSize
      //offset: pageNo * pageSize
      Premium.findAndCountAll({
        limit: limit * pageNo,
        include: [
          {
            model: Plan,
            where: {
              group_product_id: "2",
            },
          },
        ],
        where: {
          ageRange: {
            [Op.contains]: age,
          },
        },
        order: [[Plan, "sum_insured", "asc"]],
      })
    );
  });
  let totalPages = 0;
  await Promise.all(promises).then((response) => {
    // console.log("hi");
    const parsedData = JSON.parse(JSON.stringify(response));
    console.log(parsedData);
    parsedData.map((eachResponse: any) => {
      totalPages =
        totalPages < eachResponse.count ? eachResponse.count : totalPages;
      eachResponse.rows.map((item: any) => {
        const planAlreadyAdded = planData.find(
          (data: any) => data.planId === item.planId
        );
        if (!planAlreadyAdded) {
          planData.push({
            planId: item.planId,
            premium: item.premium,
            plan: item.plan,
          });
        } else {
          planData.map((eachPlan: any) => {
            if (eachPlan.planId === item.planId) {
              eachPlan.premium += item.premium;
            }
          });
        }
      });
    });
  });

  // console.log(planData);

  res.status(200).json({
    success: true,
    totalPages,
    currentPage: pageNo,
    planData,
  });
};

export const getBenefits = async (req: Request, res: Response) => {
  const planId = req.params.planId;
  const data = [
    {
      id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgajd",
      name: "Accidental Death",
      title: "Cover for death resulting from accident ",
      description: [
        "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
        "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
        "Your cover starts one day after the date of purchase.",
      ],
      cover: 2000000,
    },
    {
      id: "jshdakas-ashdjhsa-ashdgahjsd-asjhdgjjd",
      name: "Permanent Disability",
      title: "Cover for permanent total disablement resulting from accident",
      description: [
        "In case of death of the insured, due to an injury caused by an accident - the legal heir will receive the cover amount.",
        "The accident needs to have occurred during the policyperiod. The resulting death should be within 12 months of the accident.",
        "Your cover starts one day after the date of purchase.",
      ],
      cover: 2000000,
    },
  ];
  res.status(200).json({
    success: true,
    data,
  });
};

export const saveProposal = async (req: Request, res: Response) => {
  const proposalId = req?.body?.proposal_id;
  const memberDetails = req?.body?.memberDetails;
  const otherDetails = req?.body?.other_user_details;
  const requestBody = req.body;

  try {
    if (proposalId) {
      if (memberDetails) {
        await Proposal.update(
          { member_details: memberDetails },
          {
            where: {
              id: proposalId,
            },
          }
        );
      } else {
        await Proposal.update(
          { ...requestBody, other_user_details: otherDetails },
          {
            where: {
              id: proposalId,
            },
          }
        );
      }
      const data = await Proposal.findOne({
        where: { id: proposalId },
      });
      let json_data = JSON.parse(JSON.stringify(data));
      const planData = await Plan.findOne({
        where: { id: json_data.planId },
      });
      let json_plan_data = JSON.parse(JSON.stringify(planData));
      const updatedProposalData = {
        ...json_data,
        sum_insured: json_plan_data.sum_insured,
      };
      res.status(200).json({
        success: true,
        proposal: updatedProposalData,
        message: `Proposal updated succesfully`,
      });
    } else {
      const proposalData = await Proposal.create(req.body);
      res.status(200).json({
        success: true,
        proposal: proposalData,
        message: `Proposal created succesfully`,
      });
    }
  } catch (err: any) {
    console.error(err.message);
    res.status(500).json({
      success: false,
      error: err.message,
    });
  }
};

export const getProposalById = async (req: Request, res: Response) => {
  const proposalId = req?.params?.id;
  try {
    const data = await Proposal.findOne({
      where: { id: proposalId },
    });
    let json_data = JSON.parse(JSON.stringify(data));
    const planData = await Plan.findOne({
      where: { id: json_data.planId },
    });
    let json_plan_data = JSON.parse(JSON.stringify(planData));
    const updatedProposalData = {
      ...json_data,
      sum_insured: json_plan_data.sum_insured,
    };

    res.status(200).json({
      success: true,
      proposal: updatedProposalData,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: `No proposal data found`,
    });
  }
};

export const saveProposalCtx = async (req: Request, res: Response) => {
  try {
    const proposalData = await ProposalCtx.create(req.body);
    res.status(200).json({
      success: true,
      data: proposalData,
    });
  } catch (err: any) {
    console.error(err.message);
    res.status(500).json({
      success: false,
      message: err.message,
    });
  }
};
