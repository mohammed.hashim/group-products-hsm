import { Request, Response } from "express";
import { razorpayCredentials } from "../config/config";
import Razorpay from "razorpay";
import db from "../../sequelize/models";
const Transaction = require("../../sequelize/models/transaction")(
  db.sequelize,
  db.Sequelize.DataTypes
);
const Proposal = require("../../sequelize/models/proposal")(
  db.sequelize,
  db.Sequelize.DataTypes
);
const Premium = require("../../sequelize/models/premium")(
  db.sequelize,
  db.Sequelize.DataTypes
);

import crypto from "crypto";

const razorpay_creds =
  process.env.NODE_ENV === "dev"
    ? razorpayCredentials.uat
    : razorpayCredentials.prod;

export const instance = new Razorpay({
  key_id: razorpay_creds.CLIENT_ID,
  key_secret: razorpay_creds.SECRET,
});

export const checkout = async (req: Request, res: Response) => {
  const options = {
    amount: Number(req.body.amount * 100),
    currency: "INR",
  };
  try {
    const order = await instance.orders.create(options);
    const proposalData = await Proposal.findOne({
      where: {
        id: req.body.proposal_id,
      },
    });
    const gstCalculatedPremium =
      (proposalData.premium_amount * 18) / 100 + proposalData.premium_amount;
    const payment_valid = gstCalculatedPremium === req.body.amount;
    if (payment_valid) {
      res.status(200).json({
        success: true,
        order,
        proposalData,
        payment_valid,
      });
    } else {
      res.status(422).json({
        success: false,
        payment_valid,
      });
    }
  } catch (err: any) {
    console.log(err.message);
    res.status(400).json({
      success: false,
      message: err.message,
    });
  }
};

export const postProposalTransaction = async (req: Request, res: Response) => {
  const { response } = req.body;
  const payment_done = response.razorpay_payment_id ? true : false;
  try {
    if (Object.keys(response).length) {
      const transactionData = await Transaction.create({
        ...req.body,
        payment_done,
      });
      res.status(200).json({
        success: true,
        message: `pa proposal transaction success`,
        data: transactionData,
      });
    }
  } catch (err: any) {
    console.log(err.message);
    res.status(400).json({
      success: false,
      message: err.message,
    });
  }
};

export const paymentVerification = async (req: Request, res: Response) => {
  const { razorpay_order_id, razorpay_payment_id, razorpay_signature } =
    req.body;
  const body = razorpay_order_id + "|" + razorpay_payment_id;

  const expectedSignature = crypto
    .createHmac("sha256", razorpay_creds.SECRET)
    .update(body.toString())
    .digest("hex");

  const isAuthentic = expectedSignature === razorpay_signature;
  if (isAuthentic) {
    // Storing authenticated details to Database comes here
    const order_details = await instance.orders.fetch(razorpay_order_id);
    res.status(200).json({
      success: true,
      order_details,
    });
  } else {
    res.status(400).json({
      success: false,
    });
  }
};
