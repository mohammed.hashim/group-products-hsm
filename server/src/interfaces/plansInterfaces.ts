export interface IPlan {
  planId: string;
  sumInsured: number;
  premiumAmount: number;
  isPopular: boolean;
  minimumIncomeReq: boolean;
  minimumIncome: number;
  benefits: IBenefits[];
}

export interface IBenefits {
  id: string;
  name: string;
  title: string;
  description: string[];
  cover: number;
}
