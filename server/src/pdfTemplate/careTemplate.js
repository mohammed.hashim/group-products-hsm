import fs from "fs";
import path from "path";

const template = (data) => {
  return `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
      </head>
      <body>
        <img class="insurerLogo" src="data:image/jpeg;base64,${fs
          .readFileSync(
            path.resolve(
              __dirname,
              "../../public/img/Care_health_insurance_logo.png"
            )
          )
          .toString("base64")}" width="150" alt="Insurer_logo"/>
        <h3 class="title">Risk Cover Letter</h3>
        <div class="letterBody">
          <div class="policyNo-date-container">
            <div>
              <span class="label">Policy No:</span>
              <span class="value">${data.policyNumber}</span>
            </div>
            <div>
              <span class="label">Date:</span> <span class="value">${
                data.date
              }</span>
            </div>
          </div>
          <div>
            <span class="label">Name Of Member:</span>
            <span class="value">${data.name}</span>
          </div>
          <div class="subject-container">Subject: Risk Cover Letter</div>
          <div>
            <div>Dear Sir/Madam,</div><br>
            <div
              >This is to confirm that we agree to issue
              <b>Risk Cover Letter</b>.</div><br>
            <div>Policy Period shall be from ${data.startDate} TO ${
    data.endDate
  }</div><br>
            <div
              >Premium received INR <b>Rs.${
                data.premium
              }/- </b> through Cheque in favor
              of Care <b></b>Health Insurance Limited</b>(Formerly known as Religare Health
              Insurance Company Limited)</div
            ><br>
            <div>
                RCL is valid subject to realization of  instrument / Cheque
            </div><br><br><br>
            <div><b>
                Care Health Insurance Limited (Formerly known as Religare Health Insurance Company Limited)
            </b></div>
          </div>
        </div>
      </body>
      
    </html>
    
    <style>
      html {
        margin: 10px;
        padding: 10px;
        font-size: 16px;
      }
      .title {
        text-decoration: underline;
        text-align: center;
      }
      .letterBody {
        margin: 10px 100px;
        font-size: 16px;
      }
      .letterBody .label {
        font-weight: 700;
      }
      .subject-container {
        font-weight: bold;
    
        margin: 50px 0 50px 100px;
      }
      .insurerLogo{
        // background-image: url("data:image/png;base64,BINARY_CHUNKS");
      }
      .policyNo-date-container {
        display: flex;
        justify-content: space-between;
        margin: 50px 0 20px 0;
      }
    </style>
    `;
};

export default template;
