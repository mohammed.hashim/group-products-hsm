import db from "../../sequelize/models";
export const Premium = require("../../sequelize/models/premium")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const Plan = require("../../sequelize/models/plan")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const Proposal = require("../../sequelize/models/proposal")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const ProposalCtx = require("../../sequelize/models/proposal_ctx")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const Transaction = require("../../sequelize/models/transaction")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const State = require("../../sequelize/models/state")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const City = require("../../sequelize/models/city")(
  db.sequelize,
  db.Sequelize.DataTypes
);
export const Pincode = require("../../sequelize/models/pincode")(
  db.sequelize,
  db.Sequelize.DataTypes
);
