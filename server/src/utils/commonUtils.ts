import XLSX from "xlsx";
import pdfTemplate from "../pdfTemplate/careTemplate";
const puppeteer = require("puppeteer");
import fs from "fs";
const csv = require("csv-parser");
import { v4 as uuidv4 } from "uuid";
import { config } from "../config/config";
import { City, Pincode, Plan, Premium, State } from "./tables";

export const readRateChart = (product: string) => {
  //   Reading the file
  const workbook = XLSX.readFile("./RateChart.xlsx");
  //   convert XL to  JSON
  let paSheet = workbook.Sheets[product];
  const groupProductId =
    product === "GroupPa" ? 1 : product === "GroupCancer" ? 2 : 0;
  const rateChart = XLSX.utils.sheet_to_json(paSheet);
  rateChart.map((item: any, index: number) => {
    item.planId = index + 1;
  });
  // console.log(rateChart);
  const temp: any = {};
  rateChart.map((item: any) => {
    if (!temp[item["Sum Insurred"]]) {
      temp[item["Sum Insurred"]] = [];
    }
    temp[item["Sum Insurred"]].push({
      "Max Age": item["Max Age"],
      "Min Age": item["Min Age"],
      planId: item["planId"],
      premium: item["premium"],
    });
  });
  const { planData, premiumData } = transformData(temp, groupProductId);
  storeDataInDb(planData, premiumData);
};

const transformData = (data: any, groupProductId: number) => {
  const planData: any = [];
  const premiumData: any = [];
  // console.log(data);
  Object.keys(data).map((sumInsured: string) => {
    let id = uuidv4();
    planData.push({
      id: id,
      insurer_id: "99",
      min_monthly_income: null,
      created_on: new Date(),
      validity: 365,
      group_product_id: groupProductId,
      updated_on: new Date(),
      sum_insured: sumInsured,
      is_popular: false,
    });
    data[sumInsured].map((item: any) => {
      premiumData.push({
        premium: item["premium"],
        planId: id,
        ageRange: [
          { value: item["Min Age"], inclusive: true },
          { value: item["Max Age"], inclusive: true },
        ],
      });
    });
  });

  return { planData, premiumData };
};

const storeDataInDb = (planData: any, premiumData: any) => {
  try {
    planData.forEach(async (element: any) => {
      const plan = await Plan.create({
        id: element.id,
        insurer_id: element.insurer_id,
        min_monthly_income: element.min_monthly_income,
        createdAt: element.created_on,
        validity: element.validity,
        group_product_id: element.group_product_id,
        updatedAt: element.updated_on,
        sum_insured: element.sum_insured,
        is_popular: element.is_popular,
      });
    });
    premiumData.forEach(async (element: any) => {
      const premium = await Premium.create({
        premium: element.premium,
        min_age: element.minAge,
        max_age: element.maxAge,
        planId: element.planId,
        ageRange: element.ageRange,
      });
    });
  } catch (error) {
    console.log(error);
  }
};

export const generatePdf = async (req: any, res: any) => {
  //TO DO: query on db for policy details and map the data as following
  let mapObj: any = {
    policyNumber: "xxxx",
    date: "xx-xx-xxxx",
    name: "xxxx xxxx",
    startDate: "xx-xx-xxxx",
    endDate: "xx-xx-xxxx",
    premium: "xxx",
  };
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setContent(pdfTemplate(mapObj));
  const buffer = await page.pdf({
    format: "A4",
    printBackground: true,
  });
  res.setHeader("Content-Type", "application/pdf");
  res.setHeader("Content-Disposition", 'attachment; filename="coi.pdf"');
  res.send(buffer);

  await browser.close();
};

export const getConfig = async (req: any, res: any) => {
  try {
    res.status(200).json(config);
  } catch (error) {
    res.status(400).send("No config");
  }
};

export const readStateCsvFiles = async (filePaths: any[]) => {
  // array of file paths for multiple csv files
  try {
    for (let i = 0; i < filePaths.length; i++) {
      const filePath = filePaths[i];
      const stream = fs.createReadStream(filePath);
      const rows: any = [];
      stream
        .pipe(csv())
        .on("data", function (data: any) {
          rows.push({
            ...data,
          });
        })
        .on("end", async function () {
          if (filePath === "./state_db.csv") {
            await State.bulkCreate(rows);
          } else if (filePath === "./city_db.csv") {
            await City.bulkCreate(rows);
          } else {
            return;
          }
        });
    }
  } catch (error) {
    console.error(error);
  }
};
