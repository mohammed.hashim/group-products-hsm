// import modules
import express, { Express } from "express";
import cors from "cors";
import morgan from "morgan";
import dotenv from "dotenv";
import paymentRoute from "./src/routes/paymentRoutes";
import planRoutes from "./src/routes/pa/planRoutes";
import cancerPlanRoute from "./src/routes/cancer/planRoutes";
import groupPaPlanRoute from "./src/routes/groupPa/planRoutes";
import {
  // readPARateChart,
  generatePdf,
  getConfig,
  readStateCsvFiles,
} from "./src/utils/commonUtils";
dotenv.config();
import db from "./sequelize/models";
import { createEvent } from "./src/controllers/logEvents";
import { getStateCityData } from "./src/controllers/appController";
const stateFile = "./state_db.csv";
const cityFile = "./city_db.csv";
const pincodeFile = "./pincode_db.csv";
//app
const app: Express = express();

//db

//middleware
app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

//ROUTES
app.use("/api/pg", paymentRoute);
app.use("/plans", planRoutes);
app.use("/cancer/plans", cancerPlanRoute);
app.use("/groupPa/plans", groupPaPlanRoute);
// generate pdf
app.get("/getPdf", async (req, res) => {
  generatePdf(req, res);
});

// get config variables
app.get("/getConfig", getConfig);

// log event post api
app.post("/log-event", createEvent);

app.get("/states", getStateCityData);

//port
const port = process.env.PORT || 8080;

//listeners

async function main() {
  // reading rate chart
  await db.sequelize.sync({ force: true });
  // readRateChart("GroupCancer");
  // readStateCsvFiles([stateFile, cityFile, pincodeFile]);
  app.listen(port, () => console.log(`Server is running on ${port}`));
}

main();
