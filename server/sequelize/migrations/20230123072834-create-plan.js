"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("plans", {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      sum_insured: {
        type: Sequelize.INTEGER,
      },
      is_popular: {
        type: Sequelize.BOOLEAN,
      },
      insurer_id: {
        type: Sequelize.STRING,
      },
      min_monthly_income: {
        type: Sequelize.INTEGER,
      },
      validity: {
        type: Sequelize.INTEGER,
      },
      group_product_id: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("plans");
  },
};
