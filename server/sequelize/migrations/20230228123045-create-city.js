"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("cities", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      stateId: {
        type: Sequelize.INTEGER,
        references: {
          model: "states",
          key: "id",
        },
      },
      is_major: {
        type: Sequelize.BOOLEAN,
      },
      is_enabled: {
        type: Sequelize.BOOLEAN,
      },
      is_approved: {
        type: Sequelize.BOOLEAN,
      },
      slug: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("cities");
  },
};
