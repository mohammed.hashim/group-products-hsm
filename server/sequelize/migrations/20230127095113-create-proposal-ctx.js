"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("proposal_ctxes", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      full_name: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      dob: {
        type: Sequelize.DATE,
      },
      proposal_data: {
        type: Sequelize.JSONB,
        allowNull: false,
      },
      transaction_data: {
        type: Sequelize.JSONB,
        allowNull: false,
      },
      plan_id: {
        type: Sequelize.UUID,
      },
      payment_success: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("proposal_ctxes");
  },
};
