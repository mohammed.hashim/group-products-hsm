"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("pincodes", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      id: {
        type: Sequelize.INTEGER,
      },
      pincode: {
        type: Sequelize.STRING,
      },
      stateId: {
        type: Sequelize.INTEGER,
        references: {
          model: "states",
          key: "id",
        },
      },
      lat: {
        type: Sequelize.NUMBER,
      },
      lng: {
        type: Sequelize.NUMBER,
      },
      city_name: {
        type: Sequelize.STRING,
      },
      is_metro: {
        type: Sequelize.BOOLEAN,
      },
      cityId: {
        type: Sequelize.INTEGER,
        references: {
          model: "cities",
          key: "id",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("pincodes");
  },
};
