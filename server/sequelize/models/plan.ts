"use strict";
import { Model, UUIDV4 } from "sequelize";

interface planAttributes {
  id: string;
  insurer_id: string;
  min_monthly_income: number | null;
  validity: number;
  group_product_id: string;
  sum_insured: number;
  is_popular: boolean;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class plan extends Model<planAttributes> implements planAttributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: string;
    insurer_id!: string;
    min_monthly_income!: number | null;
    validity!: number;
    group_product_id!: string;
    sum_insured!: number;
    is_popular!: boolean;

    static associate(models: any) {
      // plan.hasOne(models.premium);
      // define association here
    }
  }
  plan.init(
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      sum_insured: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      is_popular: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
      insurer_id: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      min_monthly_income: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      validity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      group_product_id: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "plan",
      tableName: "plans",
    }
  );
  return plan;
};
