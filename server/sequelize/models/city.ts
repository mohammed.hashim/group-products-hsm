"use strict";
import { Model } from "sequelize";
import { State } from "../../src/utils/tables";

interface cityAtrributes {
  id: number;
  name: string;
  stateId?: number;
  is_major: boolean;
  is_enabled: boolean;
  is_approved: boolean;
  slug: string;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class city extends Model<cityAtrributes> implements cityAtrributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: number;
    name!: string;
    stateId!: number;
    is_major!: boolean;
    is_enabled!: boolean;
    is_approved!: boolean;
    slug!: string;
    static associate(models: any) {
      // define association here
      // city.belongsTo(models.state, { foreignKey: "state_id" });
    }
  }
  city.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.INTEGER,
      },
      name: DataTypes.STRING,
      stateId: {
        type: DataTypes.INTEGER,
        references: {
          model: "states",
          key: "id",
        },
      },
      is_major: DataTypes.BOOLEAN,
      is_enabled: DataTypes.BOOLEAN,
      is_approved: DataTypes.BOOLEAN,
      slug: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "city",
      tableName: "cities",
    }
  );
  State.hasMany(city);
  city.belongsTo(State);
  return city;
};
