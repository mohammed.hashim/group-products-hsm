"use strict";
const { Model } = require("sequelize");

interface eventAttributes {
  id: string;
  page_id: string;
  widget_name: string;
  type: string;
  kind: string;
  sub_kind: string;
  user_agent_info: any;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class event extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: string;
    page_id!: string;
    widget_name!: string;
    type!: string;
    kind!: string;
    sub_kind!: string;
    user_agent_info!: any;

    static associate(models: any) {
      // define association here
    }
  }
  event.init(
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      page_id: DataTypes.STRING,
      widget_name: DataTypes.STRING,
      type: DataTypes.STRING,
      kind: DataTypes.STRING,
      sub_kind: DataTypes.STRING,
      user_agent_info: DataTypes.JSON,
    },
    {
      sequelize,
      modelName: "event",
      freezeTableName: true,
      tableName: "events",
    }
  );
  return event;
};
