"use strict";
import { Model } from "sequelize";
import { City, Pincode } from "../../src/utils/tables";

interface stateAtrributes {
  id: number;
  name: string;
  is_enabled: boolean;
  slug: string;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class state extends Model<stateAtrributes> implements stateAtrributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: number;
    name!: string;
    is_enabled!: boolean;
    slug!: string;

    static associate(models: any) {}
  }
  state.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.INTEGER,
      },
      name: DataTypes.STRING,
      is_enabled: DataTypes.BOOLEAN,
      slug: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "state",
      tableName: "states",
    }
  );
  return state;
};
