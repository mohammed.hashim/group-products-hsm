"use strict";
import { Model, UUIDV4 } from "sequelize";
import db from "../models/index";
const Plan = require("../models/plan")(db.sequelize, db.Sequelize.DataTypes);

interface proposalCtxAttributes {
  id: string;
  planId: string;
  proposal_data: any;
  transaction_data: any;
  payment_success: boolean;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class proposal_ctx
    extends Model<proposalCtxAttributes>
    implements proposalCtxAttributes
  {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: string;
    planId!: string;
    proposal_data!: any;
    transaction_data!: any;
    payment_success!: boolean;

    static associate(models: any) {
      // define association here
    }
  }
  proposal_ctx.init(
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: UUIDV4,
      },
      proposal_data: { type: DataTypes.JSONB, allowNull: false },
      transaction_data: { type: DataTypes.JSONB, allowNull: false },
      planId: { type: DataTypes.UUID, allowNull: false },
      payment_success: { type: DataTypes.BOOLEAN, defaultValue: false },
    },
    {
      sequelize,
      modelName: "proposal_ctx",
      tableName: "proposal_ctxes",
    }
  );
  proposal_ctx.belongsTo(Plan)
  return proposal_ctx;
};
