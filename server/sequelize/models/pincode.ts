"use strict";
import { Model } from "sequelize";
import { City, State } from "../../src/utils/tables";

interface pinCodeAtrributes {
  id: number;
  pincode: string;
  stateId: number;
  lat: number;
  lng: number;
  city_name: string;
  is_metro: boolean;
  cityId: number;
}
module.exports = (sequelize: any, DataTypes: any) => {
  class pincode extends Model<pinCodeAtrributes> implements pinCodeAtrributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: number;
    pincode!: string;
    stateId!: number;
    lat!: number;
    lng!: number;
    city_name!: string;
    is_metro!: boolean;
    cityId!: number;

    static associate(models: any) {
      // define association here
    }
  }
  pincode.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.INTEGER,
      },
      pincode: DataTypes.STRING,
      stateId: DataTypes.INTEGER,
      lat: DataTypes.STRING,
      lng: DataTypes.STRING,
      city_name: DataTypes.STRING,
      is_metro: DataTypes.BOOLEAN,
      cityId: {
        type: DataTypes.INTEGER,
        references: {
          model: "cities",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "pincode",
      tableName: "pincodes",
    }
  );
  pincode.belongsTo(City);
  pincode.belongsTo(State);
  State.hasMany(pincode);
  City.hasOne(pincode);
  return pincode;
};
