"use strict";
import { Model, UUIDV4 } from "sequelize";

interface transactionAttributes {
  id: string;
  proposal_id: string;
  payment_done: boolean;
  request: any;
  response: any;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class transaction
    extends Model<transactionAttributes>
    implements transactionAttributes
  {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: string;
    proposal_id!: string;
    payment_done!: boolean;
    request!: any;
    response!: any;

    static associate(models: any) {
      // define association here
      transaction.belongsTo(models.proposal, {
        foreignKey: "proposal_id",
      });
    }
  }
  transaction.init(
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      proposal_id: { type: DataTypes.UUID, allowNull: false },
      payment_done: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      request: { type: DataTypes.JSON, allowNull: false },
      response: { type: DataTypes.JSON, allowNull: false },
    },
    {
      sequelize,
      modelName: "transaction",
      tableName: "transactions",
    }
  );
  return transaction;
};
