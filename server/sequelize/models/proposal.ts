"use strict";
import { Model, UUIDV4 } from "sequelize";
import db from "../models/index";
const Plan = require("../models/plan")(db.sequelize, db.Sequelize.DataTypes);
const Premium = require("../models/premium")(
  db.sequelize,
  db.Sequelize.DataTypes
);

interface proposalAtrributes {
  id: string;
  full_name: string;
  gender: string;
  email?: string;
  dob?: number;
  contact: number;
  planId?: string;
  premium_amount?: number;
  member_details?: any;
  other_user_details?: any;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class proposal
    extends Model<proposalAtrributes>
    implements proposalAtrributes
  {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: string;
    full_name!: string;
    email!: string;
    gender!: string;
    dob!: number;
    premium_amount!: number;
    planId!: string;
    contact!: number;
    member_details!: any;
    other_user_details!: any;

    static associate(models: any) {
      // define association here
    }
  }
  proposal.init(
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      full_name: { type: DataTypes.STRING, allowNull: false },
      email: { type: DataTypes.STRING, allowNull: true },
      gender: { type: DataTypes.STRING, allowNull: false },
      dob: { type: DataTypes.DATE, allowNull: true },
      planId: { type: DataTypes.UUID, allowNull: false },
      premium_amount: { type: DataTypes.INTEGER, allowNull: false },
      contact: { type: DataTypes.STRING, allowNull: false },
      member_details: { type: DataTypes.JSONB, allowNull: true },
      other_user_details: { type: DataTypes.JSONB, allowNull: true },
    },
    {
      sequelize,
      modelName: "proposal",
      tableName: "proposals",
    }
  );
  proposal.belongsTo(Plan);
  return proposal;
};
