"use strict";
import { Model } from "sequelize";
import db from "../models/index";
const Plan = require("../models/plan")(db.sequelize, db.Sequelize.DataTypes);
interface premiumAttributes {
  premium: number;
  planId: string;
  ageRange: any;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class premium extends Model<premiumAttributes> implements premiumAttributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    premium!: number;
    planId!: string;
    ageRange!: any;
    static associate(models: any) {
      // define association here
      // premium.belongsTo(models.plan, {
      //   foreignKey: "plan_id",
      // });
    }
  }
  premium.init(
    {
      premium: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      planId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "plans",
          key: "id",
        },
      },
      ageRange: {
        type: DataTypes.RANGE(DataTypes.INTEGER),
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "premium",
      tableName: "premiums",
    }
  );
  premium.belongsTo(Plan);
  // };
  return premium;
};
