import { readStateCsvFiles } from "./src/utils/commonUtils";
const stateFile = "./state_db.csv";
const cityFile = "./city_db.csv";
const pincodeFile = "./pincode_db.csv";

async function addStateData() {
  // reading rate chart
  try {
    await readStateCsvFiles([stateFile, cityFile, pincodeFile]);
  } catch (error) {
    console.log(`Server error reading csv files`);
  }
}

addStateData();
